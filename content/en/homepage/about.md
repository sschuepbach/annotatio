---
title: annotat.io
draft: false
# role: Example Role
avatar: images/logo.png
bio: Bits and pieces
social:
  - icon: envelope
    iconPack: fas
    url: mailto:example@example.com
  - icon: mastodon
    iconPack: fab
    url: https://openbiblio.social/@seb
  - icon: gitlab
    iconPack: fab
    url: https://gitlab.switch.ch/sschuepbach

widget:
  handler: about

  # Options: sm, md, lg and xl. Default is md.
  width:

  sidebar:
    # Options: left and right. Leave blank to hide.
    position:
    # Options: sm, md, lg and xl. Default is md.
    scale:
  
  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color: secondary
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment: 
---

## Hello...

Dear traveller of the vastness called World Wide Web! I'm really happy that you found exactly this site among the zillions which are out there.
            
But let me introduce myself. My name is Sebastian, and I like - among other
things, of course - to [write](posts/), [code](projects/) and sometimes
[speak](slides/) about the things that I do. I hold a master’s degree in
History and Philosophy and currently work as a full-stack developer and
software architect at the University Library of Basel.

This small site here serves as a springboard to all these ventures. Please,
have look around, I'm sure you won't regret it.
