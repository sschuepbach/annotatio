+++
title = 'Virtuoso'
date = '2015-08-11'
+++


<http://virtuoso.openlinksw.com/>

## Installation

Hinweis: x steht für {1,2}

	[clone git@github.com:openlink/virtuoso-opensource.git]

	cd ~/gitrepo/virtuoso-opensource
	git pull origin master
	./autogen.sh
	./configure --prefix=/usr/local/virtuosoX
	make
	sudo -s
	make install
	make clean
	mv /usr/local/virtuosoX/var/lib/virtuoso/{db,vsp} /data/virtuoso/virtuosoX
	rm -r /usr/local/virtuosoX/var/lib/virtuoso
	ln -s /data/virtuoso/virtuosoX /usr/local/virtuosoX/var/lib/virtuoso
	exit


Anschliessend folgende Anpassungen in /data/virtuoso/virtuoso**X**/db/virtuoso.ini vornehmen:

* [Parameters] ServerPort = 111**X**
* DirsAllowed: /data/virtuoso/temp_upload hinzufügen
* NumberOfBuffers = 2720000 (Standard ausklammern)
* MaxDirtyBuffers = 2000000 (Standard ausklammern)
* [HTTPServer] ServerPort = 889**X**
* ServerName = virtuoso**X**
* DefaultHost = localhost:889**X**


Schliesslich Umgebungsvariable in ~/.zsh/envvar setzen: ``export VIRTUOSO``**X**``_HOME="/usr/local/virtuoso``**X**``/"``

## Bulk-Upload von RDF-Daten

``virtuoso-bulk-load.sh <RDF file> <Bulk upload path> <graph IRI> <Path to Virtuoso Home> <Virtuoso SQL port> <username> <password>``

## Deinstallation

``sudo dpkg -r virtuoso``

## Referenzen

* Funktionsreferenz: <http://docs.openlinksw.com/virtuoso/functions.html>


