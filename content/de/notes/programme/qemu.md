+++
title = 'Qemu'
+++


## Installation einer neuen VM

1. Anlegen eines neuen HD-Images. Dies wird mit dem Befehl ``qemu-img`` durchgeführt. Bsp. ``qemu-img create -f raw ubuntu-server-17.10.raw 10G``
2. Installation des OS. Dies wird mit ``qemu-system-<arch>`` durchgeführt. Bsp.: ``qemu-system-x86_64 --enable-kvm -m 8G -net nic,model=virtio -cdrom ~/temp/ubuntu-17.10.1-server-amd64.iso -boot order=d -drive if=virtio,format=raw,file=ubuntu-server-17.10.raw``
3. Starten der neuen VM. Bsp. ``qemu-system-x86_64 --enable-kvm --nographic -m 8G -net nic,model=virtio -net user,hostfwd=tcp::10022-:22 -drive format=raw,file=ubuntu-server-17.10.raw,if=virtio``


## Befehle

### qemu-img
	qemu-img allows you to create, convert and modify images offline.
	It can handle all image formats supported by QEMU.


## Navigation

Tastenkombination, um Mauszeiger aus VM-Fenster zu lösen: ``Ctrl+Alt+G``

## Monitor-Konsole

* Öffnen: ``Ctrl+Alt+2``
* Verlassen: ``Ctrl+Alt+1``


## Snapshots

**Wichtig**: Funktioniert nur bei einem ``qcow2``-formatiertem System!
Über die QEMU Monitor-Konsole (Ctrl+Alt+2) :

* ``savevm <name>``: Snapshot speichern
* ``loadvm <name>``: Snapshot laden
* ``delvm <name>``: Snapshot speichern
* ``info snapshots``: Gespeicherte Snapshots auflisten


