+++
title = "PlantUML"
date = "2022-09-22T09:33:25+02:00"
description = "Kommandozeilentool zur Erstellung von UML- und anderen Diagrammen"
tags = ["cli", "uml", "softwarearchitektur"]
draft = true
+++

PlantUML ist einerseits ein Kommandozeilentool zur Erstellung von {{< ref ../standards/uml.md >}} - und
weiteren Diagrammtypen, anderseits eine DSL zur Beschreibung solcher
Diagramme. 


## Ressourcen

* [PlantUML Website](https://plantuml.com/)
* [Alternative Dokumentation](https://plantuml-documentation.readthedocs.io/en/latest/index.html)
* [Allgemeines zu UML-Diagrammen](https://www.uml-diagrams.org/)
