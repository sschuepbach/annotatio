+++
title = 'Catmandu'
+++

[Website](http://librecat.org/)

## Kernkonzepte

* **Items**: Basiseinheit der Datenprozessierung in Catmandu. Kann in einer Reihe von Formaten sein, wobei "Item" je nach Format einen anderen Umfang hat (bspw. eine Textzeile in einem Text-Format, ein Record in einer MARC-Datei etc.). Das Catmandu-eigene Format ist ähnlich wie JSON strukturiert.
* **Importers**: Werden gebraucht, um *Items* zu lesen. Der Name von *Importers* muss dem ``convert``-Befehl als erstes Argument mitgegeben werden. Mit Hilfe der ``--type``-Option kann ein Format spezifiziert werden, bspw. ``catmandu convert MARC --type XML < marc.xml`` oder ``catmandu convert MARC --type ALEPHSEQ < marc.txt``.
* **Exporters**: Werden gebraucht, um *Items* zu schreiben. Der Name von *Exporters* muss dem ``to``-Befehl als erstes Argument mitgegeben werden.
* **Stores**: Datenbanken
* **Fixes**: Transformieren / mappen Daten


## Befehle

* ``convert``: Konvertiert ein Format in ein anderes, oder lädt Daten aus dem Internet
* ``import``: Importiert Daten in eine Datenbank
* ``export``: Exportiert Daten aus Datenbank
* ``delete``: Löscht Daten aus Datenbank


## Konfigurationsdatei

Konfigurationsdateien müssen im gleichen Verzeichnis liegen, wo der ``catmandu``-Befehl ausgeführt wird.
Bsp.:
	---
	importer:
	  ghent:
	     package: OAI
	     options:
	        url: http://biblio.ugent.be
	        set: public
	        handler: marcxml
	        metadataPrefix: marc21
	store:
	  ghentdb:
	     package: MongoDB
	     options:
	        database_name: oai_data
	        default_bag: data


Anschliessend kann bspw. folgendermassen auf die Konfigurationen zugegriffen werden:
	catmandu convert ghent # Extraktion aus entfernter OAI-Schnittstelle

	catmandu import ghent to ghentdb # Dito und Import in Mongo-DB

	catmandu export ghentdb # Dump aus Mongo-DB




## JSON Paths

	Array: <key>.<index 0-...>
	Inner Object: <key>.<key>

	add_field(foo.$append,test)
	add_field(foo.0,test)

Hängt jeweils ein neuer Wert in einem Array mit Key foo an den Datensatz an, zuert mit einer impliziten, dann mit einer expliziten Indexangabe. Wird ein Element in einem Array mit expliziten Index hinzugefügt, dann werden tiefere, noch nicht vorhandene Index-Wert-Paare mit tieferem Index mit leeren Werten erzeugt (in YAML repräsentiert durch einen - )

Werte, die nicht apostrophiert werden, werden von Catmandu als potentielle JSON-Paths behandelt!

## Fix script

Kleinere Fix-Skripte können direkt in der Kommandozeile geschrieben werden. Dazu müssen die Anweisungen in der ``fix()``-Funktion abgelegt werden.

## marc_map

Substring-Extraktion, bspw. Position 7-10 von Feld 008, und Binding in Feld year:
``marc_map(008/07-10,year)``



``do visitor()`` wendet eine bestimmte Funktion auf alle Felder in allen Records an.


## Hilfe


* Kurzhilfe: ``catmandu --help`` oder spezifischer ``catmandu <command> --help``
* Ausführliche Hilfe: ``catmandu help`` oder spezifischer ``catmandu help <command>`` bzw. ``catmandu help <command> <fileformat>[[::<spec_type>]]``


