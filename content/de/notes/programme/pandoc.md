+++
title = "Pandoc"
+++


Pandoc ist ein Framework zur Konvertierung einer Vielzahl von Auszeichnungssprachen.

Einfache Transformation:

```sh
pandoc -f<Eingabeformat> -t<Ausgabeformat> -o<Zieldateiname> <Eingabedatei>
```

Unterstützte Eingabeformate:

```sh
pandoc --list-input-formats
```

Unterstützte Ausgabeformate:

```sh
pandoc --list-output-formats
```

Unterstützte Erweiterungen:

```sh
pandoc --list-extensions
```
