+++
title = 'libvirt'
+++

libvirt ist eine API, ein Daemon sowie eine Applikation zur Steuerung von Plattformvirtualisierungen und kann u.a. zum Administrieren von KVM, [Qemu](./Qemu.md), Xen und anderen Hypervisoren. Dazu steltl libvirt das CLI-Tool *virsh* zur Verfügung.

## virsh

virsh muss mit root-Rechten ausgeführt werden.

### Hilfe
``sudo virsh help``
Und zu einzelnen Befehlen:
``sudo virsh help <command>``

### Auflisten der vorhandenen Domains (=VMs)
``sudo virsh list --all``
Ohne ``--all`` werden nur die laufenden Domains angezeigt

### Aufstarten einer Domain
``sudo virsh start domain``

### Beenden der Domain
``sudo virsh stop domain``

