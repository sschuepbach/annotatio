+++
title = 'VirtualBox'
date = '2015-09-24'
+++


## Starten

	# Benötigtes Kernelmodul laden
	sudo modprobe vboxdrv
	# Kernelmodule für Bridging und Benutzung der PCI-Schnittstelle laden
	sudo modprobe vboxnetadp vboxnetflt vboxpci
	# Start GUI
	VirtualBox
	# Start CLI für Administration von virtuellen Maschinen (Installation etc.)
	VBoxHeadless
	# Start CLI (Start / Stop und Änderung von Einstellungen)
	VBoxSDL

