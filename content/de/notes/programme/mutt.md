+++
title = 'Mutt'
date = '2015-03-12'
+++


## CLI-Optionen

[Mutt-Manual Kap. 9.1.](http://www.mutt.org/doc/devel/manual.html#commandline)

### Mail über die Kommandozeile verschicken

```
mutt [<Optionen>] <Empfänger>

-a <Datei> --   Datei(en) anhängen. Dieser Parameter muss als letzte Option gesetzt und mit einem Doppelstrich abgeschlossen werden.
-b <Adresse>    BCC an
-c <Adresse>    CC an
-i <Datei>      Datei, die Body enthält (die Datei kann auch durch STDIN-Umleitung hinzugefügt werden)
-s <Betreff>    Betreff
```

### Interaktive Mutt-Instanz starten

```
mutt [<Optionen>]

-e <Befehl> Befehl nach der Initialisierung ausführen
-p          Zurückgestellte Nachricht öffnen
-z          Start nur bei neuen Nachrichten in Mailbox
-Z          Erste Mailbox öffnen, die neue Nachrichten enthält
```

## Mutt-Ansichten

[Mutt-Manual Kap. 2](http://www.mutt.org/doc/devel/manual.html#concept-screens-and-menus)

* Index: Liste der E-Mails in ausgewählter Mailbox
* Pager: Vollanzeige eines E-Mails
* File Browser
* Help
* Compose Menu: Anzeige des Headers sowie der Anhänge eines neu erstellten E-Mails
* Alias Menu: Kontextsensitives Adressbuch
* Attachment Browser: Anzeige der E-Mail-Anhänge

Tempzeugs
a -> Attachment-Präfix
A -> Alias-Präfix
b -> Mailbox
c -> Thread
e -> PGP-Schlüssel-Präfix
f -> "Dateimanager"
h -> Headers
m -> Mail-Präfix
Q -> Query-Präfix

## Suchmuster

| Pattern          | Description                                                      |
|------------------|------------------------------------------------------------------|
| `~A`             | all messages                                                     |
| `~b EXPR`        | messages which contain `EXPR` in the message body                |
| `~B EXPR`        | messages which contain `EXPR` in the whole message               |
| `~c USER`        | messages carbon-copied to `USER`                                 |
| `%c GROUP`       | messages carbon-copied to any member of `GROUP`                  |
| `~C EXPR`        | message is either `to:` or `cc:` `EXPR`                          |
| `%C GROUP`       | messages either `to:` or `cc:` to any member of `GROUP`          |
| `~D`             | deleted messages                                                 |
| `~d [MIN]-[MAX]` | messages with `date-sent` in a Date range                        |
| `~E`             | expired messages                                                 |
| `~e EXPR`        | message which contains `EXPR` in the `Sender` field              |
| `%e GROUP`       | messages which contain a member of `GROUP` in the `Sender` field |
| `~F`             | flagged messages                                                 |
| `~f USER`        | messages originating from `USER`                                 |
| `%f GROUP`       | messages originating from any member of `GROUP`                  |
| `~g`             | PGP signed messages                                              |
| `~G`             | PGP encrypted messages                                           |
| `~h EXPR`        | messages which contain `EXPR` in the message header              |
| `~H EXPR`        | messages with a spam attribute matching `EXPR`                   |
| `~i ID`          | message which match `ID` in the `Message-ID` field               |
| `~k`             | message contains PGP key material                                |
| `~L EXPR`        | message is either originated or received by `EXPR`               |
| `%L GROUP`       | message either originated or received by any member of `GROUP`   |
| `~l`             | message is addressed to a known mailing list                     |
| `~m [MIN]-[MAX]` | message in the range `MIN` to `MAX` *)                           |
| `~n [MIN]-[MAX]` | messages with a score in the range `MIN` to `MAX` *)             |
| `~N`             | new messages                                                     |
| `~O`             | old messages                                                     |
| `~p`             | message is addressed to you (consults `$alternates`)             |
| `~P`             | message is from you (consults `$alternates`)                     |
| `~Q`             | messages which have been replied to                              |
| `~R`             | read messages                                                    |
| `~r [MIN]-[MAX]` | messages with `date-received` in a Date range                    |
| `~S`             | superseded messages                                              |
| `~s SUBJECT`     | messages having `SUBJECT` in the `Subject` field.                |
| `~T`             | tagged messages                                                  |
| `~t USER`        | messages addressed to `USER`                                     |
| `~u`             | messages addressed to a subscribed mailing list                  |
| `~U`             | unread messages                                                  |
| `~v`             | message is part of a collapsed thread.                           |
| `~V`             | cryptographically verified messages                              |
| `~x EXPR`        | messages which contain `EXPR` in the `References` field          |
| `~X [MIN]-[MAX]` | messages with `MIN` to `MAX` attachments *)                      |
| `~y EXPR`        | messages which contain `EXPR` in the `X-Label` field             |
| `~z [MIN]-[MAX]` | messages with a size in the range `MIN` to `MAX` *) **)          |
| `~=`             | duplicated messages                                              |
| `~$`         | unreferenced messages (requires threaded view)                                                                     |
| `~(PATTERN)` | messages in threads containing messages matching `PATTERN`, e.g. all threads containing messages from you: `~(~P)` |

*) The forms `<[MAX]`, `>[MIN]`, `[MIN]-` and `-[MAX]` are allowed, too.

**) The suffixes `K` and `M` are allowed to specify kilobyte and megabyte respectively.


* You can force Mutt to treat EXPR as a simple string instead of a regular expression by using = instead of ~ in the pattern name.
* Dates must be in DD/MM/YY format (month and year are optional, defaulting to the current month and year).
* If you omit the minimum (first) date, and just specify “-DD/MM/YY”, all messages before the given date will be selected. If you omit the maximum (second) date, and specify “DD/MM/YY-”, all messages after the given date will be selected. If you specify a single date with no dash (“-”), only messages sent on the given date will be selected. 
* You can add error margins to absolute dates. An error margin is a sign (+ or -), followed by a digit, followed by one of the units y (years), m (months), w (weeks), d (days). As a special case, you can replace the sign by a “*” character, which is equivalent to giving identical plus and minus error margins. 
