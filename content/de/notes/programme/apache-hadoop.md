+++
title = 'Apache Hadoop'
+++


## Installation

* Hinweise zur Installation s. <https://wiki.archlinux.org/index.php/Hadoop>.
* Für die nachfolgende Konfiguration des Clusters s. Hinweise im Buch Hadoop: The Definitive Guide, Anhang A: Installing Apache Hadoop, Configuration: Pseudodistributed Mode
* Wichtig: Die in ``hdfs-site.xml`` angegeben Pfade (bspw. ``dfs.name.dir``, ``dfs.data.dir``, ``dfs.namenode.data.dir``, ``dfs.namenode.name.dir``) müssen ``chmod 777`` sein!


