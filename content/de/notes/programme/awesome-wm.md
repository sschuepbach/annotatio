+++
title = 'awesome WM'
date = '2015-08-02'
+++


Einige Links:

* <http://awesome.naquadah.org/wiki/Main_Page>
* <http://awesome.naquadah.org/doc/api/>
* <http://awesome.nquadah.org/wiki/My_first_awesome>
* <http://awesome.naquadah.org/wiki/Awesome_3_configuration>
* <http://awesome.naquadah.org/wiki/Awesome_3.4_to_3.5>
* <http://awesome.naquadah.org/wiki/Awesome_3.x>
* <http://awesome.naquadah.org/wiki/FAQ>
* <http://www.lua.org/pil/>
* <http://awesome.naquadah.org/wiki/The_briefest_introduction_to_Lua>


## Default Keymap

### Window Manager Control
Mod4 + Ctrl + r		Restart awesome
Mod4 + Shift + q		Quit awesome
Mod4 + r			Run prompt
Mod4 + x			Run Lua code prompt
Mod4 + Return		Spawn terminal emulator
Mod4 + w			Open main menu

### Clients
Mod4 + m			Maximize client
Mod4 + n			Minimize client
Mod4 + Ctrl + n		Restore client
Mod4 + f			Set client fullsc­reen
Mod4 + Shift + c		Kill focused client
Mod4 + t			Set client on-top

### Navigation
Mod4 + j			Focus next client
Mod4 + k			Focus previous client
Mod4 + u			Focus first urgent client
Mod4 + Left			View previous tag
Mod4 + Right		View next tag
Mod4 + 1-9			Switch to tag 1-9
Mod4 + Ctrl + j		Focus next screen
Mod4 + Ctrl + k		Focus previous screen
Mod4 + Esc			Focus previously selected tag set.

### Layout Modification
Mod4 + Shift + j		Switch client with next client
Mod4 + Shift + k		Switch client with previous client
Mod4 + o			Send client to next screen
Mod4 + h			Decrease master width factor by 5%
Mod4 + l			Increase master width factor by 5%.
Mod4 + Shift + h		Increase number of master clients (d.h., die Zahl der Clients, welche im für den master vorgesehenen Bereich des Layouts angezeigt werden, wird erhöht)
Mod4 + Shift + l		Decrease number of master clients
Mod4 + Control + h	Increase number of columns for non-master windows by 1.
Mod4 + Control + l	Decrease number of columns for non-master windows by 1.
Mod4 + space		Switch to next layout
Mod4 + Shift + space	Switch to previous layout.
Mod4 + Ctrl + space	Toggle client floating status.
Mod4 + Ctrl + Return	Swap focused client with master.
Mod4 + Ctrl + 1-9	Toggle tag view.
Mod4 + Shift + 1-9		Tag client with tag.
Mod4 + Shift + Ctrl + 1-9	Toggle tag on client: Client wird auf verschiedenen 

