+++
title = "ip"
+++


Zeigt an und verändert Einstellungen zu Routing, Netwerkgeräten, Schnittstellen und Tunnels

## Anwendungsbeispiele

### Statische Adresse

IP-Adressen auflisten:

```sh
sudo ip address show
```

Statische IP-Adresse erstellen:

```sh
sudo ip address add <address/prefix_len> broadcast + dev <interface>
```

Bspw:

```sh
sudo ip address add 192.168.1.123/24 broadcast + dev enp0s20f0u3
```

Statische IP-Adresse löschen:

```
sudo ip address del <address/prefix_len> dev <interface>
```