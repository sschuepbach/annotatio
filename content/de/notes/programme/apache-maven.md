+++
title = 'Apache Maven'
+++


## Installation

* Binärdatei von Website
* Entpacken und kopieren nach ``/usr/local``
* Ergänzung der ``$PATH``-Umgebungsvariable:

	export PATH=$PATH:/usr/local/apache-maven/bin


## Dokumentation

### Kernkonzepte

* **Goal**: Spezifische Aufgabe, die entweder einzeln oder mit anderen Goals als Teil eines grösseren Builds ausgeführt wird. Goals können über Properties konfiguriert werden, welche entweder via POM oder direkt in der Kommandozeile gesetzt werden können
* **Lifecycle Phases**: Ein Lifecycle bezeichnet den gesamten Zyklus der Softwareerstellung. Maven umfasst einen Standard-Lifecycle mit einem Set an aufeinanderfolgenden Phasen, an die jeweils verschiedene Goals geknüpft sind. Wird eine bestimmte Phase ausgeführt, werden die Goals aller Phasen bis und mit der auszuführenden aufgerufen.
* **Plugin**: Eine Sammlung von einem oder mehreren Goals


### Neues Maven-Projekt

#### Über die Kommandozeile
	mvn archetype:create -DgroupId=<groupid> \
	-DartifactId=<artifactid> \
	-DpackageName=<packagename>


Dies erstellt das Projektverzeichnis inklusive eines rudimentären POM.

* ``archetype``: Der verwendete Archetyp. Beim Archetyp archetype handelt sich um einen sehr simplen Typ, welcher ein Hello-World-Skript in einer Datei App.java erstellt.
* ``create``: Das „goal“ des Maven-Kommandos. Mit create wird ein Projekt basierend auf dem angegebenen Archetyp erstellt
* ``DgroupId:`` Die Group-ID: Namespace für Maven-Projekte
* ``DartifactId:`` Name des Maven-Projektes.
* ``DpackageName``: Name des Java-Packets


#### In IntelliJ

1. File > New > Project > Maven
2. Optional kann ein Archetyp geladen werden
3. ``GroupId``, ``ArtifactId`` und ``Version`` angeben
4. Projektname und -pfad angeben


### POM
Erlaubte Top-level Elemente gemäss [POM-Modelversion 4.0](https://maven.apache.org/pom.html):
	<project xmlns="http://maven.apache.org/POM/4.0.0"
	  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
						  http://maven.apache.org/xsd/maven-4.0.0.xsd">
	  <modelVersion>4.0.0</modelVersion>
	 
	  <!-- The Basics -->
	  <groupId>...</groupId>
	  <artifactId>...</artifactId>
	  <version>...</version>
	  <packaging>...</packaging>
	  <dependencies>...</dependencies>
	  <parent>...</parent>
	  <dependencyManagement>...</dependencyManagement>
	  <modules>...</modules>
	  <properties>...</properties>
	 
	  <!-- Build Settings -->
	  <build>...</build>
	  <reporting>...</reporting>
	 
	  <!-- More Project Information -->
	  <name>...</name>
	  <description>...</description>
	  <url>...</url>
	  <inceptionYear>...</inceptionYear>
	  <licenses>...</licenses>
	  <organization>...</organization>
	  <developers>...</developers>
	  <contributors>...</contributors>
	 
	  <!-- Environment Settings -->
	  <issueManagement>...</issueManagement>
	  <ciManagement>...</ciManagement>
	  <mailingLists>...</mailingLists>
	  <scm>...</scm>
	  <prerequisites>...</prerequisites>
	  <repositories>...</repositories>
	  <pluginRepositories>...</pluginRepositories>
	  <distributionManagement>...</distributionManagement>
	  <profiles>...</profiles>
	</project>

