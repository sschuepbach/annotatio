+++
title = 'Neovim-Konfiguration mit Lua'
date = '2022-08-19'
+++


Ab Neovim v0.5 steht eine [Lua]({{< ref "/notes/programmiersprachen/Lua.md" >}})-API zur Konfiguration der Applikation zur Verfügung. Dazu wird `init.vim` durch `init.lua` ersetzt. Ausführliche Hilfe mit `:h lua`.

## Ressourcen

* https://github.com/nanotee/nvim-lua-guide
* https://mattermost.com/blog/how-to-install-and-set-up-neovim-for-code-editing/
* https://icyphox.sh/blog/nvim-lua/
* https://learnxinyminutes.com/docs/lua/
* https://rsdlt.github.io/posts/rust-nvim-ide-guide-walkthrough-development-debug/
