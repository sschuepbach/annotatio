+++
title = 'Übersicht über benutzte Plugins'
date = '2022-06-24'
+++


## Allgemeines

### fugitive.vim

[GitHub](https://github.com/tpope/vim-fugitive)


### vim-gitgutter

[GitHub](https://github.com/airblade/vim-gitgutter)


### surround.vim

[GitHub](https://github.com/tpope/vim-surround)


### vinegar.vim

[GitHub](https://github.com/tpope/vim-vinegar)


### Emmet-vim

[GitHub](https://github.com/mattn/emmet-vim)


### Tagbar

[GitHub](https://github.com/majutsushi/tagbar)


### NERD Commenter

[GitHub](https://github.com/preservim/nerdcommenter)


### vim-easymotion

[GitHub](https://github.com/easymotion/vim-easymotion)


### GDB for neovim

[GitHub](https://github.com/sakhnik/nvim-gdb)


### Conquer of Completion (CoC)

[GitHub](https://github.com/neoclide/coc.nvim)


### vim-header

[GitHub](https://github.com/alpertuna/vim-header)


### vimwiki

[GitHub](https://github.com/vimwiki/vimwiki)


### fzf

[GitHub](https://github.com/junegunn/fzf.vim)


### UltiSnips

[GitHub](https://github.com/SirVer/ultisnips)


## Themes

### palenight.vim

[GitHub](https://github.com/drewtempelmeyer/palenight.vim)


### monochrome

[GitHub](https://github.com/fxn/vim-monochrome)


### gruvbox

[GitHub](https://github.com/morhetz/gruvbox)


### Everforest

[GitHub](https://github.com/sainnhe/everforest)


### vim-airline

[GitHub](https://github.com/vim-airline/vim-airline)


### vim-airline-themes

[GitHub](https://github.com/vim-airline/vim-airline-themes)
