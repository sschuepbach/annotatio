+++
bookCollapseSection = true
title = 'Vim'
+++


## Reguläre Ausdrücke in Vim


### Quantoren

#### Greedy

* ``* / \{}``: 0 oder mehr
* ``\+``: 1 oder mehr
* ``\=`` / ``\?``: 0 der 1
* ``\{n,m}``: n bis m
* ``\{n}``: Genau m
* ``\{n,}``: Mindestens n
* ``\{,m}``: Höchstens m


#### Non-greedy

* ``\{-n,m}``: n bis m
* ``\{-n}``: Genau n
* ``\{-n,}``: Mindestens n
* ``\{-,m}``: 0 bis m
* ``\{-}``: 0 oder mehr


### Lookaround-Assertionen
In Vim sind folgende Lookaround-Assertionen möglich:

* *Lookbehind*: ``(muster)\@<=``
* *Negativer Lookbehind*: ``(muster)\@<!``
* *Lookahead*: ``(muster)\@=``
* *Negativer Lookahead*: ``(muster)\@!``


Beispiel Lookbehind:
``:%s/\(look\)\@<=behind/ahead/g``

Ersetzt lookbehind durch look*ahead*; lockbehind wird nicht ersetzt

Beispiel negativer Lookbehind:
``:%s/\(look\)\@<!behind/ahead/g``

Ersetzt lockbehind durch lock*ahead*; lookbehind wird nicht ersetzt

Beispiel Lookahead:
``:%s/look\(behind\)\@=/lock/g``

Ersetzt lookbehind durch *lock*behind; lookahead wird nicht ersetzt

Beispiel negativer Lookahead
``:%s/look\(behind\)\@!/lock/g``

Ersetzt lookahead durch *lock*ahead; lookbehind wird nicht ersetzt

Ressourcen:

* [regular-expressions.info: Lookaround](http://www.regular-expressions.info/lookaround.html)
* [Lookbehind / Lookaround Regex in Vim](https://www.inputoutput.io/lookbehind-lookahead-regex-in-vim/)


## Vimdiff

Befehle:

* [``c``: Zur vorherigen Änderungsstelle
* ``]c``: Zur nächsten Änderungsstelle
* ``do``: Ändert den aktuellen Buffer, um die Stelle an diejenige des anderen Buffers anzugleichen. Wenn eine andere Range oder ein anderer Buffer (bei Bufferzahl >2) gewählt werden muss: ``:[range]diffg[et] [bufspec]``
* ``dp``: Ändert den anderen Buffer, um dessen Stelle an diejenige des aktuellen Buffers anzugleichen. Wenn eine andere Range oder ein anderer Buffer (bei Bufferzahl >2) gewählt werden muss: ``:[range]diffp[ut] [bufspec]``


## Vimscript


* <https://unix.stackexchange.com/questions/8101/how-to-insert-the-result-of-a-command-into-the-text-in-vim>
* <http://learnvimscriptthehardway.stevelosh.com/>
* <http://vimdoc.sourceforge.net/htmldoc/usr_41.html#function-list>
* <https://www.ibm.com/developerworks/library/l-vim-script-1/index.html>
* <https://www.ibm.com/developerworks/library/l-vim-script-1/index.html>


## Plugins

Einige nützliche Links:

* <http://net.tutsplus.com/sessions/vim-essential-plugins/>
* [vundle](https://github.com/gmarik/vundle)
* [vim-phpqa](http://www.vim.org/scripts/script.php?script_id=3980)
* [phpcomplete](http://www.vim.org/scripts/script.php?script_id=3171)
* [vdebug](http://www.vim.org/scripts/script.php?script_id=4170)
* [nerdtree](http://www.vim.org/scripts/script.php?script_id=1658)
* [taglist](http://www.vim.org/scripts/script.php?script_id=273)
* [php.vim--Garvin](http://www.vim.org/scripts/script.php?script_id=2874)
* [PHP-Indenting-for-VIm](http://www.2072productions.com/to/phpindent.txt)
* [vim-latex](http://vim-latex.sourceforge.net/)
* [EasyMotion](http://www.vim.org/scripts/script.php?script_id=3526)
	* Hilft der einfacheren Navigation im Text
	* Mit \\w können die folgenden Wörter mit einem bestimmten Charakter direkt angesteuert werden
	* Mit \\f{Charakter} kann ein bestimmter auf die Position des Cursors folgender Charakter angesteuert werden
* [SuperTab-continued](http://www.vim.org/scripts/script.php?script_id=1643)
* [minibufexpl](http://vim.sourceforge.net/scripts/script.php?script_id=159)
* [sparkup](https://github.com/rstacruz/sparkup)
* [surround](http://www.vim.org/scripts/script.php?script_id=1697)
* [snipMate](http://www.vim.org/scripts/script.php?script_id=2540)
* [tComment](http://www.vim.org/scripts/script.php?script_id=1173)
* [xmledit](http://www.vim.org/scripts/script.php?script_id=301)
* [bash-support](http://www.vim.org/scripts/script.php?script_id=365)
* [AutoComplPop](http://www.vim.org/scripts/script.php?script_id=1879)
* [OmniCppComplete](http://www.vim.org/scripts/script.php?script_id=1520)
* [pythoncomplete](http://www.vim.org/scripts/script.php?script_id=1542)


## Weitere Ressourcen


* [Vim Regular Expressions 101](http://www.vimregex.com/)


