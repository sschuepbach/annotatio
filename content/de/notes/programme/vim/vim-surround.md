+++
title = 'vim-surround'
+++


## Syntax

``[Modus][Selektion][{Tag/Zeichen alt (bei cs / ds)}][{Tag/Zeichen neu (bei cs / ds)}]``

**Modi**:

* ``ys``: Neu
* ``cs``: Ändern
* ``ds``: Löschen


**Selektion:**

* Normale Selektionen
* ``s``: Ganze Zeile


**Tag/Zeichen:**

* Klammern (``[]``, ``{}``, ``()``)
	* Sich öffnende Klammer: Klammern mit Leerzeichen
	* Sich schliessende Klammer: Klammern ohne Leerzeichen
* ``<``: (Nur bei ``ys``/``cs``) Neuer Tag
* ``t``: (Nur bei ``cs``/``ds``) Ändern / löschen von Tag 


## Beispiele

* ``ys3iw)``: Dieses und nächstes Wort mit runden Klammern umschliessen
