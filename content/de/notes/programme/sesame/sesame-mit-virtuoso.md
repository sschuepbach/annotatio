+++
title = 'Sesame mit Virtuoso'
date = '2015-08-19'
+++


## Installation

1. [Sesame installieren](../Sesame.md)
2. ([Virtuoso installieren](../Virtuoso.md))
3. Sesame Provider von Virtuoso [herunterladen](http://virtuoso.openlinksw.com/dataspace/doc/dav/wiki/Main/VOSDownload#Sesame%20Provider) (virt_sesame2.jar und virtjdbc4.jar)
4. In Maven  folgende Dependencies definieren:

	<dependencies>
	
	    <dependency>
	        <groupId>commons-logging</groupId>
	        <artifactId>commons-logging</artifactId>
	        <version>1.2</version>
	    </dependency>
	
	    <dependency>
	        <groupId>org.slf4j</groupId>
	        <artifactId>slf4j-api</artifactId>
	        <version>1.7.12</version>
	    </dependency>
	
	    <dependency>
	        <groupId>org.slf4j</groupId>
	        <artifactId>slf4j-jdk14</artifactId>
	        <version>1.7.12</version>
	    </dependency>
	
	    <dependency>
	        <groupId>org.openrdf.sesame</groupId>
	        <artifactId>sesame-repository-sparql</artifactId>
	        <version>2.8.5</version>
	    </dependency>
	
	    <dependency>
	        <groupId>com.openlinksw.virtuoso</groupId>
	        <artifactId>virt-sesame</artifactId>
	        <version>1.25</version>
	        <scope>system</scope>
	        <systemPath>${basedir}/lib/virt_sesame2.jar</systemPath>
	    </dependency>
	
	    <dependency>
	        <groupId>com.openlinksw.virtuoso</groupId>
	        <artifactId>virtjdbc</artifactId>
	        <version>3.84</version>
	        <scope>system</scope>
	        <systemPath>${basedir}/lib/virtjdbc4.jar</systemPath>
	    </dependency>
	
	</dependencies>

Die Version der jar-Files kann folgendermassen abgefragt werden:
	java -jar virtjdbc4.jar



## Entwicklung

Zugangsdaten:
// String repoHost = "jdbc:<virtuoso://sb-ls1.swissbib.unibas.ch:1111>";
String repoHost = "<http://sb-ls1.swissbib.unibas.ch:8890/sparql>";
String repoUser = "swissbib";
String repoPwd = "12swissbib34";

String[] esNodes = {"localhost:9300", "localhost:9301", "localhost:9302"};
String esClustername = "linked-swissbib";
String index = "testsb3";
String[] types = {"bibliographicResource"};


