+++
title = 'Sesame'
bookCollapseSection = true
date = '2015-03-04'
+++


## Übersicht

### Sesame
<http://rdf4j.org>

Sesame is a powerful **Java framework for processing and handling RDF data**. This includes **creating, parsing, storing, inferencing and querying over such data**. It offers an easy-to-use API that can be connected to all leading RDF storage solution


* Sesame offers two out-of-the-box RDF databases (the in-memory store and the native store), and in addition many third party storage solutions are available:
	* [Ontotext GraphDB](http://www.ontotext.com/products/ontotext-graphdb/)
	* [CumulusRDF](https://code.google.com/p/cumulusrdf/)
	* [Stardog](http://www.stardog.com/)
	* [Systab Bigdata](http://www.bigdata.com/)
	* Oracle DB (durch [Adapter](http://docs.oracle.com/cd/E18283_01/appdev.112/e11828/sem_sesame.htm))
	* [Virtuoso](http://virtuoso.openlinksw.com/dataspace/doc/dav/wiki/Main/VirtSesame2Provider)
	* [Strabon](http://www.strabon.di.uoa.gr/)
* The framework offers a large scala of tools to developers to leverage the power of RDF and related standards.
* Sesame fully supports the SPARQL 1.1 query and update languages for expressive querying and offers transpararent access to remote RDF repositories using the exact same API as for local access.
* Finally, Sesame supports all mainstream RDF file formats, including RDF/XML, Turtle, N-Triples,  N-Quads, JSON-LD, TriG and TriX.


### AliBaba
AliBaba is a Sesame-based RDF application library for developing complex RDF storage applications. AliBaba is the next generation of the Elmo codebase. It is a collection of modules that provide simplified RDF store abstractions to accelerate development and facilitate application maintenance.

AliBaba includes many features not found in Sesame core to facility building complex, modern RDF applications.

An API that allows for mapping Java classes onto ontologies and for generating Java source files from ontologies. This makes it possible to use specific ontologies like RSS, FOAF and the Dublin Core directly from Java.
(<http://en.wikipedia.org/wiki/Sesame_%28framework%29>)


## Installation

### Voraussetzungen

* [OpenJDK](./OpenJDK.md)
* Apache Tomcat


### Sesame

* <http://sourceforge.net/projects/sesame/files/latest/download?source=files>


## Betrieb (Webinterface)

1. Tomcat aufstarten
2. Als Webinterface
	1. Workbench: <http://localhost:8080/openrdf-workbench>
	2. Übersicht: <http://localhost:8080/openrdf-sesame>
3. [Als API](./Sesame/Sesame_mit_Apache_Maven.md)


## Dokumentation

### Sesame

* [Sesame Server, Workbench, and Command Console: installation and use](http://rdf4j.org/sesame/2.8/docs/using+sesame.docbook?view)
* [Programming with the Sesame APIs](http://rdf4j.org/sesame/2.8/docs/programming.docbook?view)
* [System documentation](http://rdf4j.org/sesame/2.8/docs/system.docbook?view)
* [Sesame 2.8 upgrade notes](http://rdf4j.org/sesame/2.8/docs/articles/upgrade-notes.docbook?view)
* [API Javadoc](http://rdf4j.org/sesame/2.8/apidocs/)


### AliBaba

* [User documentation](https://bitbucket.org/openrdf/alibaba)
* [API Javadoc](http://rdf4j.org/alibaba/2.0/apidocs/)
* [AliBaba Messaging Ontology](http://rdf4j.org/alibaba/2.0/owldocs/messaging-ontology.xhtml?view)


