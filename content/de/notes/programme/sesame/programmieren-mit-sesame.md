+++
title = 'Programmieren mit Sesame'
date = '2015-03-17'
+++


## Ein Repository-Objekt erstellen

Zentrales Interface der Repository-API: Repository-Interface. Verschiedene Implementationen dieses Interfaces - die wichtigsten:

* org.openrdf.repository.sail.SailRepository: Funktioniert direkt auf SAIL. Meistens gebraucht, wenn auf ein lokales Sesame-Repository zugegriffen bzw. ein solches erstellt werden soll. Wichtig: Das Verhalten des Repository ist abhängig von den SAIL(S), auf welchen es basiert. Bspw. wird RDF Schema oder OWL nur dann unterstützt, wenn der SAIL stack entsprechende Inferencer beinhaltet.
* org.openrdf.repository.http.HTTPRepository: Wird gebraucht, um über HTTP auf ein entferntes Repository zuzugreifen.
* org.openrdf.repository.sparql.SPARQLRepository: Dient als Proxy für einen entfernten SPARQL-Endpoint (unabhängig davon, ob der Endpoint mit Sesame implementiert ist oder nicht)


The constructor of the SailRepository class accepts any object of type Sail:

* CustomGraphQueryInferencer
* DirectTypeHierarchyInferencer
* Federation
* ForwardChainingRDFSInferencer
* LimitedSizeNativeStore
* MemoryStore
* MySqlStore
* NativeStore
* NotifyingSailBase
* NotifyingSailWrapper
* PgSqlStore
* RdbmsStore
* SailBase
* SailWrapper


