+++
title = 'GnuPG'
+++


## Erneuern eines Schlüssels

1. ID des abgelaufenen Schlüssels herausfinden: ``gpg --list-secret-keys``
2. In Editor wechseln: ``gpg --edit-key <id>``
3. Mit dem Befehl ``expire`` ein neues Ablaufdatum setzen.
4. Dasselbe mit Subkeys wiederholen. Dafür Subkey mit ``key <1..>`` selektieren, anschliessend mit ``expire`` neues Datum setzen
5. Editor verlassen (``quit``)


=> [Pro und Kontra einer Erneuerung des Schlüssels](https://unix.stackexchange.com/a/177310)

