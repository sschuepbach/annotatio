+++
title = 'Knora v2'
+++

Neuerungen:

* JSON-LD als Austauschformat, später auch XML geplant (Format via Content Negotiation)
* Elemente von schema.org verwendet
* ``schema:Thing`` = ``knora:Ressource``
* Möglichst generische (d.h. auch möglichst wenige) Anfrage- und Antwortformate => Verschiedene Routen verwenden dasselbe Antwortformat: eine Liste von Ressourcen mit ihren Werten
* Verschiedene Routen liefern mehr oder weniger Daten zurück, das Format bleibt aber dasselbe
* Externes und internes Format stärker entkoppeln
* Externes Format in zwei Varianten: simple und complex


KnarQL:

* Subset von SPARQL 1.1 mit Knora-spezifischen Typannotationen
* KnarQL wird via URL-Parameter an Knora-Route gesandt, Antwort in JSON-LD anstelle von Triples (hybrides System zwischen API und SPARQL-Endpoint)
* KnarQL besteht immer aus einer ``CONSTRUCT``-Query
* Knora übersetzt KnarQL für Backend in SPARQL (via RDF4J (ex-Sesame))
* Ermöglicht komplexe Suchen über mehrere Ebenen von Ressourcen


Nächste Schritte in Entwicklung:

* Schreibender Zugriff auf Daten (im Moment zur lesbar)
* Ontologien über die API erzeugen und ändern
* Unterstützung der IIIF Presentation-API


Fragerunde:

* Es wäre grundsätzlich möglich, eine direkte Route zum Triplestore zu definieren und damit SPARQL-Abfragen zuzulassen
* Zusätzlicher Key-Value-Store für weiteren State
* Wie können angepasste Präsentationsoberflächen für einzelne Projekte ins DaSCH überführt werden. Hintergrund: Custom-GUIs (bspw. Präsentationsoberfläche für Editionen) können im DaSCH nicht unterhalten werden.
* Visionen Knora:
	* Vision LDP (Linked Data Platform) für Knora: REST-API, doch offen für beliebige Ontologien
	* Knora Simplified Data Model (mit API als Transmissionsriemen)?
	* Andere Möglichkeiten?


