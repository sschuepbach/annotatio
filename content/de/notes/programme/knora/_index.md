+++
bookCollapseSection = true
title = 'Knora'
+++

Knora (Knowledge Organization, Representation, and Annotation) is a software framework for storing, sharing, and working with humanities data.

Folgende Komponenten sind in Knora enthalten:

* Knora-Ontologien
* Knora API Server
* [Sipi](./Sipi.md)
* [SALSAH](./SALSAH.md) GUI


## Knora-Ontologien

Knora Base Ontology: <https://raw.githubusercontent.com/dhlab-basel/Knora/develop/knora-ontologies/knora-base.ttl>

In Knora müssen alle Daten zu einem bestimmten Projekt gehören. Projekte müssen eine ``kb:knoraProject`` definieren, welche folgende Eigenschaften aufweisen kann (Kardinalität in Klammern):

* ``shortname ``(1)
* ``basepath ``(1)
* ``foaf:name ``(0-1)
* ``description ``(0-1)
* ``belongsTo ``(0-1)


### Values
Werte in Knora werden reifiziert, wobei die Oberklasse ``kb:Value ``folgende Unterklassen kennt:

* ``kb:TextValue``
* ``kb:DateValue``
* ``kb:IntValue``
* ``kb:ColorValue``
* ``kb:DecimalValue``
* ``kb:UriValue``
* ``kb:BooleanValue``
* ``kb:GeomValue``
* ``kb:GeonameValue``
* ``kb:IntervalValue``


Charakterisierung:

* Die Knora-Ontologie verwendet für Eigenschaften vieler Klassen eine Reihe von Restriktionen (bspw. Kardinalität). Restriktionen werden also nicht generell für eine Property definiert, sondern beziehen sich auf die spezifische Verwendung einer Property im Kontext einer bestimmten Klasse. Bsp.: 

	:StillImageRepresentation rdf:type owl:Class ;
	                          rdfs:subClassOf
									[ rdf:type owl:Restriction ;
									  owl:onProperty :hasStillImageFileValue ;
									  owl:minCardinality "1"^^xsd:nonNegativeInteger
									] ;


* Knora speichert Metadaten über Beziehungen zwischen Ressourcen. Daher werden Links zwischen Ressourcen normalerweise reifiziert


## Knora API Server



## Ressourcen


* Code: <https://github.com/dhlab-basel/Knora>
* Website: <http://www.knora.org/>


