+++
title = 'Anaconda'
+++


## Umgebung erstellen

	sudo conda create --name <envname> <zu_installierende_pakete>

Anconda erstellt eine neue *environment* unter ``/opt/anaconda3/envs/<envname>`` erstellt. Anschliessend kann die Umgebung folgendermassen in der Kommandozeile aktiviert werden:
	source activate <envname>

Und um die Umgebung wieder zu verlassen:
	source deactivate

Alle Pakete auflisten:
	conda list

Zusätzliche Pakete installieren:
	conda install <paketname>
y

