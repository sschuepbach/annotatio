+++
title = 'IntelliJ IDEA'
+++


## Nützliche Shortcuts

| Tastenkürzel           | Beschreibung                                                                                              |
|:-----------------------|:----------------------------------------------------------------------------------------------------------|
| Alt + Insert           | Kontextmenü öffnen                                                                                        |
| Ctrl + Shift + A       | Befehlssuche                                                                                              |
| Alt + Home             | Auf Navigationsleiste springen (Navigation auf Leiste mit Pfeiltasten); mit ESC auf Editor zurückspringen |
| Alt + 1                | Projektstruktur-Fenster                                                                                   |
| Alt + 2                | Favoriten-Fenster                                                                                         |
| Alt + 6                | TODO-Fenster                                                                                              |
| Alt + 7                | Struktur-Fenster                                                                                          |
| Alt + rechts/links     | Navigation über die Editor-Reiter                                                                         |
| Ctrl + Shift + Alt + S | Projektstruktur-Einstellungen                                                                             |
| Ctrl + N               | Gehe zu Klasse                                                                                            |
| Shift + F10            | Ausführen                                                                                                 |
| Shift + F6             | Objekt umbennen                                                                                           |


### Debugging
| Tastenkürzel      | Beschreibung                             |
|:------------------|:-----------------------------------------|
| Shift + F9        | Start Debugging Session                  |
| Ctrl + F8         | Breakpoint auf selektierter Linie setzen |
| Ctrl + Shift + F8 | Breakpoints anzeigen                     |


