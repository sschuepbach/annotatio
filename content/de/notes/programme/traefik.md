+++
title = 'Traefik'
+++


## Configuration

* Dynamic routing configuration aka *dynamic configuration*:
	* Gets configuration from providers
	* Contains everything that defines how the requests are handled
	* Can change and is hot-reloaded
* Startup configuration aka *static configuration*:
	* Set up connections to providers
	* Defines the entrypoints
	* Can be in a configuration file (as ``toml`` or ``yaml``, default in ``/etc/traefik``) or declared as command-line arguments or environment variables


## Provider

Existing infrastructure component, wheter orchestrator, container engine, cloud provider, or key-value store. Traefik queries the provider's API in order to find relevant information about routing, and to update the routes as soon as a change is detected.

### Docker

* Works for Docker Engine and Docker Swarm Mode (``providers.docker.swarmMode=true``)
* Listens via endpoint (e.g. ``providers.docker.endpoint=tcp://127.0.0.1:2375``)
* Configuration via labels


Routing:
``traefik.http.routers.<my-container>.rule=Host(`mydomain.com`)``

Ports detection works as follows:

* If a container exposes only one port, then Traefik uses this port for private communication.
* If a container exposes multiple ports, or does not expose any port, then you must manually specify which port Traefik should use for communication by using the label ``traefik.http.services.<service_name>.loadbalancer.server.port``


Providers have the prefix ``providers`` in the configuration.

## Entrypoint

Listens for incoming traffic by defining the port which will receive the requests (wheter HTTP or TCP).

Define port: ``entryPoints.<name>.address = :<port>``

Entrypoints have the prefix ``entryPoints`` in the configuration.


## Router

Analyses the requests (host, path, headers, SSL etc.) and connects them to the services that can handle them. In the process, routers may use pieces of middleware to update the request, or act before forwarding the request to the service.


## Service

Forwards the request to your services (load balancing etc.)


## Middleware

May update the request or make decisions based on the request (authentication, rate limiting, headers etc.)

