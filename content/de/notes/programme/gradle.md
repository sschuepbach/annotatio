+++
title = 'Gradle'
+++


* [Website](https://gradle.org)
* [User guide](https://docs.gradle.org/current/userguide/userguide.html)
* [Groovy DSL](https://docs.gradle.org/current/dsl/)


## Neues Projekt

``gradle init``: Startet neues Projekt. Dies führt zu folgender Struktur:
	.
	|-- build.gradle
	|-- gradle
	|   |-- wrapper
	|       |-- gradle-wrapper.jar
	|       |-- gradle-wrapper.properties
	|-- gradlew
	|-- gradlew.bat
	|-- settings.gradle

Wobei:

* ``build.gradle``: Projektkonfigurationsskript
* ``gradle-wrapper.jar``: Gradle Wrapper JAR
* ``gradle-wrapper.properties``: Gradle Wrapper-Konfiguration
* ``gradlew``: Gradle Wrapper-Skript für unixoide Systeme
* ``gradlew.bat``: Gradle Wrapper-Skript für Windows
* ``settings.gradle``: Konfigurationsskript für Einstellungen, welche Projekte am Build beteiligt sind


Gradle stellt für die Projektkonfiguration eine Groovy- und eine Kotlin-DSL zur Verfügung.

## Projects

Die [Project](https://docs.gradle.org/4.7/dsl/org.gradle.api.Project.html)-Klasse erlaubt den programmatischen Zugriff auf Gradle. Es besteht eine Eins-zu-eins-Beziehung zwischen ``Project`` und ``build.gradle``.

## Settings

[Settings](https://docs.gradle.org/4.7/dsl/org.gradle.api.initialization.Settings.html) definiert die Konfiguration, welche zur Instanzierung und Konfiguration der Hierarchie von ``Project``-Instanzen benötigt wird. D.h., ``Settings`` sind Metaeinstellungen des *build*. Es besteht eine Eins-zu-eins-Beziehung zwischen ``Settings`` und ``settings.gradle``.

## Tasks

Ein Projekt besteht grundsätzlich aus einer Reihe von [Tasks](https://docs.gradle.org/4.7/dsl/org.gradle.api.Task.html), von welchen jeder eine bestimmte basale Operation ausführt (bspw. Komplieren von Klassen, einen Unittest laufen lassen, eine JAR-Datei komprimieren etc.). Programmatisch wird ein neuer Task durch die Methode ``create()`` der Klasse ``TaskContainer`` erstellt.

## Dependencies



## Plugins


* [Jar](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.bundling.Jar.html)
* [Scala](https://docs.gradle.org/current/userguide/scala_plugin.html)
* [Shadow](http://imperceptiblethoughts.com/shadow/)


