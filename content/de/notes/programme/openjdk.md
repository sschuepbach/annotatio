+++
title = 'OpenJDK'
+++


## Installation

* Installation via Paketverwaltung:
    ```bash
    sudo apt-get install openjdk-7-jre icedtea-7-plugin openjdk-7-jdk openjdk-7-source openjdk-7-demo openjdk-7-doc openjdk-7-jre-headless openjdk-7-jre-lib
    ```

* Definieren der Umgebungsvariablen in ``linked-swissbib:Applikationen:HOME.zshenv``
    ```bash
    export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
    export JRE_HOME=$JAVA_HOME
    ```

* Ergänzung der $PATH-Umgebungsvariable:
    ```bash
    export PATH=$PATH:$JAVA_HOME/bin
    ```