+++
title = 'GNU Debugger'
+++


## Befehle

Starten mit Kommandozeilenargumente für das Programm:
	gdb --args <programmname> <programmargumente>


In der Konsole:

* ``r`` / ``run``: Starten des Programms
* y


