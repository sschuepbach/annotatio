+++
title = 'Git'
+++


## git branch

### Wichtige Befehle

* ``git branch -m <branch>``: Branch umbenennen$
* ``git branch -t <branch>``: Für neuen Branch auch entfernten Branch aufsetzen
* ``git branch --edit-description <branch>``: Erklärenden Text zu Branch editieren
* ``git branch --contains <commit>``: Nur Branches aufführen, die ``commit`` enthalten (``HEAD`` falls nicht definiert).
* ``git branch --no-contains <commit>``: Nur Branches aufführen, die ``commit`` nicht enthalten (``HEAD`` falls nicht definiert).
* ``git branch --merged <commit>``: Nur Branches aufführen, die von ``commit`` erreicht werden können
* ``git branch --no-merged <commit>``: Nur Branches aufführen, die nicht von ``commit`` erreicht werden können


## git checkout

* Ein Klon checkt nur den ``master``-Branch aus. Um einen anderen remote Branch auszuchecken, genügt ein ``git checkout <Name des entfernten Branches>``
* ``git checkout`` kann auch dazu benutzt werden, um einzelne Dateien in einem bestimmten Commit auszuchecken: ``git checkout <commit> <file>``, oder die Kurzform ``git checkout <file>`` für ``git checkout HEAD <file>``


## git cherry

Findet Commits, die noch nicht in upstream integriert wurden.
	git cherry [-v] [<upstream> [<head> [<limit>]]]


Beispiel:
``git cherry -v master <branch>`` - Zeige alle Commits von ``branch``, die noch nicht in ``master`` gemergt wurden

## git log

* ``git log <base-branch>..<branch>``: Commits anzeigen, die in branch, aber nicht in ``<base-branch>`` vorhanden sind


## git reset

* ``git reset`` setzt ``HEAD`` in einen bestimmten Status zurück
* Letzter Commit löschen: ``git reset –hard „HEAD^“``


## git remote

*Verwaltet die registrierten Repositorien (*[Manpage](https://git-scm.com/docs/git-remote)*).*
	git remote add <name> <url> # Hinzufügen eines neuen entfernten Repositorium


## Anmerkungen zu Branches und Commits

* ``HEAD`` verweist im Normalfall auf einen benannten Branch (und nicht auf einen spezifischen, beispielsweise den letzten, Commit!)
* Wird hingegen ein früherer Commit in einem Branch geöffnet, verweist ``HEAD`` direkt auf diesen Commit. Dies wird auch als „detached HEAD state“ bezeichnet
* Wichtig ist, dass ein Commit, der auf Änderungen basiert, die ihrerseits nicht aus dem neusten Commit in einem Branch abgeleitet werden, von nichts referiert werden können. Um dies zu ändern, gibt es drei Möglichkeiten:
	* ``git checkout -b foo`` # Abgeleitete Commits werden in einem neuen Branch foo abgelegt, ``HEAD`` verweist neu auf diesen Branch
	* ``git branch foo`` # Gleich wie oben, ``HEAD`` befindet sich aber noch im „detached state“
	* ``git tag foo`` # Neuer Tag foo, welcher sich auf den letzten Commit bezieht. ``HEAD`` verbleibt im „detached state“


### Siehe auch
[Start a new branch on your remote git repository](https://www.zorched.net/2008/04/14/start-a-new-branch-on-your-remote-git-repository/)

