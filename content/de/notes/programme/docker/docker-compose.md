+++
title = 'Docker Compose'
+++


## Reference (v3)

*: Ignored in swarm mode
': Ignored in compose mode

### Service

* [build](https://docs.docker.com/compose/compose-file/#build): Configuration options that are applied at build time
	* [context](https://docs.docker.com/compose/compose-file/#context): Either a path to a directory containing a Dockerfile, or a url to a git repository
	* [dockerfile](https://docs.docker.com/compose/compose-file/#dockerfile): Alternate Dockerfile
	* [args](https://docs.docker.com/compose/compose-file/#args): Add build arguments, which are environment variables accessible only during the build process
	* [cache_from](https://docs.docker.com/compose/compose-file/#cache_from): A list of images that the engine uses for cache resolution
	* [labels](https://docs.docker.com/compose/compose-file/#labels): Add metadata to the resulting image using Docker labels. Either an array or a dictionary
	* [shm_size](https://docs.docker.com/compose/compose-file/#shm_size): Size of the ``/dev/shm`` partition for this build's containers. Either numerical (bytes) or as a string expressing a byte value
	* [target](https://docs.docker.com/compose/compose-file/#target): Build the specified stage as defined inside the ``Dockerfile``.
* [cap_add, cap_drop](https://docs.docker.com/compose/compose-file/#cap_add-cap_drop):
* [cgroup_parent](https://docs.docker.com/compose/compose-file/#cgroup_parent):
* [command](https://docs.docker.com/compose/compose-file/#command)
* [configs](https://docs.docker.com/compose/compose-file/#configs)
* [container_name](https://docs.docker.com/compose/compose-file/#container_name)
* credential_spec
* depends_on
* deploy
	* endpoint_mode
	* labels
	* mode
	* placement
	* replicas
	* resources
	* restart_policy
	* rollback_config
	* update_config
* devices
* dns
* dns_search
* domainname
* entrypoint
* env_file
* environment
* expose
* external_links
* extra_hosts
* healthcheck
* hostname
* image
* init
* ipc
* isolation
* labels
* links
* logging
* mac_address
* network_mode
* networks
	* aliases
	* ipv4_address, ipv6_address
* pid
* ports
* privileged
* read_only
* restart
* secrets
* security_opt
* shm_size
* stdin_open
* stop_grace_period
* stop_signal
* sysctls
* tmpfs
* tty
* ulimits
* user
* userns_mode
* volumes
* working_dir


### Volumes

* driver
* driver_opts
* extrenal
* labels
* name


### Networks

* driver
* driver_opts
* attachable
* enable_ipv6
* ipam
* internal
* labels
* external
* name


