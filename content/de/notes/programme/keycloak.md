+++
title = 'Keycloak'
+++


## Securing Applications and Services

Keycloak supports both OpenID Connect and SAML 2.0

OIDC use cases:

* Application that asks the Keycloak server to authenticate a user for them. After a successful login, the application will receive an *identity token* and an *access token*. The *identity token* contains information about the user such as username, email, and other profile information. The *access token* is digitally signed by the realm and contains access information (like user role mappings) that the application can use to determine what resources the user is allowed to access on the application.
* A client that wants to gain access to remote services. In this case, the client asks Keycloak to obtain an *access token* it can use to invoke on other remote services on behalf of the user. Keycloak authenticates the user then asks the user for consent to grant access to the client requesting it. The client then receives the *access token*. This *access token* is digitally signed by the realm. The client can make REST invocations on remote services using the *access token*. The REST service extracts the *access token*, verifies the signature of the token, then decides based on access information within the token whether or not to process the request.


SAML use cases:

* An application that asks the Keycloak server to authenticate a user for them. After a successful login, the application will receive an XML document that contains something called a SAML assertion that specifies various attributes about the user. This XML document is digitally signed by the realm and contains access information (like user role mappings) that the application can use to determine what resources the user is allowed to access on the application.
* A client that wants to gain access to remote services. In this case, the client asks Keycloak to obtain a SAML assertion it can use to invoke on other remote services on behalf of the user.



**OAuth**: Open standard for access delegation, commonly used as a way for Internet users to grant websites or applications access to their information on other websites but without giving them the passwords.
**SAML** (Security Assertion Markup Language): Open standard for exchanging authentication and authorization data between parties, in particular, between an identity provider and a service provider. The single most important use case that SAML addresses is web-browser single sign-on (*SSO*).
**OpenID**: Open standard and decentralized authentication protocol.
**OpenID Connect **(**OIDC**): Authentication layer on top of OAuth 2.0, which allows computing clients to verify the identity of an end-user based on the authentication performed by an authorization server, as well as to obtain basic profile information about the end-user in an interoperatble and REST-like manner. For that purpose OpenID Connect specifies a RESTful HTTP API, using JSON as a data format (JWT).

Realm
Client
Client Scope
Roles
Identity Providers
User Federation
Authentication



* <https://community.containo.us/t/forwardauth-openid-keycloak/1788/4>
* <https://stackoverflow.com/questions/56902664/keycloak-jupyterhub-auto-signon-using-jwt-token>
* <https://geek-cookbook.funkypenguin.co.nz/recipes/keycloak/setup-oidc-provider/>
* <https://aboullaite.me/secure-kibana-keycloak/>


