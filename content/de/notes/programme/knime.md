+++
title = 'KNIME'
+++


* Integration und gezielte, mehrstufige Bearbeitung heterogener Daten
* Basale Suite, zahlreiche Erweiterungen vorhanden
* Einfaches Austauschen von Workflows


## Kernelemente von KNIME

* **Nodes** / **Metanodes**: Einzelner Prozessschritt (zahlreiche Typen) (Metanodes: Alias für mehrere Nodes, zur besseren Übersicht)
* **Workflow**: Datenanalysesequenz / Pipeline


``F6``: Knoten-Einstellungen
``F7``: Knoten ausführen
``Shift`` + ``F6``: Resultat anzeigen
``Shift`` + ``F7``: Alle Knoten ausführen

