+++
title = 'VuFind'
date = '2015-05-27'
+++


 Every search performed by VuFind involves three key classes:

* The **search options class**, which loads static options from configuration files. This determines what types of searches are legal.
* The **search parameters class**, which represents the settings for the current search – query, page size, facet list, etc. This class has routines for processing user input, or all parameters can be set individually by code. A search options object is used for validation.
* The **search results class**, which actually performs searches (usually with the help of a search parameters object, though it also has some static methods for retrieving records by ID) and turns them into an array of record drivers.



Fragen zu Vufind:

* Wie neue Abhängigkeiten installieren? Über globale composer.json-Datei?
* Was ist der ClassMapAutoloader -> s. [Webentwicklung mit Zend Framework](./Zend_Framework_2/zendframework2.leanpub.pdf) 2, S. 38ff.
* Wie funktioniert die Bedienung über die Konsole [(]()Swissbib:Module.php)?
* Options - params - results-Triple?
* Wo befinden sich die view-templates in LinkedSwissbib?




Debugging-Session in PHPStorm:

* In Firefox: localhost/vfrd/Exploration/Search?lookfor=hello
* In PHPStorm: Button Start listening for PHP Debug Connections



## Workshop 8. Juli 2015

config/application.config.php:

* Um merge-Konflikte zu vermeiden, können in der httpd-vufind.conf weitere custom Module angegeben werden.


Themes in Vufind:
Themes sind voneinander abgeleitet. Bspw. linkedswissbib / sbvfrdjus / sbvfrdmulti -> sbvfrd -> bootstrap3 -> root

Konfigurationsdateien: config/vufind
**Die Konfigurationsdateien in diesem Verzeichnis sollten nicht überschrieben werden!**
Um Konfigurationsdateien zu überschreiben, können im Verzeichnis local/config/vufind entsprechende Dateien abgelegt werden. Solche Konfigurationsdateien werden auch nicht in git erfasst. Die lokale config.ini muss daher manuell erstellt oder durch das Skript geladen werden.

Probleme beim Swissbib-Repository. Bsp.: Beim Swissbib-Repository ist in der config die Holding IDSBB = 1 gesetzt. Dies ist aber nur für Swissbib Basel/Bern relevant, nicht aber für Classic Swissbib und Linked Swissbib.

Das Layout von Vufind basiert auf Bootstrap, und dieses wiederum auf less (eine Programmiersprache, die kompiliert css ergibt). Vufind kompiliert diese less-Dateien mithilfe eines PHP-Skripts (util/cssBuilder.php), welche die less-Verzeichnisse der Themes durchgeht und die vorhandenen less-Dateien übersetzt (in die Datei css/compiled.css im jeweiligen Template-Verzeichnis).

ViewHelper: Methoden, welche PHP-Code für das Template beinhalten, um die Template-Dateien übersichtlich zu halten.

