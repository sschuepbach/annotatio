+++
title = "Argo CD"
date = "2022-08-22"
+++


Argo CD ist ein deklaratives, auf GitOps-Konzepten (nicht nur) basierenden Tool für
Kubernetes. 

* Incubator in Cloud Native Computing Foundation 
* First class in OpenShift
* Nur eine Komponente des Argo-Projekts:
  * Argo Workflows (DAG-basierte Workflow engine, v.a. eingesetzt mit
    BigData-Bereich)
  * Argo Events (Event-driven Workflow-Automatisierung, geht Hand in Hand mit
    Argo Workflows)
  * Argo Rollouts (Unterstützt deklarative progressive Delivery-Strategie,
    v.a. Ergänzung zu ArgoCD, welches nur das kann, was Kubernetes kann.
    Rollouts unterstützt darüber hinaus weitere Strategien, wie bspw.
    Blue-Green-Deployments, Canary-Deployments etc.)


Git-Repositories als Source of Truth

* Läuft im Cluster selbst (cloud native, hat bspw. keine Datenbanklösung)
* Tracks resources 24/7 (operator style reconcile loop)

* Supports any git scm
* Supports any kubernetes cluster (also running on openshift etc.)

Weitere Features:
* _Hooks_ run jobs on Argo CD state changes
* _Actions_ definieren benutzerdefinierte Aktionen (bspw. restarts)
* _Notification_, welche an eine Drittlösung wie Slack, webhooks etc. geschickt werden können

## Komponenten

* GUI
* API: Zentrale Komponente, welche mit allen anderen Komponenten interagiert
  (auch mit anderen Teilen des Argo-Projekts)
* Repo Service
* Controllers: application controller, der die out-of-sync-Checks macht;
  notification controller etc.

## Template Tooling

* Bundles preferred versions of its supported templating tools
  * Helm
  * kustomize
  * ks
  * jsonnet
* Config management plugins can be used to support more tools or pin versions

## Weitere Features

* Pod-Konfiguration kann direkt in ArgoCD angepasst werden. Der
  (automatisierte) Sync-Prozess sollte solche Änderungen allerdings gleich
  wieder überschreiben.

## Kommandozeile

```sh
argocd app list

argocd app get my-app

argocd app set my-app -p image.tag=v1.0.1
```

## App-of-Apps

Declaratively specify an Argo CD app that consists of other apps

## Tipps und Tricks

* Alle Argo CD-Konfigurationen in einem Git-Repository verwalten
* Sich auf wenige Standards beschränken. Empfohlen Helm + raw manifests
* Eindeutige Image-Tags verwenden (Deployment nur von Tags)
  * Image-Tags mit SHA-Summe erstellen (für eher agilen Prozess)
  * Jeweils manuell Tags auch in dev-Branch (oder, falls dev-Branch fehlt,
    auch in  main/master) setzen, bspw. 0.1.2-alpha1
* `targetRevision` in dev-Umgebung mit wildcards arbeiten
* tags können automatisiert mittels git commit-messages erstellt werden

## Chart museum

Chart museum anstelle von OCI:

`helm cm-push ...`

Von Argo CD kann anschliessend auf das Chart Museum zugegriffen werden

Ein zentrales Helm Chart-Repository (Museum), aber Helm-Charts werden in
App-Repos verwaltet => Prüfen, ob das GitLab unterstützt.

## Ressourcen

* [Argo CD Website](https://argoproj.github.io/)
* [Dokumentation](https://argo-cd.readthedocs.io/en/stable/)
