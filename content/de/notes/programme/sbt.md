+++
title = 'sbt'
date = '2015-09-17'
+++


## sbt Build Definitionen

sbt-Definitionen können in einer ``build.sbt`` (im root-Verzeichnis des Projekts), in ``*.scala``-Dateien (unter ``/project``) oder in einer Kombination von beiden abgelegt werden.

### Setup
	root/
	.gitignore (should contain target/)
	build.sbt
	project/
	<can contain .sbt or .scala files for build>
	src/
	main/
	resources/
	<files to include in main jar here>
	scala/
	    <main Scala sources>
	java/
	<main Java sources>
	test/
	resources/
	    <files to include in test jar here>
	scala/
	<test Scala sources>
	java/
	<test Java sources>


### build.sbt
Siehe auch Kapitel [.sbt build definition](https://www.scala-sbt.org/1.x/docs/Basic-Def.html) in der offiziellen Dokumentation.

``build.sbt`` definiert ein Projekt, welches eine Liste von Scala expressions enthält, die Settingsgenannt werden. Jede Setting ist eine Sequenz aus Schlüssel-Wert-Paaren.

#### Schlüssel
Ein Schlüssel ist eine Instanz von einer der folgenden Klassen, wobei T dem erwarteten Typ des Wertes entspricht:

* ``SettingKey[T]``: Werden beim laden des Projektes prozessiert und für weiteren Gebrach gespeichert (Beispiele: ``name``, ``organization``)
* ``TaskKey[T]``: Ihr Wert bei jedem Ausführen des Projektes neu berechnet (Beispiele: ``console``, ``compile``)
* ``InputKey[T]``: Identisch zu task keys mit dem Unterschied, dass sie Kommandozeilen-Argumente als Input haben (``run``, ``runMain``, ``testOnly``, ``testQuick``)


Schlüssel verfügen über folgende Methoden, die jeweils ein Objekt vom Typ ``Setting[T]`` (im Fall einer Instanz von ``SettingKey[T]``) oder vom Typ ``Setting[Task[T]]`` (im Fall einer Instanz von ``TaskKey[T]``) zurückgeben:
| Operator | Zweck                                                            | Beispiel                          |
|:---------|:-----------------------------------------------------------------|:----------------------------------|
| ~=       | Modifiziert den vorher gesetzten Wert                            | name ~= {n => "project"+n}        |
| +=       | Fügt einem existierenden Wert einen neuen hinzu                  |                                   |
| ++=      | Fügt eine Sequenz von Werten einem existierenden Wert hinzu      |                                   |
| <<=, <<  | Setzt einen Wert basierend auf dem Wert eines anderen Schlüssels | version <<= name {n => n +"-1.0"} |
| :=       | Setzt neuen Wert oder ersetzt einen alten                        |                                   |


Es ist auch möglich, eigene Schlüssel zu definieren. 

##### Built-In Keys
Eingebaute Schlüssel sind Felder des Objekt Keys (sbt.Keys), die beim Aufruf von build.sbt implizit importiert werden und ohne Klassenpfad aufgerufen werden können.

**Settings Keys**
| Name                | Zweck                                                                                 | Typ                   |
|:--------------------|:--------------------------------------------------------------------------------------|:----------------------|
| name                | Projektname                                                                           | String                |
| organization        | Organisations/Gruppen-ID                                                              | String                |
| version             | Version des Moduls                                                                    | String                |
| scalaVersion        | Scala-Version                                                                         | String                |
| isSnapshot          | Snapshot-Version                                                                      | Boolean {false}       |
| offline             | sbt soll ohne Netzwerk-Verbindung arbeiten, wenn möglich                              | Boolean {false}       |
| resolvers           | Zusätzliche URI für automatisch verwaltete Abhängigkeiten                             | Seq[Resolver]         |
| libraryDependencies | Verwaltete Abhängigkeiten                                                             | Seq von Ivy-Modul-IDs |
| parallelExecution   | Aufgaben parallel ausführen                                                           | Boolean {true}        |
| publishTo           | resoolver, auf den das Projekt publiziert werden soll                                 |                       |
| publishMavenStyle   | POM publizieren (ansonsten eine Ivy-Datei)                                            | Boolean {false?}      |
| pollInterval        | Interval, in dem im getriggerten Modus nach modifizierten Quellen gesucht werden soll | Int                   |
| scalaSource         | Scala-Source-Verzeichnis                                                              | File                  |
| javaSource          | Java-Source-Verzeichnis                                                               | File                  |
| sourceDirectories   | Alle Source-Verzeichnisse (verwaltet und nicht-verwaltet)                             | Seq[File]             |


Für Beispiele von Task Keys und Input Keys siehe Kommandozeilenbefehle.

##### Eigene Schlüssel
Es können auch eigene Schlüssel mit ihrer jeweiligen erstellenden Methode definiert werden, ``settingKey``, ``taskKey`` oder ``inputKey``. Jeder Methode muss der Typ des Wertes sowie eine Beschreibung mitgegeben werden. Der Name des Schlüssels wird vom Name des val abgeleitet, dem der Schlüssel zugeordnet wird. Beispiel:
``lazy val hello = taskKey[Unit]("An example task")``

#### Scopes
Jeder Schlüssel kann mehrere Werte haben, allerdings nur in verschiedenen *Scopes*. In vielen Fällen ist der Scope impliziert, so dass er nicht speziell angegeben werden muss.

Ein Scope kann auf drei verschiedenen Ebenen definiert werden:

* Projekt (*Project*): Für einen Build, der mehrere Projekte umfasst. In diesem Fall sollte jedes Projekt über eine eigene .sbt-Datei mit projektspezifischen Einstellungen verfügen (globale Einstellungen können aber trotzdem in einer globalen .sbt-Datei definiert werden)!
* Konfiguration (*Configuration*): Eine Konfiguration ist ein Modul, in welchem prozessbezogene Quellen, Pakete etc. gruppiert sind. Alle Schlüssel, die mit Kompilierung, Paketierung und Runs zu tun haben, müssen allenfalls in jeder Konfiguration anders funktionieren. Weitverbreitete SBT-Konfigurationen sind
	* ``Compile``: Pfade für Kompilierungs-Aufgaben
	* ``Test``: Pfade für testbezogene Aufgaben
	* ``Runtime``: Pfade für Aufgaben zur Laufzeit
* Aufgaben (*Task*): Settings befinden sich im Scope eines bestimmten Tasks. Settings können also für verschiedene Tasks verschiedene Werte haben


Ein spezieller Wert ist *Global*, der zur Anwendung kommt, wenn kein Scope explizit angegeben ist.

Um einen Schlüssel einem Scope zuzuweisen, muss der Operator ``in`` benutzt werden. Bsp.:
	parallelExecution in Global :=			// Project-Scope
	parallelExecution in Test := false		// Configuration-Scope
	parallelExecution in testOnly := false	// Task-Scope

Um einen Scope-spezifischen Wert im interaktiven Modus zu testen (camelCase Notation wird im interaktiven Modus durch Bindestriche ersetzt):
``test:parallel-execution``

Und für den task-level:
``test-only::parallel-execution``

## Kommandozeilenbefehle

sbt kann im batch-Modus oder in einem interaktiven Modus bedient werden. Zum Start des interaktiven Modus ``sbt`` in der Kommandozeile eingeben.
	
	{sbt} clean		# Löschen der durch sbt generierten Dateien
	{sbt} compile		# Projekt kompilieren
	{sbt} test		# Tests laufen lassen
	{sbt} testOnly		# Nur angegebene Tests werden ausgeführt (oder alle, wenn keine angegeben werden). Wildcards möglich
	{sbt} testQuick		# Wie testOnly, Tests werden aber nur unter bestimmten Bedingungen ausgeführt (Fehler, geänderte Abhängigkeiten)
	{sbt} run		# Projekt starten
	{sbt} console		# ScalaREPL starten (mit Projekt im classpath)
	{sbt} doc		# Generiert die API-Dokumentation für das Projekt
	{sbt} packageBin	# Erstellt ein main-Artefakt (binäre jar-Datei)
	{sbt} packageSrc	# Erstellt ein source-Artefakt (Jar mit Quellcode und Resourcen)
	{sbt} update		# Löst Abhängigkeiten auf und importiert sie falls nötig ins Projekt
	{sbt} publish		# Veröffentlicht Artefakte in einem Repository
	{sbt} publishLocal	# Veröffentlicht Artefakte in einem lokalen Repository
	{sbt} reload		# Lädt die build-Definition von Neuem
	{sbt} help		# Zeigt die Hilfe zu einem Befehl an


### Zusätzliche Befehle im Interaktiven Modus
	!		# Hilfe zu den History-Befehlen anzeigen
	!!		# Letzter Befehl nochmals ausführen
	!:		# Alle vorherigen Befehle anzeigen
	!:n		# n letzte Befehle anzeigen
	!n		# Befehl mit Index n ausführen, wie vom !:-Befehl angezeigt
	!-n		# Den n-ten Befehl vor diesem Befehl ausführen
	!string		# Den letzten Befehl ausführen, der mit string startet
	!?string	# Den letzten Befehl ausführen, der string enthält


**Hinweise**

* Eine Tilde (``~``) vor einem Befehl bewirkt, dass der Befehl bei jedem Speichern iterativ ausgeführt wird. Im interaktiven Modus kann das iterative Ausführen mit <Return> beendet werden
* Im Batch-Modus müssen Befehle mit Argumente in Anführungs- und Schlusszeichen geschrieben werden. Bsp.: ``sbt clean compile "testOnly TestA TestB"``


