+++
title = 'Jupyter'
+++


## Bedienung

Jupyter kennt zwei Modi für eine Zelle: *Edit mode* (grüner Seitenbalken) und *command mode* (blauer Seitenbalken). Mit Enter gelangt man in den edit mode, mit Esc gelangt man in den command mode.

### Beide Modi: Wichtige shortcuts
Strg + Enter: Zelle ausführen
Alt + Enter: Zelle ausführen, neue Zelle unterhalb
Shift</key+<key>Enter: Zelle ausführen, eine Zelle nach unten
Strg + Shift: Speichern

### Command mode: Wichtige shortcuts

#### Navigation

↑ / K: Eine Zelle nach oben
↓ / J: Eine Zelle nach unten
F: Finden und ersetzen
Shift + ↑ / K: Zelle oberhalb zusätzlich markieren
Shift + ↓ / J: Zelle unterhalb zusätzlich markieren

#### Zellen editieren
Alt: Neue Zelle oberhalb
B: Neue Zelle unterhalb
X: Zelle ausschneiden
Strg: Zelle kopieren
V: Zelle unterhalb einfügen
Shift + V: Zelle oberhalb einfügen
DD: Zelle löschen
M: Zelle zu Markdown
Y: Zelle zu Codeh

#### Notebook
00: Kernel neustarten
H: Liste mit shortcuts

### Edit mode: Wichtige shortcuts
Tab: Autovervollständigung
Shift + Tab: Tooltip
Strg + ]: Einrücken
Strg + [: Ausrücken
Strg + Z: Rückgängig machen
Strg + Y: Nochmals machen
Strg + →: Ein Wort rechts
Strg + ←: Ein Wort rechts
Strg + Shift + -: Zelle aufteilen

## Zusätzliche Kernels

<https://github.com/ipython/ipython/wiki/IPython-kernels-for-other-languages>
Die verschiedenen zusätzlichen Kernels sind standardmässig manuell in [/usr/local](file:///usr/local) installiert.

### Spark-Kernel
<https://github.com/ibm-et/spark-kernel/wiki/Getting-Started-with-the-Spark-Kernel>

Aufstarten des Apache Spark-Kernels:
[/usr/local/spark-kernel/dist/spark-kernel/bin/spark-kernel](file:///usr/local/spark-kernel/dist/spark-kernel/bin/spark-kernel)
Kernel-Optionen unter [~/.ipython/kernels/spark/kernel.json](file:///home/seb/.ipython/kernels/spark/kernel.json)

Hinweis:
Die aktuelle Version des Kernels (0.1.5.) läuft nur mit Apache Spark 1.5.1. Daher referenziert die lokale Umgebungsvariable $SPARK_HOME in der Startup-Datei [/usr/local/spark-kernel/dist/spark-kernel/bin/spark-kernel](file:///usr/local/spark-kernel/dist/spark-kernel/bin/spark-kernel) auf die Spark-Instanz [/usr/local/spark-1.5.1.](file:///usr/local/spark-1.5.1.) Dies muss wahrscheinlich bei einem Update des Kernels geändert werden (und damit kann auch die alte Version von Spark gelöscht werden).

### IJulia-Kernel
<https://github.com/JuliaLang/IJulia.jl>

