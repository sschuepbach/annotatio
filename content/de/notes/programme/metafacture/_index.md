+++
bookCollapseSection = true
title = 'Metafacture'
date = '2015-03-12'
+++


* Das grundlegende Strukturelement sind **Literale**. Sie erlauben es einen Datenwert mit einem Namen zu versehen. Dieser macht die Bedeutung des Datenwert deutlich und dient dazu, den Datenwert gezielt auswählen zu können.
* Um komplexere Strukturen modellieren zu können, gibt es **Entitäten**. Entitäten fassen mehrere Literale und auch andere Entitäten zusammen. Sie sind ebenso wie Literale benannt, um die Bedeutung der in der Entität enthaltenen Literale und Entitäten auszudrücken und sie auswählbar zu machen.
* Der **Datensatz** ist das äußerste Strukturelement des Metamorph-Datenmodells. Er fasst Entitäten und Literale zusammen. Um den Datensatz identifizieren zu können, kann er mit einer Datensatz-ID versehen werden, die quasi als Name des Datensatzes fungiert.

(*Das Metamorph-Datenmodell, *<http://b3e.net/metamorph-book/latest/datamodel.html>)

Datensätze und Entitäten können ineinander umgewandelt werden. Wird ein Datensatz in eine Entität überführt, ist es sinnvoll, ihm einen festen Namen zu geben, da immer wieder die gleichen Entitäten in verschiedenen Datensätzen verwendet werden. Umgekehrt macht es Sinn, bei einer Umwandlung von Entitäten in Datensätze einen eindeutigen Namen zu vergeben, um so einen Datensatz in verschiedenen Rollen verwenden zu können (bspw. als Autor oder Herausgeber).


Angesichts dessen wurde beim Entwurf von Metamorph entschieden, das System für die Verarbeitung von schema-freien Daten – d. h. Daten, deren Struktur nicht in einer Ontologie definiert ist – auszulegen, und es robust gegenüber fehlerhaften oder unerwarteten Datenstrukturen zu gestalten.

Metamorph verwendet nur einen einzigen Datentyp - Zeichenfolgen. Hinweise auf Datentypen in der Form ^^IsoDate oder ^^xs:date sind nur als Hinweise an den Leser, nicht aber an den Rechner zu verstehen (obwohl natürlich ein entsprechender Interpreter implementiert werden könnte...)

## Metamorph

Metamorph ist die Metafacture-eigene DSL zur Transformation von Metadaten. Es handelt sich um ein XML-förmiges Skript, das eine [Reihe von spezifischen Tags](https://github.com/culturegraph/metafacture-core/wiki/Metamorph-functions) für Transformationsregeln zur Verfügung stellt.

<vars>		Definition von Variablen
<rules>	Definition von Transformationsregeln
<maps>	Mappings


**Damit in der Pipe die ID des Records abgefragt werden kann, muss _id gesetzt werden!**

## Ressourcen

[Metafacture-Wiki](https://github.com/culturegraph/metafacture-core/wiki)
[Vortrag Christoph Böhme](http://swib.org/swib13/slides/boehme_swib13_131.pdf)
[Metamorph: A Transformation Language for Semi-structured Data](http://dlib.org/dlib/may15/boehme/05boehme.html)
[Das Metamorph-Datenmodell](http://b3e.net/metamorph-book/latest/datamodel.html)

