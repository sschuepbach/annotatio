+++
title = 'Debugging FLUX-File'
date = '2015-03-19'
+++


Am Beispiel von ./examples/beacon/combine/combine-beacon.flux

## Neue Applikation hinzufügen

Run -> Edit Configurations
Alt + Insert -> Application

![](./pasted_image.png)

## Breakpoint setzen

Entweder:

* Am Schluss des FLUX-Durchlaufs
* Nach Ausführen eines FLUX-Befehls


### Am Anfang des FLUX-Durchlaufs


1. Ctrl-N -> Nach Flux suchen
2. Breakpoint auf loadCustomJars(); setzen


### Nach Ausführen eines FLUX-Befehls


1. Nachschlagen einer bestimmten FLUX-Funktion in der [Liste der FLUX-Befehle.](./FLUX-Befehle.md) Klassenname kopieren
2. Ctrl-N -> Klassenname gemäss 1.



## Debugging-Prozess starten

![](./pasted_image001.png)

