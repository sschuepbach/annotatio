+++
title = 'Metamorph'
date = '2015-07-09'
+++


## Übersicht Methoden und Programmablauf

public **Metamorph**(final String morphDef, final Map<String, String> vars, final InterceptorFactory interceptorFactory)
->
private static InputSource **getInputSource**(final String morphDef)
->
public **Metamorph**(final InputSource inputSource, final Map<String, String> vars, final InterceptorFactory interceptorFactory)
->
private void **init**()
->
protected void **dispatch**(final String path, final String value, final List<NamedValueReceiver> fallback)

* Ruft die Methode findMatchingData auf
* Liefert diese Variablennamen zurück, wird die Methode send aufgerufen

->
private List<NamedValueReceiver> **findMatchingData**(final String path, final List<NamedValueReceiver> fallback)

* Sucht in der Morph-Datei nach dem (Variablen-)Namen eines bestimmten Feldnamens in der Ursprungsdatei


private void **send**(final String key, final String value, final List<NamedValueReceiver> dataList)

* Registriert der Fundort der 



Daneben habe ich mir ein wenig Zeit genommen, die Mechanismen in der Metamorph Klasse besser zu verstehen. Zentral ist dabei eine Liste mit Objekten vom Typ NamedValueReceiver. Jedes dieser Objekte referenziert auf die Werte einer bestimmten in der Morph-Datei definierten Variable. 
XML-Attribute


## Befehle

metamorph
Root tag
Attribute:
entity 

