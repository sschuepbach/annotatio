+++
title = 'tmux'
date = '2016-01-14'
+++


## tmux-session für verschiedene user

Wenn sich verschiedene user an einer tmux-session beteiligen wollen, müssen alle zugriff auf das socket-file haben. Dazu muss sich das socket-file einer Gruppe gehören, in der alle user member sind (bspw. auf ls1: tmux-shared). Ist diese Gruppe erstellt, sind folgende Schritte notwendig (/tmp/tmux und linked-swissbib können natürlich durch beliebige Namen ersetzt werden):
	user 1: tmux -S /tmp/tmux new-session -t linked-swissbib
	user 1: sudo chgrp tmux-shared /tmp/tmux
	user 2: tmux -S /tmp/tmux attach -t linked-swissbib [-s EigeneSession]

Oder für direkten entfernten Zugriff
``ssh sls1 -t — 'tmux -S /tmp/tmux attach -t linked-swissbib [-s EigeneSession]``'

