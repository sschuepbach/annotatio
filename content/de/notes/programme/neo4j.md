+++
title = 'Neo4j'
date = '2015-10-01'
+++


## Graph

A **labeled property graph** is made up of *nodes*, *relationships*, *properties*, and *labels*.


* **Node**: Analog zu einem Dokument in einer dokumentenorientierten Datenbank. Knoten können eine beliebige Anzahl an Eigenschaften (Schlüssel-Wert-Paare) enthalten sowie mit einem oder mehreren Labels (Kategorien) bezeichnet werden
* **Relationship**: Beziehung zwischen zwei Knoten. Eine Beziehung ist immer gerichtet und muss einen Start- und einen Endknoten haben. Zudem können Beziehungen ebenfalls mit Eigenschaften versehen werden.
* **Property**: Eigenschaft eines Knotens oder einer Beziehung in Form eines Schlüssel-Wert-Paares. Schlüssel entsprechen immer dem Java-Typ String, Werte können Strings, primitive Typen oder Arrays dieser Typen sein
* **Label**: Kategorie eines Knotens


## Abfragesprachen

Neo4j unterstützt verschiedene Abfragesprachen, u.a.

* [Cypher](../Sprachen/Cypher.md)
* [SPARQL](../Sprachen/SPARQL.md)
* [Gremlin](https://github.com/tinkerpop/gremlin/wiki/)


