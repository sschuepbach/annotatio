+++
title = 'MariaDB'
+++


## Aufsetzen unter Archlinux

	sudo -s
	pacman -S mariadb
	mysql_install_db --user=mysql --basedir=/usr --datadir=<Pfad zum Datenverzeichnis>
	# Falls das Datenverzeichnis von /var/lib/mysql abweicht, muss in /etc/mysql/my.cnf in der Sektion [mysqld] datadir=<Pfad zum Datenverzeichnis> gesetzt werden
	systemctl start mariadb
	mysql_secure_installation # DB interaktiv konfigurieren


## Starten des Clients

	sudo systemctl start mariadb
	mysql -u root -p


## Datenbankverwaltung

### Erstellen einer neuen Datenbank
	CREATE DATABASE IF NOT EXISTS <db_name>;


Für weitere Informationen zum Erstellen von Datenbanken s. [Create Database](https://mariadb.com/kb/en/mariadb/create-database/) in der MariaDB-Dokumentation.

### Zu Datenbank navigieren
	USE <dbname>;


## Benutzerverwaltung

### Anlegen eines neuen Users
Im MariaDB-Client:
	CREATE USER '<username>'@'localhost' IDENTIFIED BY '<password>';


Für weitere Informationen zur Benutzerverwaltung s. [User Account Management](https://mariadb.com/kb/en/mariadb/user-account-management/) und insbesondere [CREATE USER](https://mariadb.com/kb/en/mariadb/create-user) in der MariaDB-Dokumentation.

### User-Privilegien
	GRANT <Privilegien> <Datenbank>.<Tabelle> TO '<username>'.'<host>';
	FLUSH PRIVILEGES;

Wobei Privilegien:

* ``ALL PRIVILEGES``: Alle Privilegien
* ``CREATE``: Erstellen von Tabellen oder Datenbanken
* ``DROP``: Löschen von Tabellen oder Datenbanken
* ``DELETE``: Löschen von Zeilen in Tabellen
* ``INSERT``: Einfügen von Zeilen in Tabellen
* ``SELECT``: Lesen von Daten
* ``UPDATE``: Modifizieren von Tabellenzeilen
* ``GRANT OPTION``: Modifizieren von Rechten für andere User


Für weitere Privilegien s. ``GRANT``-Befehl in [Dokumentation](https://mariadb.com/kb/en/mariadb/grant/).

Für Datenbank / Tabelle: Ein ``*`` dient als Wildcard.

### Alle Benutzer anzeigen
	SELECT DISTINCT user, host FROM mysql.user;


### Passwort neu setzen
	SET PASSWORD FOR <username>@<host> = PASSWORD('<newpassword>');


## Tabelle erstellen

	CREATE TABLE IF NOT EXISTS <dbname>(
	  <zeilenname> <typ> <weitere_attribute>,
	  PRIMARY KEY (<zeilenname>),
	  INDEX (<zeilename>, ...)
	)
	<tabellenattribute>;


Wichtige Seiten:

* [CREATE TABLE](https://mariadb.com/kb/en/create-table)
* [Indizes setzen](https://mariadb.com/kb/en/getting-started-with-indexes)
* [Charsets und Collations definieren](https://mariadb.com/kb/en/setting-character-sets-and-collations/)
* [Erlaubte Charsets und Collations](https://mariadb.com/kb/en/supported-character-sets-and-collations/)


## Import eines SQL-Dumps

	mysql -u <username> -p <dbname> < <sql-dump-Datei>


## Onlineressourcen


* [MariaDB-Website](https://mariadb.org/)
* [MariaDB / MySQL auf dem Archlinux Wiki](https://wiki.archlinux.org/index.php/MySQL)
* [Liste wichtiger SQL-Befehle](https://mariadb.com/kb/en/mariadb/basic-sql-statements)


