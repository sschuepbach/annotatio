+++
title = 'Graphviz'
date = '2016-01-27'
+++


Graphviz ist eine Sammlung von Programmen zur Erstellung von Graphen.

## Gerichtete und ungerichtete Graphen

Ein Graph kann entweder gerichtet (``digraph``) oder ungerichtet (``graph``) sein. Bei einem gerichteten Graph werden die Kanten mittels ``->``, bei einem ungerichteten mittels ``--`` designiert.

### Ungerichteter Graph
	graph {
		first_node -- second_node
		first_node[color="blue"]
	}


![](./diagram.png) 

### Gerichteter Graph
	digraph {
		subgraph mfwf{
			color=blue
			i[label="Input data"]
			o[label="Output data"]
			c1[label="MF-command 1"]
			c2[label="MF-command 2"]
			i->c1->c2->o
		}
		subgraph flux{
			style=filled;
			color=lightgrey;
			cmd1[label="cmd1 |"]
			cmd2[label="cmd2 |"]
		}
		cmd1->c1
		cmd2->c2
	}


![](./diagram001.png)

## Links

* [Elementattribute](http://www.graphviz.org/content/attrs)
* [Farbennamen](http://www.graphviz.org/content/color-names)
* [Knotenformen](http://www.graphviz.org/content/node-shapes)
* [Pfeilformen](http://www.graphviz.org/content/arrow-shapes)
* [Graphviz und Python](http://matthiaseisen.com/articles/graphviz/)


