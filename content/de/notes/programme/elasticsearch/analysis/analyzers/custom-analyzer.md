+++
title = 'Custom Analyzer'
date = '2015-04-14'
+++


## Settings

* ``tokenizer``: The logical / registered name of the tokenizer to use
* ``filter``: An optional list of logical / registered name of token filters
* ``char_filter``: An optional list of logical / registered name of char filters


## Further information

* <http://proquest.tech.safaribooksonline.de/book/web-development/search/9781449358532/10dot-index-management/custom_analyzers_html?bookview=search&query=custom+analyzer>


