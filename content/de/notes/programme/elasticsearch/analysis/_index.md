+++
bookCollapseSection = true
title = 'Analysis'
date = '2015-04-15'
+++


The [index analysis module](http://www.elastic.co/guide/en/elasticsearch/reference/1.5/analysis.html) acts as a configurable registry of [Analyzers](./Analysis/Analyzers.md) that can be used in order to both break indexed (analyzed) fields when a document is indexed and process query strings. It maps to the Lucene Analyzer.

