+++
title = 'Shield'
+++

Zur Zugriffsregelung auf unseren Elasticsearch-Cluster verwenden wir das Plugin Shield. Folgend einige Hinweise zur allgemeinen Konfiguration und spezifischen Einstellungen für unseren Cluster:

## Allgemeines

Die Einsatzbereiche von Shield sind u.a.:

* Benutzerauthentifizierung, 


## Konfigurationsdateien

Allgemeine Konfiguration: $ES_HOME/config/elasticsearch.yml
Rollen: $ES_HOME/config/shield/roles.yml


## Rollen linked-swissbib

**lsb_user**:

* Leserechte auf Indizes testsb*
* Für anonyme User

**lsb_edit**:

* transport_client-Rechte auf Cluster
* Schreibrechte auf Indizes testsb*
* Für Java client

**lsb_index**:

* create_index und manage auf Indizes testsb*
* Für Python put index script

**lsb_kibana4**


## User linked-swissbib

**lsb_tc**: Transport client
**lsb_ind**: Python script
**lsb_kibana4**: Kibana / Marvel / Sense

## Privilegien

<https://www.elastic.co/guide/en/shield/2.3/shield-privileges.html>

## User management in Native Realm

<https://www.elastic.co/guide/en/shield/2.3/native-realm.html>

## Konfiguration von Clients

* [Java Client](https://www.elastic.co/guide/en/shield/2.3/_using_elasticsearch_java_clients_with_shield.html)
* [Hadoop / Spark](https://www.elastic.co/guide/en/shield/2.3/hadoop.html)
* [Python Client](https://elasticsearch-py.readthedocs.org/en/master/#ssl-and-authentication)
* [HTTP/REST](https://www.elastic.co/guide/en/shield/2.3/_using_elasticsearch_http_rest_clients_with_shield.html)
* [Marvel](https://www.elastic.co/guide/en/shield/2.3/marvel.html)
* [Kibana](https://www.elastic.co/guide/en/shield/2.3/kibana.html)
