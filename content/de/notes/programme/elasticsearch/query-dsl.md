+++
title = 'Query DSL'
date = '2015-05-05'
+++


## Übersicht Queries

| Name                | Typ             | Beschreibung                                                                                                  | Methode                      |
|:--------------------|:----------------|:--------------------------------------------------------------------------------------------------------------|:-----------------------------|
| geo_shape           | Geo             |                                                                                                               | GeoShapeQueryBuilder         |
| geo_bounding_box    | Geo             |                                                                                                               | GeoBoundingBoxQueryBuilder   |
| geo_distance        | Geo             |                                                                                                               | GeoDistanceQueryBuilder      |
| geo_distance_range  | Geo             |                                                                                                               | GeoDistanceRangeQueryBuilder |
| geo_polygon         | Geo             |                                                                                                               | GeoPolygonQueryBuilder       |
| geohash_cell        | Geo             |                                                                                                               | GeohashCellQuery.Builder     |
| template            | Spezialisiert   |                                                                                                               | TemplateQueryBuilder         |
| span_term           | Positional      |                                                                                                               | SpanTermQueryBuilder         |
| span_multi_query    | Positional      |                                                                                                               | SpanMultiTermQueryBuilder    |
| span_first          | Positional      |                                                                                                               | SpanFirstQueryBuilder        |
| span_near           | Positional      |                                                                                                               | SpanNearQueryBuilder         |
| span_or             | Positional      |                                                                                                               | SpanOrQueryBuilder           |
| span_not            | Positional      |                                                                                                               | SpanNotQueryBuilder          |
| span_containing     | Positional      |                                                                                                               | SpanContainingQueryBuilder   |
| span_within         | Positional      |                                                                                                               | SpanWithinQueryBuilder       |
| wildcard            | Termbasiert     | Abgespeckte Regexp-Suche, in der ? und * verwendet werden können                                              | WildcardQueryBuilder         |
| limit               | Zusammengesetzt | Begrenzt die Zahl der Dokumente, die pro Shard durchsucht werden                                              | LimitQueryBuilder            |
| function_score      | Zusammengesetzt | Der zurückgegebene Score der Suche kann durch weitere Funktionen modifiziert werden                           | FunctionScoreQueryBuilder    |
| term                | Termbasiert     | Dokumente, die den exakten Term im angegebenen Feld enthalten                                                 | TermQueryBuilder             |
| terms               | Termbasiert     | Dokumente, die irgendeinen der angegebenen Terme im angegebenen Feld enthalten                                | TermsQueryBuilder            |
| exists              | Termbasiert     | Dokumente, in denen ein Feld exisitiert und keinen null-Wert hat                                              | ExistsQueryBuilder           |
| missing             | Termbasiert     | Dokumente, in denen ein Feld fehlt oder null ist                                                              | MissingQueryBuilder          |
| indices             | Zusammengesetzt | Eine Suche für einen eine andere für einen anderen Index durchführen                                          | IndicesQueryBuilder          |
| simple_query_string | Volltext        | Einfachere Form von query_string                                                                              | SimpleQueryStringBuilder     |
| nested              | Zusammenfügend  | Eingebettete (nested) Felder in einem Dokument durchsuchen                                                    | NestedQueryBuilder           |
| fuzzy               | Termbasiert     | Findet Dokumente, deren Terme höchstens eine bestimmte Levensthein-Distanz zum Suchterm aufweisen             | FuzzyQueryBuilder            |
| more_like_this      | Spezialisiert   | Findet Dokumente, die ähnlich zum angegebenen Text, Dokument oder Dokumentenkollektion sind                   | MoreLikeThisQueryBuilder     |
| match_all           | match_all       | Generische Suche über alle Felder                                                                             | MatchAllQueryBuilder         |
| has_parent          | Zusammenfügend  | Gibt Dokumente zurück, deren "Eltern" den Suchkriterien entsprechen                                           | HasParentQueryBuilder        |
| has_child           | Zusammenfügend  | Gibt Dokumente zurück, deren "Kinder" den Suchkriterien entsprechen                                           | HasChildQueryBuilder         |
| regexp              | Termbasiert     | Regexp-Suche nach Termen in Dokumenten                                                                        | RegexpQueryBuilder           |
| script              | Spezialisiert   | Skript, welches als Filter benutzt werden kann. Ähnlich zu function_score                                     | ScriptQueryBuilder           |
| match               | Volltext        | Standard für Volltextsuche (fuzzy matching, phrase / proximity matching)                                      | MatchQueryBuilder            |
| bool                | Zusammengesetzt | Standard für zusammengesetzte Suchen. Komponenten sind must, should, must_not, filter                         | BoolQueryBuilder             |
| prefix              | Termbasiert     | Suche nach Termen mit einem bestimmten Präfix                                                                 | PrefixQueryBuilder           |
| type                | Termbasiert     | Sucht nach Dokumenten mit spezifischem Typ                                                                    | TypeQueryBuilder             |
| ids                 | Termbasiert     | Sucht nach Dokumenten mit spezifischem Typ und ID                                                             | IdsQueryBuilder              |
| common_terms        | Volltext        | Teilt die Suche in häufigere (schwach gewichtet) und weniger häufige (stark gewichtet) Terme ein              | CommonTermsQueryBuilder      |
| range               | Termbasiert     | Terme in einer bestimmten Reichweite (Daten, Zahlen, Strings)                                                 | RangeQueryBuilder            |
| boosting            | Zusammengesetzt | Treffer können Score nicht nur positiv, sondern auch negativ beeinflussen                                     | BoostingQueryBuilder         |
| query_string        | Volltext        | Unterstützt die Suchstring-Syntax von Lucene                                                                  | QueryStringQueryBuilder      |
| dis_max             | Zusammengesetzt | Vereinigt verschiedene Suche, der Score-Wert ist gleich dem höchsten Score-Wert der einzelnen queries         | DisMaxQueryBuilder           |
| constant_score      | Zusammengesetzt | Wrapper für eine andere query, die im Filter-Kontext und mit konstantem Score als Rückgabe durchgeführt wird. | ConstantScoreQueryBuilder    |
| multi_match         | Volltext        | match-query über mehrere Felder                                                                               | MultiMatchQueryBuilder       |

	

## Suche nach Grösse von Listen / Arrays

	{ 
		"filter": {
			"script": {
				"script": "doc['<Feld>'].values.size()>=<Wert>"
			}
		}
	}


## Suche in Nested Documents

	{
		"query": {
			"nested": {
				"path": "<Object where the doc is nested>",
				"query": {
					"match": {
						"<Field of nested object>": "<value>"
					}
				}
			}
		}
	}

