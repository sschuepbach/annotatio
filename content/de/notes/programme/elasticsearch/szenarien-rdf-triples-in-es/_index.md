+++
bookCollapseSection = true
title = 'Szenarien RDF Triples in ES'
date = '2015-04-01'
+++


Wie sollen Triples in ES gespeichert werden?

* In der [kompakten Form von JSON-LD](../../Standards/JSON-LD.md)


Wie kann in ES gesucht werden?

* Via Mapping in ES


Wie werden Triples präsentiert?

* Für Maschinen: In der ausgeweiteten Form von JSON-LD
* Für Menschen: In der kompakten Form von JSON-LD



Anmerkungen zu Swissbib-Triples:

* Subjekt ist IRI auf Swissbib-Ressource
* Prädikat verlinkt auf externe Ontologien
* Objekt ist entweder ein Literal oder eine IRI (verlinkt mit Swissbib- oder externer Ressource)


