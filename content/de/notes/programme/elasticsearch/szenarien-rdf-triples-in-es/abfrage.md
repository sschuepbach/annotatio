+++
title = 'Abfrage'
date = '2015-06-02'
+++


Folgende Suchen sollen in linked.swissbib.ch möglich sein:

1. Einfache Suche: Einstellungen für _all-Feld in mapping definieren (insbesondere: Welche Felder sollen ausgeschlossen werden?); Implementierung einer Methode queryAll: 2. Wie muss nach einzelnen Wörtern (OR-Verknüpfung) in Dokumenten gesucht werden? => Match-Suche genügt
2. Boolesche Suche nach Begriffen (must, must_not, should) => multi_match-Suche
3. Filter nach Daten (range-Filter)
4. Filter nach Sprachen
5. Filter nach Formaten
6. Filter nach Bibliotheksverbünden
7. Kombination aus 2. und 3. - 6.


## Beispiel einfache Suche

	GET /testsb/bibliographicResource/_search
	{
	  "query": {
		"match": {
		  "_all": "beispiel suche"
		}
	  }
	}


## Beispiel erweiterte Suche

	GET /testsb/bibliographicResource/_search
	{
	  "query": {
		"bool": {
		  "should": [
			{
			  "match": {"dct:title": "test"}
			},
			{
			  "nested": {
				"path": "dc:contributor",
				"query": {
				  "match": {
					"foaf:firstName": "John"
				  }
				}
			  }
			}
		  ],
		  "must": [
			{
			  "range": {
				"dct:issued": {
				  "from": "1900",
				  "to": "2010"
				}
			  }
			}
		  ],
		  "minimum_number_should_match": 1
		}
	  }
	}


Anmerkungen:

* Es muss eine boolesche Abfrage durchgeführt werden. Sechs Szenarien für Bool-Blöcke sind dabei denkbar:
	* **should**: Suche nach Vereinigungsmenge ohne Filter
	* **must**: Suche nach Schnittmenge mit bzw. ohne Filter
	* **must,  must_not**: Suche nach Differenzmenge ohne Filter; Suche nach Differenzmenge mit Filter
	* **should, must_not**: Suche nach Differenzmenge ohne Filter
	* **should, must**: Suche nach Vereinigungsmenge mit Filter
	* **must, should, must_not**: Suche nach Differenzmenge mit Filter


