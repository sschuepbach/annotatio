+++
title = 'Wichtige API-Befehle'
date = '2015-05-20'
+++


Vollständige Curl-Anfrage
curl -X**POST** 'localhost:9200/**exampleindex/exampletype/_search?pretty=true**' -d
**{ "query": { "match": { "fieldname": "value" } } }**

Im Folgenden verwendete Kurzform
POST exampleindex/exampletype/_search?pretty=true { "query": { "match": { "fieldname": "value" } } }

## Dokumente

Anzahl Dokumente / Typ
GET indexname/typname/_count

Anzahl Dokumente / Index
GET indexname/_count

## Index

Detaillierte Liste
GET _cat/indices?v

Erstellen
PUT indexname

Erstellen mit Settings & Mappings:
	curl -XPOST localhost:9200/indexname --data-binary @datei; echo


## Cluster

Gesundheit
GET _cluster/health?pretty=true

Statistik
GET _cluster/stats?human&pretty

Status
GET _cluster/state

## Suche nach (Nicht-)Existenz von Feldern

	curl -XGET 'sb-ues1.swissbib.unibas.ch:8080/lsb_20170718/bibliographicResource/_search?pretty' -H 'Content-Type: application/json' -d '
	{
	 "size": 0,
	  "query": {
	        "bool":
	       {
	       "must_not":
	       {
	
	            "exists" : { "field" : "dct:rights" }
	      }
	    }
	}
	}'


## Bulk Upload

``curl -s -XPOST localhost:9200/_bulk --data-binary @<Datei>; echo``

