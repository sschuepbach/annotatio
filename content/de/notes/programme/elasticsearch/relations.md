+++
title = 'Relations'
date = '2015-04-22'
+++


## Übersicht

### Inner Object

* Easy, fast, performant
* Only applicable when one-to-one relationships are maintained
* No need for special queries

The problem with inner objects was that each nested JSON object is not treated as an individual component of the document. Instead they were merged with other inner objects sharing the same property names (because of the flattened file structure of ES / Lucene)

### Nested

* Nested docs are stored in the same Lucene block as each other, which helps read/query performance. Reading a nested doc is faster than the equivalent parent/child.
* Updating a single field in a nested document (parent or nested children) forces ES to reindex the entire nested document. This can be very expensive for large nested docs
* “Cross referencing” nested documents is impossible
* Best suited for data that does not change frequently

Nested documents remain independent of each other.

### Parent/Child

* Children are stored separately from the parent, but are routed to the same shard. So parent/children are slightly less performance on read/query than nested
* Parent/child mappings have a bit extra memory overhead, since ES maintains a “join” list in memory
* Updating a child doc does not affect the parent or any other children, which can potentially save a lot of indexing on large docs
* Sorting/scoring can be difficult with Parent/Child since the Has Child/Has Parent operations can be opaque at times


### Denormalization

* You get to manage all the relations yourself!
* Most flexible, most administrative overhead
* May be more or less performant depending on your setup


## Überlegungen

Zusammenhang herstellen zwischen bibliographischem Datensatz und Personendaten

1. Personendaten werden ins gleiche Dokument geladen. Dies kann grundsätzlich mit einem Nested Object bewerkstelligt werden. Ein Inner Object hingegen macht nur Sinn, wenn keine kombinierte Suche nach Personen möglich sein soll (wie bspw. nach allen Werken von Autoren mit Nachnamen Meier und Geburtsjahr 1942 - Treffer wären in diesem Beispiel auch Werke, in welchem eine Person mit Nachnamen Meier und eine andere mit Geburtsjahr 1942 mitgewirkt haben).
	* Vorteile:
		* Vorausgesetzt, die Personendaten sind in Inputdaten integriert, ist "Triage" einfach
		* Performanz bei Ausgabe: Es müsste nur ein Dokument pro bibliographischem Datensatz serialisiert werden
	* Nachteile:
		* Redundanz: Daten zur gleichen Person werden in vielen Dokumenten auftauchen
		* Datenpflege: Änderungen an den Personendaten müssten in vielen Dokumenten vorgenommen werden
		* Müssen passende Personendaten als externe Ressourcen eingebunden werden, ensteht ein Mehraufwand (Extraktion aus externer Ressource, Integration in den lokalen Datensatz)
2. Personendaten werden in den gleichen Index, aber in separate Dokumente mit eigenem Typ geladen. Dies kann grundsätzlich mit einer Parent-Child-Relation oder einer Denormalization bewerkstelligt werden
	* Vorteile:
		* Suche muss nur über einen Index gemacht werden
		* Es kann jeweils ein eigener Typ (und ein eigenes Mapping) für bibliographische Daten und Personendaten definiert werden
	* Nachteile:
		* Vorausgesetzt, die Personendaten sind in Inputdaten integriert, ist Triage vor Indexierung in ES aufwendig
3. Eigener Index für Personendaten. Dies kann nur mit einer Denormalization bewerkstelligt werden
	* Vorteile:
	* Nachteile:



## See also

* [Managing relations inside Elasticsearch](https://www.elastic.co/blog/managing-relations-inside-elasticsearch/)
* <https://groups.google.com/forum/#!topic/elasticsearch/aPiNgfuVwEQ>
* <http://stackoverflow.com/questions/27884360/elasticsearch-keep-redundant-denormalized-data-or-keep-a-list-of-ids-for-cros>
* <http://blog.comperiosearch.com/blog/2014/06/18/using-parentchild-relationships-document-security-elasticsearch/>
* <http://www.spacevatican.org/2012/6/3/fun-with-elasticsearch-s-children-and-nested-documents/>


