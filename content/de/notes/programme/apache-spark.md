+++
title = 'Apache Spark'
+++

Apache Spark is a fast and general engine for large-scale data processing. 

## Installation (Source Code)

1. [Download](https://spark.apache.org/downloads.html) des komprimierten Source Code
2. Entpacken der Datei
3. [Kompilieren:](https://spark.apache.org/docs/latest/building-spark.html)
	1. In der POM die folgenden Optionen der scala-maven-plugin setzen:
		1. ``<arg>-Xmax-classfile-name</arg>``
		2. ``<arg>128</arg``
	2. In project/SparkBuild.scala ergänzen: ``scalacOptions in Compile ++= Seq("-Xmax-classfile-name", "128"),``
	3. Maven goal generieren: ``./build/mvn -Pyarn -Phadoop-2.6 -Dhadoop.version=2.7.1 -Dscala-2.11 -Phive -Phive-thriftserver -DskipTests clean package``


