+++
title = 'Stardog'
date = '2015-10-06'
+++


## Neue Datenbank erstellen

``$STARDOG_HOME/bin/``stardog-admin db create -n myDB

## Daten hinzufügen über die CLI

``$STARDOG_HOME/bin/stardog data add myDatabase 1.rdf 2.rdf 3.rdf``

