+++
title = 'Salsah v2'
+++

## Vorbemerkungen

* Anspruch: Salsah soll auch eine Forschungsumgebung sein
* Im Knora-Repository gibt es einen Knora-Ordner, der allerdings der Salsah-Version 1.5 entspricht (= altes Layout)
* Dokumentation von Salsah2 ist nicht up-to-date


## Entwicklungsstand

* Arbeit hauptsächlich immer noch am Admin-Interface
* Administration von Users bereits implementiert
* Verwaltung von Ontologien erst im Entstehen begriffen
* Für ein Projekt wird es möglich sein, auch mehrere Ontologien zu erstellen
* Ontologien sollen auch projektübergreifend verwendet werden können (noch nicht implementiert)
* Schritt-für-Schritt-Anlegung von neuen Ressourcen: Noch sehr basal
* Verwendung von Open-Sea-Dragon für Bildansichten
* Video- und Audio-Annotation-Tool


