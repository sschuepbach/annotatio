+++
title = 'MongoDB'
+++


## Wichtige Shell-Befehle

	db							# Aktuelle Datenbank
	show dbs					# Alle Datenbanken
	show collections			# Collections der Datenbank
	use <db>					# Zu Datenbank db wechseln
	<db>.<collection>.find()	# Records in Collection anzeigen
	<db>.<collection>.insert( { <json-string> }	# Dokument einfügen (erstellt auch db und collection, falls nicht vorhanden)
	<...>.find().pretty() 		# Lesbarere Darstellung
	quit()						# Shell verlassen (oder: Ctrl-C)


### Weitere Ressourcen
[Shell-Parameter](https://docs.mongodb.com/manual/reference/program/mongo/#bin.mongo)
[Befehlsreferenz](https://docs.mongodb.com/manual/reference/mongo-shell/)

## Datenset in lokale Instanz von Mongo importieren

```bash
ssh -L 29017:localhost:29017 sdb4
mongoexport --host=localhost:29017 --db=<Name der Datenbank> --collection=<Name der Collection> -o<Dateiname>
sudo systemctl start mongodb.service
mongoimport --host=localhost:27017 --db=<Name der Datenbank> --collection=<Name der Collection> <Pfad zum Dateinamen>
```

