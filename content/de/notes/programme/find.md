+++
title = 'find'
+++


## Nach Dateitypen suchen

### Verzeichnisse aus Suche ausschliessen
	find . -path <Dateiname> -prune -o -name '*'


Alternativ kann auch mit grep gearbeitet werden:
	find | grep -v '*<Dateiname>*'


## Nach Zeitstempel suchen

	find -[Aktion][Zeiteinheit] [Bereich][Zeit]

Aktion:

* ``a`` (für ``atime``): Letzter Zugriff auf die Datei (schreibend oder lesend) (zu überprüfen mit ``ls -lu <Datei>``)
* ``c`` (für ``ctime``): Letzte Modifikation an der Datei oder ihren Attributen (zu überprüfen mit ``ls -lc <Datei>``)
* ``m`` (für ``mtime``): Letzte Modifikation an der Datei (zu überprüfen mit ``ls -l <Datei>``)


Zeiteinheit:

* ``time``: Tage
* ``min``: Minuten
* ``newer``: Neuer als Datei, bspw. ``find -anewer eintext.txt``


Bereich:

* (leer): Genau dann
* ``+``: Vor mehr als
* ``-``: Vor weniger als


Um einen endlichen Zeitbereich in der Vergangenheit zu definieren, können auch mehrere Parameter gesetzt werden. Bspw.:
	find -mtime +50 -mtime -100


## Online-Ressourcen

* [Manpage](https://linux.die.net/man/1/find)
* [25+ examples of Linux find command](https://www.linux.com/blog/25-examples-linux-find-command-search-files-command-line)
* [What is the difference between mtime, atime and ctime?](https://www.quora.com/What-is-the-difference-between-mtime-atime-and-ctime)


