+++
title = "Redland API"
date = "2015-03-31"
description = "Eine Reihe von freien C-Bibliotheken für das Arbeiten mit RDF"
+++

## Installation Raptor RDF Syntax Library

<http://librdf.org/raptor/>

Zusätzlich benötigte Pakete:

* libxml2-dev
* libcurl3-dev


	sudo apt-get install libxml2-dev libcurl3-dev
	./configure
	make
	sudo make install


## Installation Rasqal RDF Query Library

<http://librdf.org/rasqal/>

	./configure
	make
	sudo make install


## Installation Redland RDF API

<http://librdf.org/>

	./configure
	make
	sudo make install


## Installation Redland RDF Language Bindings

<http://librdf.org/bindings/>

Zusätzlich benötigte Pakete:

* python3-dev


	sudo apt-get install python3-dev
	./configure  -with-python=python3
	make
	sudo make install

