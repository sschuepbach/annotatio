+++
title = 'Angular Material'
+++


## Aufsetzen eines neuen Projekts

Überprüfen, ob notwendige Tools installiert sind:
	node -v
	npm -v
	ng -v # Falls nicht vorhanden: npm install -g @angular/cli


Neues Projekt erstellen:
	ng new <Projektname>


Angular Material installieren:
	npm install --save @angular/material


Installation Hammerjs (Gesten für mobile Geräte):
	npm install --save hammerjs
	npm install --save-dev @types/hammerjs


Hammerjs in Root-Module importieren:
	import 'hammerjs';


Alle Pakete auf die aktuelle Version bringen:
	sudo npm install -g npm-check-updates
	ncu          # Zeigt Differenzen zur aktuell in package.json definierten Version an. Falls ncu nicht installiert ist: sudo npm install -g ncu
	ncu -u       # Trägt neueste Version in package.json ein
	npm upgrade  # Führt Upgrade durch


In ``tsjson.conf`` ``compilerOptions``-Objektliteral hinzfügen:
	"types": [
	  "hammerjs"
	]


Optional: Material-Icons. Dazu in ``index.html`` hinzufügen:
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


Optional: Schriftart Roboto. Dazu in ``index.html`` hinzufügen:
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic" rel="stylesheet">


Optional: Material-Theme hinzufügen (in ``styles.css``):
	@import '~@angular/material/prebuilt-themes/deeppurple-amber.css';

