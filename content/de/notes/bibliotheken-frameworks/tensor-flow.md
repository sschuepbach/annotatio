+++
title = "TensorFlow"
date = "2018-05-24"
description = "Open Source-Bibliothek für maschinelles Lernen"
+++

TensorFlow computations are expressed as stateful dataflow graphs.

## APIs (Auswahl)

Offiziell:
* [Python](https://www.tensorflow.org/api_docs/python/tf)
* [Java](https://www.tensorflow.org/api_docs/java/org/tensorflow/package-summary)

Community
* [Rust](https://tensorflow.github.io/rust/tensorflow/)
* [Scala](http://platanios.org/tensorflow_scala/)
