+++
title = 'Discretionary Access Control'
+++

@Systemadministration

## Definition

Discretionary Access Control (**DAC**)" ist eine Zugangskontrolle "as a means of restricting access to objects based on the identity of subjects and/or groups to which they belong. The controls are discretionary in the sense that a subject with a certain access permission is capable of passing that permission (perhaps indirectly) on to any other subject (unless restrained by mandatory access control)".

## Rollen


* *User* (``u``)
* *Group *(``g``)
* *Other users* (``u``)
* (*All* (``a``))


## Rechte


* *Read* (``r`` / ``4``):
	* Datei: Leserechte
	* Verzeichnis: Inhaltsanzeige
* *Write* (``w`` / ``2``):
	* Datei: Schreibrechte
	* Verzeichnis: Erstellen / Löschen von Dateien
* *Execute* (``x`` / ``1``):
	* Datei: Ausführbar
	* Verzeichnis: ``cd`` erlaubt


## setuid / setgid / sticky bit


* *setuid* (``s`` (wenn ``x``) / ``S`` (wenn nicht ``x``)):
	* Datei: Wird mit *owner* ausgeführt
	* Verzeichnis: Wird in der Regel ignoriert
* *setgid* (``s`` (wenn ``x``) / ``S`` (wenn nicht ``x``)):
	* Datei: Datei mit der besitzenden *group* ausgeführt
	* Verzeichnis: Neue Dateien gehören der gleichen Gruppe, der das Verzeichnis gehört. Neu erstellte Unterverzeichnisse erhalten ebenfalls standardmässig das ``gid``-Bit
* sticky bit (``t`` (wenn ``x``) / ``T`` (wenn nicht ``x``)):
	* Datei: Datei kann nur durch den *owner* der Datei, den *owner* des Verzeichnisses und *root* umbenannt oder modifiziert werden
	* Verzeichnis: dito


## Befehle


### chmod
Mit ``chmod`` können die Rechte einer Datei bzw. eines Verzeichnisses geändert werden. Zu diesem Zweck existieren zwei Varianten

#### Textuelle Methode
Syntax: ``chmod <role><operator><permissions>``
Dabei gilt:

* Rollen:
	* ``u``: User
	* ``g``: Group
	* ``o``: Other users
	* ``a``: All
* Operatoren:
	* ``=``: Rechte neu setzen 
	* ``+``: Rechte addieren
	* ``-``: Rechte subtrahieren
* Rechte
	* ``r``: Lesbar
	* ``w``: Schreibbar
	* ``x``: Ausführbar
	* ``s``: uid / gid
	* ``t``: Sticky bit
	* (keine): Keine Rechte, nur möglich bei ``=``-Operator


#### Numerische Methode
Dreistellig (``ugo``) oder vierstellig (Sticky bit / uid / gid + ``ugo``).
Für ``ugo`` gilt:

* ``4``: Lesbar / setuid
* ``2``: Schreibbar / setgid
* ``1``: Ausführbar / sticky bit


## ACLs

## Attributes

