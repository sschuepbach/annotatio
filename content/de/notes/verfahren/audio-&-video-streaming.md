+++
title = 'Audio & Video Streaming'
+++


## Containers

[Übersichtsartikel](https://en.wikipedia.org/wiki/Digital_container_format)
[Übersicht Container](https://en.wikipedia.org/wiki/Comparison_of_video_container_formats)

Beispiele:

* OGG


## Formate

### Audioformate
[Übersicht Audioformate](https://en.wikipedia.org/wiki/Audio_file_format)

### Bildformate
[Übersicht Bildformate](https://en.wikipedia.org/wiki/Image_file_formats)

### Videoformate
[Übersicht Videoformate](https://en.wikipedia.org/wiki/Video_file_format)

Beispiele:

* Theora


MPEG (Moving Picture Expert Group) definieren eine Reihe von [Standards](https://en.wikipedia.org/wiki/Moving_Picture_Experts_Group#Standards).
``The MPEG standards consist of different Parts. Each part covers a certain aspect of the whole specification. The standards also specify Profiles and Levels. Profiles are intended to define a set of tools that are available, and Levels define the range of appropriate values for the properties associated with them. Some of the approved MPEG standards were revised by later amendments and/or new editions.`` 


## Ressourcen


* [The Technology of Video and Audio Streaming, 2nd Edition](https://learning.oreilly.com/library/view/the-technology-of/9780240805801/)
* ([Networked Audiovisual Systems](https://learning.oreilly.com/library/view/networked-audiovisual-systems/9780071825733/))
* [How Video Works, 3rd Edition](https://learning.oreilly.com/library/view/how-video-works/9781317661801/)
* [Video Compression Handbook, Second Edition](https://learning.oreilly.com/library/view/video-compression-handbook/9780134846736/)
* [Web TV - AV-Streaming im Internet](https://learning.oreilly.com/library/view/web-tv-/9783868992915/)
* [The MPEG-4 Book](https://learning.oreilly.com/library/view/mpeg-4-book-the/0130616214/)
* [Compression for Great Video and Audio, 2nd Edition](https://learning.oreilly.com/library/view/compression-for-great/9780240812137/)
* [Video Over IP, 2nd Edition](https://learning.oreilly.com/library/view/video-over-ip/9780240810843/)


