+++
bookCollapseSection = true
title = 'Text Mining'
+++


## Definition

Prozess der

* Zusammenstellung und
* Organisation,
* formalen Strukturierung und
* algorithmischen Analyse

grosser Dokumentensammlungen zur

* bedarfsgerechten Extraktion von Informationen und zur
* Entdeckung versteckter inhaltlicher Beziehungen zwischen Texten und Textfragmenten


## Methodik

1. Auswahl von geeignetem Datenmaterial
2. Aufbereitung des Datenmaterials
3. Analyse des Datenmaterials
4. Ergebnispräsentation


### Datenmaterial

* Datenmaterial sollte gewisse Ähnlichkeiten hinsichtlich der Grösse, Sprache und Thematik aufweisen, weisen aber in der Regel keine einheitliche Datenstruktur auf ("freies Format")


### Datenaufbereitung

* Überführung der Dokumente in ein einheitliches Format (heute meist XML)


