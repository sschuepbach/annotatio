+++
title = 'Methodenseminar Text Mining - Tag 3'
+++

##### Key term extraction

Verschiedene Begrifflichkeiten: "Terminology mining", "term extraction" "term recognition", "glossary extraction" -> Unterart von Informationsextraktion
Ziel ist die automatische Extraktion von relevanten Termen aus einem gegebenen Korpus
Ausgeweitete Ausgabe: "Ontology learning"

Zugang basierend auf:

* Frequenz
	* Frequenz
	* TF-IDF
* Vergleichskorpus (allgemeinsprachlicher Korpus)
	* Log likelihood
	* Characteristic elements diagnostics


#### Frequenz

Annahmen:

* Je häufiger, desto wichtiger (ev. nach Abzug der Stopwörter)


Evaluation:

* Sprache ist nach dem Zipfschem Gesetz verteilt
* Rohdatenterme sind nicht sinnvoll


#### Vergleichskorpus

=> Normalerweise besserer Ansatz als frequenzbasierte Methoden


* Difference based Term Extraction methods follow a different approach:
	* Comparing frequencies in a target corpus T with frequencies in a general comparison corpus C
	* significant deviation in T from expected term distribution measured in C is considered as relevancy criterion
* Test used in CA (u.a.)
	* Log Likelihood
	* Characteristic elements diagnostics


Log likelihood -> [Likelihood-ratio test](https://de.wikipedia.org/wiki/Likelihood-Quotienten-Test)


##### Machine Learning

Drei Abhängigkeiten: Experience (E), Task (T), Performance (P) (=Evaluation)

Generelle Unterscheidung in ML: Supervised vs. Unsupervised Learning

Supervised Learning = _Classification_:
- given expected answer -> build prediction answer

Unsupervised Learning:

* Clustering: Strukturen in Daten finden
* Dimensionality Reduction: Find most important sources of variance in data
* Anomaly detection: Find datapoints that strongly deviate from the majority


Zwischenansätze:

* Semi-supervised learning
* Active learning: Rückmeldung durch Analyst
* Reinforcement learning: Rückmeldung durch Analyst, wenn richtig gelernt


#### Naïve Bayes

Naïve Bayes: Logarithmischer Bereich wird verwendet, um extrem kleine Wahrscheinlichkeitswerte zu vermeiden, die bei der Mulitplikation aller Wahrscheinlichkeitswerte der Terme für eine bestimmte Kategorie resultieren.

#### Support Vector Machines

SVM: Large margin classifier
=> finding a separating hyperplane

##### Classification

