+++
title = 'Methodenseminar Text Mining - Tag 2'
+++

##### Inhalt


* Applications Overview
* Information Retrieval
* Frequency analysis
* Cooccurrence
* Key term extraction



##### Text Mining Applications

Procedures vs. application:

* Procedure: specifically defined NLP task (computer science view)
* Application: Combination of procedures to produce an analysis result (analysts perspective)

=> Normalerweise sind Analysten Application users. Die Frage lautet für den Analysten also meistens, welche Applikationen er anwenden möchte

Kategorien:

* Information Retrieval
* Frequency analysis: Welche Wörter kommen wie oft in einem bestimmten Fokus vor?
* Cooccurrence analysis: In welchem Zusammenhang kommen Wörter vor?
* Information extraction: Key term extraction
* Classification: Category identification; sentiment analysis; proportional classification
* Clustering: Document similarity; topic models; dimensional scaling


#### Information Retrieval

Motivation:

* Nadel im Heuhaufen: Relevante Informationen in einem Ozean von Daten finden
* Führend v.a. Suchmaschinen wie Google



* Definition: "Information retrieval is the activity of obtaining information resources relevant to an information need from a collection of information resources. Searches van be based on metadata or on full-text (or other content-based) indexing." (Wikipedia)
* Ablauf: Input (Query) -> Output (Rangliste der Dokumente, welche zum Query passen) => Aufgabe: Define a scoring function between query and documents such that the highest scoring documents satisfy a users information need
* Standard approaches: Boolean Model (Suche nach der Existenz bzw. Nicht-Existenz einer Eigenschaft); Vector Space Model (auch quantitative Angaben über die Eigenschaft möglich (wie oft kommt sie vor?)) -> Normalerweise werden die beiden Modelle in einem mehrstufigen Verfahren kombiniert (bspw. in Apache Lucene): 1. Welche Dokumente enthalten den Suchterm; 2. Wie genau passen die beiden VSM (der Query und des spezifischen Dokuments) zusammen?


TF-IDF: Term Frequency * Inverted Document Frequency (Formel siehe Beiblatt)


##### Frequency analysis


* Motivation:
	* Analysis: Comparing frequencies:
		* Verschiedener Terme
		* Eines Terms in verschiedenen Kollektionen
		* Eines Terms in einer Zeitreihe
	* Frequency: Count of a clearly defined event within a certain time frame / unit of analysis => Aggregation des Vorkommens eines bestimmten Terms in bestimmten Zeiträumen (bspw. Monate)
* Die zu zählenden Items müssen nicht einzelne Terme sein, sondern beispielsweise auch Dokumente oder Konzepte (Set von semantisch ähnlichen Termen
* Unit of Analysis: Term Frequency; Document Frequency



* Probleme von "Term as events":
* "Burstiness of terms" ->  probability of a word occuring again after seen once increases drastically -> use log(tf(w)) or df(w)?
* Variierende Sammlungsgrössen (bspw. werden Zeitungen dicker oder dünner über die Jahre) -> Normalisierung hinsichtlich der Sammlungsgrösse (absolute vs. relative Termhäufigkeit)

=> Zipfsches Gesetz: Beobachtung von Wörtern in einem Korpus -> Jedes Wort bekommt einen Rang (höher, wenn Frequenz höher). Multipliziert man diesen Rang mit der Termhäufigkeit, sollten sich die Multiplikationen ähnlich sein => Implikation: Rund 50% des Vokabulars kommt nur einmal in einem Dokument/einer Kollektion vor!
=> Aggregation / Normalisierung:

* Aggregation: tf(w, d) -> summiert über alle d in einem Monat(m) -> tf(w,m) summiert über alle Monate in einem Jahr = tf(w,y)
* Normalisierung: Formel siehe Beiblatt


Counting concepts?!?

* Diktionäre mit Termen (bspw. alle Terme, die positiv konnotiert sind (basale Sentiment analysis), oder die aus dem gleichen diskursiven Feld stammen); Operationalisierung von theoretischen Hypothesen (bspw. alle Wörter, die mit Thatchers TINA-Rhetorik zusammenhängen könnten)
* Vorsicht:
	* Sollen alle Events (=Terme) gleich gezählt werden?
	* Does occurrence match appropriate context?


Applying frequency analysis:

* Measuring frequency simply neglects contexts
* BUT: the right context may be represented (as good as possible) by your selected corpus! (e.g. counting "no alternative" in documents on European politics compared to a general corpus
* Applying filter beforehand increases chances to generate informative data


* contents
* developments
* time periods


Für die Frequenzanalyse ist die gute Auswahl des Korpus sehr wichtig!

Fragen Frequenzanalysen:

* Wie sind Frequenz und Relevanz voneinander abhängig?
* To what extent do term frequencies contribute to CA studies in social science?
* What are flaws, stumbling blocks, alternatives?
* Do word clouds contribute to understanding?


Hauptnutzen:

* Übersicht über Korpus verschaffen
* Vergleichen von Konzepten über eine Zeitreihe


##### Einschub: Einführung in R


* `which(C < 5)` gibt alle Werte aus, die kleiner als 5 sind. `C < 5` hingegen gibt Boolsche Werte aus.
* `colnames(C) <- c("one", "two")` weist der Matrix `C` die beiden Spaltennamen "one" und "two" zu.
* Rechenoperation auf Matrizen wie beispielsweise `C + 1` wendet die Operation auf jedes Element der Matrix an
* Multiplikation von Matrizien C %*% t(C)
* `paste("String1", "String2", sep="***")` fügt die beiden Strings mit dem Trenner ### zusammen
* `seq` erstellt eine Zahlenreihe (dritter Parameter setzt die Frequenz fest)
* `lapply` wendet auf jedes Element eine bestimmte Operation an
* `meta` (tm-Paket): Zeigt Metadaten eines bestimmten Objektes an


Type-Token-Ratio: Masszahl, mit welcher sich die Grammatikalität einer Sprache feststellen lässt (vgl. letzte Übung auf Übungsblatt 1)

Was ist ein sinnvolles Aggregatslevel (Monat, Jahr...)?


##### Cooccurrence Analysis


* Structuralist semantics (Saussure)
	* Syntagmatic relation: signifiers which occur conjointly complement w.r.t function and content
	* Paradigmatic relation: signifiers which occur in similar context have similar function w.r.t grammar and content -> cp. distributional hypothesis
* Computing cooccurrences
	* local context C(w): Set von Wörtern, welchen im selben "Fenster" w auftauchen
	* global context G(w): Set von Wörtern, welche zusammen mit w in statistisch signifikanter Weise auftauchen
	* Fenster: Sätze, Paragraphen, Dokumenten, Headlines, k left/right neighbor words 


Application in Social Science

* (change ofI meaning may be inferred from cooccurrence results
* cooccurrence analysis -> comparison of different result sets
	* change of context units (neighbours, sentence, document...
	* filter terms by POS-/NE-types
	* tracking change of global contexts by comparing time ranges
* Visual analytics
	* tables
	* graphs
	* KWIC-Lists



* Cooccurrence analysis:
	* global contexts -> meaning of terms ("discourse level")
	* significancy of cooccurrence relation is crucial
* "visual hermeneutics" / distant reading of collections through graphical representations
* informational enrichment by creative filtering:
	* different sub collections
	* time ranges
	* person names / NE
	* certain POS-types


