+++
title = 'Literaturliste Text Mining'
+++


* Indurkhya Nitin & Damerau, Fred J. Handbook of Natural Language Processing. London 2010.
* Ingersoll, Grant S., Morton, Thomas S. & Farris, Andrew L. Taming Text: How to Find, Organize, and Manipulate It. Greenwich 2013.
* Jurafsky, Daniel & Martin, James H. Speech and Language Processing. Upper Saddle River 2008.
* Kao, Anne & Poteet, Steve R. Natural Language Processing and Text Mining. London 2007.
* Konchady Manu. Text Mining Application Programming (Charles River Media Programming). Boston 2006.
* Miner, Gary, Elder, John & Hill, Thomas. Practical Text Mining and Statistical Analysis for Non-structured Text Data Applications. Waltham 2012.
* Pustejovsky James. Natural Language Annotation for Machine Learning. Sebastopol 2012.


