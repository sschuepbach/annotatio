+++
title = 'Methodenseminar Text Mining - Tag 4'
+++

##### Unsupervised ML (Clustering)

#### Clustering

Beim unüberwachten Lernen sind keine Trainingsdaten nötig, sondern geht nur auf statistische Kennzahlen und versucht, entsprechende Zusammenhänge zu finden.

Vector Space Models (VSM): "Semantisches" Koordinatensystem -> Semantisch ähnliche Terme gruppieren sich zu Clustern -> Ziel: Cluster identifizieren
Ein Verfahren des VSM: k-Means. k verschiedene Cluster sollen identifiziert und deren Mittelpunkt extrahiert werden.
Unterschied zu SVM: Zugang von der Semantik her, nicht von den einzelnen Termen
Verschiedene Cluster sollten einen möglichst grossen, die Elemente eines Clusters einen möglichst kleinen Abstand haben.

k-Means: Ein Mittelwert-Vektor, mit dem der Raum des Clusters gut beschrieben werden kann

Datenpunkte können nicht nur nach einem Gesichtspunkt geordnet werden -> die Auswahl der Features ist entscheidend!

Typen von Cluster-Algorithmen:

* Clusteranzahl: Manuell vs. automatisch
* Überlappende vs. disjunkte Clusters
* Hierarchisch vs. nicht-hierarchisch / eben


=> Die Evaluierung von Clustern ist schwierig, reine Berechnung ist meistens nicht ausreichend.


#### k-Means Clustering


* Ziel: Aufteilung der Daten in k verschiedene Klassen (z.B. DTM für das Clustering von Dokumenten)



* Distanzmass zwischen Dokument (d) und Cluster (c)
* Zufällige Initialisierung: k Datenpunkte werden als Clusterzentren ("Centroid") gesetzt, alle Dokumente werden den Centroiden auf Grund ihrer Distanz zugeordnet
* Durch einen iterativen Prozess, in dem im jeden Schritt die Centroiden neu berechnet werden, werden die idealen Abstandsmasse identifiziert



Zu Folie 56: Varianzen können auch zwischen verschiedenen k getestet werden (die muss kein linearer Prozess sein!)

Zu Folie 57: Standardisierung: Beispielsweise in 0 und 1 unterteilen

Zu Folie 59: Hierarchisches Clustering ermöglicht es, Kategorienhierarchien zu erstellen (bspw. durch näher verwandte Cluster mit Hilfe von Dendrogrammen)

* L2-Messung: Bezieht sich auf den euklidischen Abstand
* L1-Messung: Bezieht sich auf das Manhattan-Mass.
* Das Manhattan-Mass geht nicht über die direkteste Distanz, sondern orientiert sich am Koordinatensystem und geht immer über 1er-Schritten auf einer Achse des Koordinationsystems


Zu Folie 60:

* cosim: Cosinus Similarity
* Das Cosinus-Mass ist eine "Normalisierung", welche die Abstände auf einen Einheitskreis projiziert: Dokumentlänge, welche die Vektoren vergrössern würde, spielt eine kleinere Rolle für die Berechnung.



#### Topic Models


* LDA: Dirichlet-Verteilung: Eine multinominal-Verteilung wird verschieden stark gewichtet; Mixed-Membership Model; Ergebnis sind immer Themen-Listen von Wörtern mit abnehmender wahrscheinlicher Zuordnung. Die Labels für die Listen müssen manuell vergeben werden.
* HDP: Non-parametrische Erweiterung von LDA (k müssen nicht angegeben werden)
* HPY: Die zugrundeliegende Verteilung ist eine Poisson-Verteilung, keine Dirichlet-Verteilung


Topics sind eine Art konvergierte Kookkurrenzmuster, die über alle Dokumente gefunden werden. Latente Themencluster werden über das Auftreten mit geteilten Termen identifiziert.

Prozess verläuft über die einzelnen Dokumente: Nachdem die Clusters für das erste Dokument zufällig erzeugt wurde und iterativ genauer bestimmt worden sind, wird für das nächste Dokument bereits diese Zuordnung herangezogen.

Proportions Parameter alpha: Bestimmt die Verteilung der Themen (soll eher ein Thema extrahiert werden oder mehrere?)

Zu Folie 69: Ziel: Möglichst distinkte Kategorien schaffen.

Zu Folie 71: Absolute Grössen lassen sich aus diesem Ergebnis nicht ableiten! Sondern: Das semantische Muster ist zu einer bestimmten Zeit so stark gefunden worden (Exploration!). So können interessante Zeitpunkte identifiziert werden, die dann wiederum in einem deduktiven Prozess (bspw. über Kookurrenzanalysen oder durch einen qualitativen Ansatz) genauer untersucht werden. 

Zu Folie 73: Paramteroptimierung kann auch automatisch ausgeführt werden.


##### Dimensional Scaling

Annahme: Thematisch relativ kohärenter Korpus -> Ziel: Abbildung von verschiedenen Positionen innerhalb dieses thematischen Blocks

