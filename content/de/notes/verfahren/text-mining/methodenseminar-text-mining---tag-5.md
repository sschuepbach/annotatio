+++
title = 'Methodenseminar Text Mining - Tag 5'
+++

##### 4. Methodological integration

"Qualitative analysis at its very core can be condensed to a close and repeated review of data, categorizing, interpreting and writing." (Schönfelder 2011)

=> Text Mining Hilfe beim Review und der Kategorisierung von Daten

#### Zu Folie 6
 Triangulation: Ergebnis aus einem anderen (bspw. anderen methodischen) Blickwinkel betrachten.
Datenhaltung: Datenbanken, Raw-Files, ...?
Verfahren: Evaluierung stets wichtig!

#### Zu Folie 7
Jeder Punkt eine "Gegensätzlichkeit"

#### Zu Folie 9


* Text Mining macht dort Sinn, wo grössere (> mehrere 100) Dokumente analysiert werden sollen.
* Zusammenspiel von unüberwachten und überwachten Methoden


#### Zu Folie 12


* Die Auswahl der Text Mining-Verfahren sind immer abhängig von der Forschungsfragen / dem Erkenntnisinteresse


#### Zu Folie 13

4 Prinzipien von Text Mining nach Grimmer (2013):

#### Zu Folie 15


* Reproduktion kann schwierig sein (nicht-deterministische Algorithmen)
* Stopwortliste allenfalls auch selber erstellen / modifizieren


#### Zu Folie 16

Tipps für bessere Reliabilität:

* Kenntnis der Daten
* Kenntnis der Algorithmen
* Ergebnisse validieren
* Dokumentation und Speicherung der Roh-, Zwischen- und Outputdaten sowie des Workflows und der Konfiguration


#### Zu Folie 17


* Datenset kann (sollte?), anstatt integral durch ein Verfahren ausgewertet zu werden, in verschiedene Zeitperioden, Themen, Metadaten, ... unterteilt werden


#### Zu Folie 19


* Wörterbuch Neoliberalismus: Termextraktion von Werken neoliberaler Theoretiker
* Retrieval-Precision: Stichprobenartige manuelle Bewertung der Relevanz der Dokumente
* Abschliessender Schritt (nicht auf Folie): Anwendung der identifizierten neoliberalen Argumente auf den Gesamtkorpus



##### Anmerkungen

Dice-Tests eignen sich v.a. für kleinere, Log-Likelihood für grössere Datenmengen.
Topic-Model in R kann nicht mit "0-Zeilen" arbeiten -> die Auswahl in der Dokument-Term-Matrix muss so gewählt werden, dass nur Dokumente mit > 0 auszuwertenden Termen gewählt werden


#### CLARIN

Common Language Resources and Technology Infrastructure 

=> clarin.eu

