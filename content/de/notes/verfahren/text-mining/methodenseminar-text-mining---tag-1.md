+++
title = 'Methodenseminar Text Mining - Tag 1'
+++

##### Übersicht

Inhalte Methodenseminar:

* Computer-assisted QDA
* Data management
* Computational semantics
* Preprocessing
* Text Mining applications
	* Lexicometrics
	* Machine Learning (u.a. Clustering, Klassifikation)
* Visualizations
* (Best) Practices in social science research



Heutige Vorlesung:

* Introduction
* Qualitative data analysis
* Digital text as data
* Computational semantics
* Data management
* Preprocessing / Text Mining procedures



##### Einführung

Anwendungen Text Mining:

* Suchmaschinen
* Spamerkennung (durch Klassifikatoren) - heute mit einer Genauigkeit >99%
* Siri (App iPhone)
* Watson (Computer im Jeopardy-Spiel)



Definitionsversuch:
Text Mining is a set of "computer based methods for a semantic analysis of text that help to automatically, or semi-automatically, structure text, particular very large amounts of text" (HEYER, 2008, p.2)
- linguistic knowledge
- statistical knowledge


##### Qualitative data analysis


* Digital Humanities: Eigene Disziplin oder Hilfswissenschaft? -> Gemeinsamkeit liegt in den Methoden, nicht im Erkenntnisinteresse



* Lange Tradition...
	* beispielsweise Journal "Computers and the Humanities" (seit 1966)
* ...aber neue Entwicklungen:
	* Verfügbarkeit von digitalen Texten (retro oder direkt digitalisiert)
	* Grössere Rechnerleistung
	* Neue Modelle und Algorithmen, die "Semantik" beschreiben können


Typologien von cQDA-Methoden:

* STONE (1997)
	* (data-dricen) expolration of therm frequencies / concordances
	* (theory-driven) hypothesis testing on categories (dictionaries)
* KRIPPENDORFF (2013)
	* retrieval functions for character strings on raw text -> Suche
	* computational content content analysis (CCA) -> Automatische Analyse
	* computer-assisted qualitative data analysis software (CAQDAS) -> Manuelle Analyse
* LOWE (2003)
	* dictionary based CCA
	* parsing approaches to CCA
	* contextual similarity measures
* SCHARKOW (2012)
	* unsupervised approches -> Ohne Hypothese
	* supervised approahces -> Hypothesengeleitet
	* Kombinierbar mit
		* statistical vs. linguistic approaches
		* deductive vs. inductive approaches
* WIEDEMANN (2013)
	* CAQDAS: Context-cpmprehensive manual coding
	* Computational Content Analysis: Context-neglecting automativ coding
	* Lexicometrics / corpus linguistics: Context-observing content exploration
	* Machine Learning: Pattern- und model-based latent analysis


"Qualitative analysis at its very core can be condensed to a close and repeated review of data, categorising, interpreting and writing" (Schönfelder, 2011)

Vorteile von Text Mining für QDA:

* Exploration of large data sets
* Analysis of time series
* Identifying structures / propositional regularities
* Quantification -> measuring relevancy of qualitative categories
* Improving quality of qualitative research

Probleme von Text Mining für QDA:

* Übersetzung der analytischen Begrifflichkeiten -> Operationalisierung
* Spannungsfeld Semantik <-> Algorithmen
* Integration von theoretischem Wissen
* Gute Verbindung zwischen close und distant reading finden
* Algorithmen können nicht interpretieren, sondern finden Muster und Strukturen



##### Digital Text as Data

*symbol* <- Bedeutung
- Charakter: Linguistische Einheit (Bedeutungsrepräsentation - beispielsweise Herz, Karo)
- Glyphen: Grafische Repräsentation eines Symbols (bspw. verscheidene Druckweisen des Buchstabens a (fett, kursiv, klein-/grossgeschrieben, verschiedene Schriftarten etc.))

*alphabet*: Abgeschlossene Menge von Zeichen

*encoding*:
- unambigous assignment of character from an alphabet to bit pattern, octets etc.
- standards: UTF-8, ASCII usw.

*String*: concatenation alphabet elements
- Essentieller, elementarer Datentyp in der Computerlinguistik

*Document*: Compound data type
- (collection of) strings (e.g. title, body) [+ Metadaten]

*Corpus*: collection of documents

*Type* (cp. class)
- (abstract) string representing a meaningful concept, eg. words

*Token* (cp. object)
- (concrete) string as instance of a meaningful concept (Instanzen eines Type in einem bestimmten Dokument)

*Vocabulary* 
- Complete set of types of a document or collection


Vorgehen Text -> Digitale Daten:

* Transformation von Text in Daten
* -> Data Mining



##### Data sources

u.a.:

* Zeitungsarchive: beispielsweise <http://www.zeit.de/archiv> (Artikel seit 1946) -> Probleme von nicht-einheitlichen Metadaten (bspw. verschiedene Schreibweisen von Kürzeln, Boolsche Angabe oder String etc.)
* Blogs: beispielsweise blogactiv.eu -> Probleme bei Blog beispielsweise Crawling and Web scraping
* Foren: beispielsweise www.1000fragen.de
* Soziale Netzwerke (Schwierigkeiten: Kurze Textsnippets, je nach dem keine Schnittstelle (u.a. Facebook)); sentiment viz von Twitter (<http://www.csc.ncsu.edu/faculty/healey/tweet_viz/tweet_app/>)
* Emails
* Politische Reden (bspw. Datensatz von Adrien Barbaresi des Bundespräsidial- und Kanzleramtes" (<http://perso.ens-lyon.fr/adrien.barbaresi/corpora>)
* Parlamentsprotokolle, Parteimanifeste (Beispiele PolMine (<http://polmine.sowi.uni-due.de/>); Comparative Party Manifesto Project (WZB) (<https://manifesto-project.wzb.eu/>))
* Veröffentlichte Fragen von Fragebögen


Fragen nach der sinnvollen Quelle:

* Which source fits my research interests (oder umgekehrt)?
* What knowledge is represented in these sources?
* For whom this knowledge is representative (basic population, selection bias)?
* Do we need sampling? Which are good sampling strategies?
* What kind of results do we want to have in the end?
	* insightful ansewrs to interesting questions


Methodologie: Ontologische / epistemiologische Grundlagen einer empirischen Analyse
Methode: Operationalisierung: Collecting and interpreting instances of concepts (bspw. Zahlen, Akteure); systematisches Vorgehen

Text Mining applications may

* be integrated with various methodologies
* directly support established methods / concepts
* provide additional concepts / (visual) results for interpretation



##### Software

"tools" vs. "procedures"

* "tool" ~ standalone program incorporation functionality for data input/management, data processing and data output (visualization)
* procedure ~ analysis technique providing basically
	* data processing


NLP pipelines

* Pipeline: application of different data manipulation procedures in row
	* preprocessing
	* actual analysis
	* output format
* e.g. data reader -> filter -> sentences -> tokens -> sentiments -> data writer
* Jeder Schritt benötigt eine Menge Parameter:
	* Welches sind die besten Parametereinstellungen?
	* Reproduzierbarkeit?


#### (Open Source) Software

Middleware / NLP processing frameworks (stellen nur Rahmen bereit)

* Dragon Toolkit
* UIMA
* GATE
* Matlab
* R
* etc.


Libraries:

* openNLP
* R packages (Bsp.: tm, topicmodels, lda, e1071, cluster)
* Libsvm
* WEKA
* Mallet



##### Computational Semantics


* semantics: "in a general sense (...) the meaning of a word, a phrase, a sentence, or any text in human language, and the study of such meaning" (Turney & Pantel 2010)


Computer brauchen ein Model von Semantik!

Levels of semantics:

* lexical: representation of word meaning
* sentence: composition of word meanings & syntax structure
* discourse: dialog / intertextuality


compuational models of semantics:

* representation of semantics
	* linguistic pattern (Diktionäre, in denen Worte nach einer bestimmten Eigenschaft/Bedeutung gruppiert werden)
		* beispielsweise mit Hilfe von regulären Ausdrücken kann nach diesen Mustern gesucht werden bzw. beide können kombiniert werden
		* Ähnlichkeiten von Strings
			* Matchings of sub parts FIXME
			* absolute / relative share of characters FIXME
			* share of character-trigrams FIXME
			* edit distance (Levenshstein distance) (wie viele Editierfunktionen (löschen, einfügen, ändern) müssen vorgenommen werden, um vom einen String zum anderen zu kommen) 
		* Complex string patterns may model meaning 
	* logical
		* First-order logic (FOL): formalism to represent sentence meaning
		* Bsp.: friend-of(john, edward) -> John ist mit Edward befreundet; komplex: red(a) ^ bike(a) ^ broken(a) -> The red bike is broken; auch kombinierbar mit Quantoren etc.
	* distributional/statistical (textual statistics):
		* meaning of words can be inferred from observations in large-scale, empirical language data
		* meaning of words can be encoded in sets of numbers
		* FIXME
		* Fünf Hypothesen:
			* Statistical semantics hypthesis: Frequenzen, Kookkurenzen
			* Bag of words hypothesis: Anordnung der Strings ist weniger wichtig
			* Distributional hypothesis: Bedeutung wird hauptsächlich über Kontext hergestellt (Wörter, die in ähnlichen Kontexten erscheinen, haben eine ähnliche Bedeutung)
			* Extended distributional hypothesis: Patterns that co-occur with similar pairs tend to have similar meanings
			* Latent relation hypothesis: Pairs of words that co-occur in similar patterns tend to have similar semantic relations


Textstatistiken Werden oft in Vector Space Models überführt:

* Document-Term-Matrix
* Word-Context-Matrix (event: cooccurrence of type ma and type mb within certain window)
* Pair-Pattern-Matrix (m pairs of words <-> n patterns) -> Weniger interessant für Methodenseminar



##### Data Management

Mögliche Dateitypen:

* Textdateien: Unstrukturiert
* CSV, XML, (X)HTML: Semi-strukturiert


Abspeichern:

* Als CSV-/XML-Dateien (nur sinnvoll bei kleinerer Menge an Dokumenten <20000)
* Data base management systems (DBMS) (bei grösseren Datenmengen)
* Full text index (Extension von DBMS, die Suche nach Schlüsselwörtern in Dokumenten erlaubt) (bspw. Apache Solr / Lucene; ElasticSearch)


Daten sammeln:

* Fragen an den Datensammlungsprozess in textueller Inhaltsanalysen:
	* Daten verfügbar?
	* Daten digitalisiert
	* OCR-isiert?
	* Bereinigt?
	* Annotiert / präprozessiert?


Crawling data

* Crawling: massive automated download of data from the web
* Uninformed approach
	* start with initial list of URLs
	* download web page from URL
	* expand list of URL by analyzing page source
* Informed approach
	* generate list of URLs by patterns


Scraping data

* Scraping: Extraction of relevant content from downloaded XHTML source
* (manual) investigation of XML-tree
* identify relevant elements


-> Crawling / scraping Data: Buch Automated data collection with R (<http://www.r-datacollection.com/>)


##### Text Mining Procedures

#### (Linguistic) preprocessing

* Combination of several steps to prepare data for further procedures (bspw. encoding, data cleaning, handling duplicates, spelling correction, stopwords, dictionaries, pruning, unification (stemming usw.))
* e.g. conversion of text into a term-document matrix
* essentially influences results of further steps!


### Encoding

* Immer in UTF-8 arbeiten!
* Konvertierung beispielsweise durch `iconv -f original_charset -t utf-8 originalfile > newfile`


### Data cleaning

* Quelldateien können "noise" enthalten (falsche OCR-Resultate, Seitenkopfdaten, Seitennummer, Copyright-Informationen in XML-Dateien etc.)


### Duplicates

* Datensammlungen enthalten oft Duplikate
* Definition von Duplikaten ist nicht immer klar
* Identifikation ist nicht immer einfach, da es sich nicht immer um ein 1:1-Verhältnis handeln muss


### Spelling correction

### Stop words

-> Standardlisten

* Informationsverlust bedenken!


### Dictionaries

* Positivliste
* Können manuell oder automatisch (Term extraction) erstellt werden


### Pruning

* Filtering the vocabulary of a collection by minimum / maximum thresholds of occurrence (term frequency / document frequency (welche Terme kommen beispielsweise in mehr als 99% oder weniger als 1% vor?)
* Very useful preprocessing step to reduce vocabulary size


### Unification: Stem vs. Lemma

* Observation: similar semantic types share similar orthographic forms
* Idea: Map variants to reduced form
* Stemming: Tendenziell regelbasiert (Probleme: Overstemming (es wird zu viel gekürzt -> künstliche Ambiguität, Understemming (es wird zu wenig gekürzt -> Vereinheitlichung schlägt fehl)). Diesen Problemen kann grundsätzlich mit einem Diktionär von Ausnahmen begegnet werden
* Lemmatization: Auf Diktionär basierend (Probleme: Vollständigkeit, Fehlerfreiheit und Verfügbarkeit von Listen)



-> Texte in verschiedenen Sprachen separat analysieren (ausser wenn ein 1:1 mapping möglich ist (also nur bei starker Reduktion))!

#### Advanced Preprocessing


* Präprozessierung beinhaltet die Identifikation von sprachlichen Einheiten:
	* tokens = words
	* sentences
	* Part-of-Speech = Wortarten
	* named entities = Personen, Orte, Organisationen, Daten etc.
	* morphology = Modi, Zeitformen, Fälle, Geschlecht etc.
	* parses / chunks = Strukturelle Zusammenhänge zwischen Termen
	* anaphoras = Relation zwischen Subjekt - Pronomen 
	* semantic roles = Akteur - Handlung, Subjekt - Objekt
* Funktioniert in de Regel über probabilistische Modelle (bspw. durch openNLP (einbindbar in R))


### Sentence detection / Tokenization

* tokenization = process to segment character strings into sentences and/or tokens (words)
* Sentence detection: Rule based approach: defining set of rules to identify beginnings and endings of sentences (machine learning task: classify each occurrence of [.?!] in a) is a sentenceEnd b) is not sentenceEnd
* Tokenization: Nach Leerzeichen zu trennen ist nicht schlecht, sollte aber trotzdem ausgeweitet werden (regelbasiert und mit probabilistischen Modellen (machine learning task: classify each non-alphanumeric character in a) is tokenBoundary b) is not tokenBoundary))


### Part-of-Speech

* Labeling von jedem Token mit einer Wortart
* Hervorragende Aufgabe für machine learning classifiers auf annotierte Trainingsdaten!
* Tag-sets für verschiedene Sprachen 
* OpenNLP eignet sich gut für diese Aufgabe!


### Named Entity Recognition

* Auch hier ist ein machine learning classifier auf Trainingsdaten oder OpenNLP geeignet




##### Glossar

CCA:	Computational Content Analysis
cQDA: 	Computer assisted qualitative data analysis software (e.g. MAXQDA)

