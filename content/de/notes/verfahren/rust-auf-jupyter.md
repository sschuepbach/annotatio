+++
title = "Rust in Jupyter Notebook betreiben"
date = "2021-11-03T15:13:08+01:00"
draft = false
+++


1. Miniconda installieren (`miniconda` in AUR)
2. `conda` binary temporär `$PATH` hinzufügen: `export
   PATH=$PATH:/opt/miniconda/bin`
3. Neue conda-Umgebung erstellen: `conda create -n <name-der-env> python=3`
4. Umgebung aktivieren: `conda activate <name-der-env>`
5. Notwendige Erweiterungen für conda installieren: `conda install -c conda-forge jupyterlab` und `conda install -c anaconda cmake -y`
6. [EvCxR Jupyter Kernel](https://github.com/google/evcxr) installieren:
   `cargo install evcxr_jupyter; evcxr_jupyter --install`
7. Jupyter Lab starten: `jupyter lab`
