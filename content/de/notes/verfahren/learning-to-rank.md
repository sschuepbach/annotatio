+++
title = 'Learning to Rank'
+++


## Ressourcen

* [Artikel auf der englischsprachigen Wikipedia](https://en.wikipedia.org/wiki/Learning_to_rank)
* [Plugin für Elasticsearch](https://github.com/o19s/elasticsearch-learning-to-rank)
	* [Dokumentation](https://elasticsearch-learning-to-rank.readthedocs.io/)
* Artikelserie auf OpenSource Connections:
	* [We're Bringing Learning to Rank to Elasticsearch](https://opensourceconnections.com/blog/2017/02/14/elasticsearch-learning-to-rank/)
	* [What is Learning To Rank?](https://opensourceconnections.com/blog/2017/02/24/what-is-learning-to-rank/)
	* [Is Your Infrastructure Ready For Learning To Rank?](https://opensourceconnections.com/blog/2017/07/05/infrastructure_for_ltr/)
	* [Learning to Rank 101 -- Linear Models](https://opensourceconnections.com/blog/2017/04/01/learning-to-rank-linear-models/)
	* [Test Driving Elasticsearch Learning to Rank with a Linear Model](https://opensourceconnections.com/blog/2017/04/03/test-drive-elasticsearch-learn-to-rank-linear-model/)
	* [How is search different than other machine learning problems?](https://opensourceconnections.com/blog/2017/08/03/search-as-machine-learning-prob/)
* [How Bloomberg Integrated Learning-to-Rank into Apache Solr](https://www.techatbloomberg.com/blog/bloomberg-integrated-learning-rank-apache-solr/)


