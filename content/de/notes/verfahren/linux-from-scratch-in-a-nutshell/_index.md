+++
title = 'Linux from Scratch in a Nutshell'
+++

@Systemadministration @WIP

Dies ist eine Zusammenfassung der [Linux from Scratch-Anleitung](http://www.linuxfromscratch.org/lfs/view/stable/) (v. 8.2).

## Voraussetzungen des Host-Systems testen

Das Host-System muss eine Reihe von Programmen zur Verfügung stellen, damit das LFS-System aufgesetzt werden kann: [Liste](http://www.linuxfromscratch.org/lfs/view/stable/) und [Testskript.](./version_check.sh)

## LFS-Partition erstellen

Empfohlen wird 

* mindestens 20G, verteilt über eine ``/``, eine ``/boot``- und eine ``/home``-Partition (weitere bei Bedarf), und
* eine Swap-Partition mit Grösse RAM*2


Abfolge:

* Erstellen der benötigten Partitionen sowie Formatierung (empfohlen wird ``ext2``, ``ext3`` oder ``ext4``)
* Shortcut zu LFS-Root-Partition: ``export LFS=/mnt/lfs``
* Mounten der Partitonen (``swapon`` der Swap-Partition nicht vergessen!)


## Download der benötigten Toolchain-Pakete


* ``mkdir -v $LFS/sources``
* ``chmod -v a+wt $LFS/sources``
* Download der Liste benötigter Pakete und Patches: [wget-list](http://www.linuxfromscratch.org/lfs/view/stable/wget-list) ([Ausführliche Liste Pakete](http://www.linuxfromscratch.org/lfs/view/stable/chapter03/packages.html) / [ausführliche Liste Patches](http://www.linuxfromscratch.org/lfs/view/stable/chapter03/patches.html))
* ``wget --input-file=wget-list --continue --directory-prefix=$LFS/sources``
* Download [md5sums](http://www.linuxfromscratch.org/lfs/view/stable/md5sums)
* ``pushd $LFS/sources; md5sum -c md5sums; popd;``


## Weitere Vorbereitungsschritte

* Verzeichnis für Tools erstellen (als root): ``mkdir -v $LFS/tools``
* ``ln -sv $LFS/tools /``
* Benutzer ``lfs`` erstellen: ``groupadd lfs && useradd -s /bin/bash -g lfs -m -k /dev/null lfs`` (``-k /dev/null`` verhindert das Kopieren von Skelett-Dateien in das ``home``-Verzeichnis des neuen Users)
* ``passwd lfs``
* User ``lfs`` zu Besitzer von ``$LFS/tools`` und ``$LFS/sources`` machen
* Login als user ``lfs``: ``su - lfs`` (``-`` startet eine Login-Shell)
* Erstellen von ``~/.bash_profile`` mit folgendem Inhalt

``exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash``

* Erstellen von ``~/.bashrc`` mit folgendem Inhalt:

	set +h
	umask 022
	LFS=/mnt/lfs
	LC_ALL=POSIX
	LFS_TGT=$(uname -m)-lfs-linux-gnu
	PATH=/tools/bin:/bin:/usr/bin
	export LFS LC_ALL LFS_TGT PATH


* ``source ~/.bash_profile`` nicht vergessen!
* Ev.: ``export MAKEFLAGS='-j 2``' (bei Problemen / Debugging auf ``-j 1`` zurücksetzen
* Es wird empfohlen, für zentrale Pakete - ``GCC``, ``Binutils`` und ``Glibc`` die Testsuite laufen zu lassen (doch auch hier für Bootstraping-Phase grundsätzlich überflüssig)


## Technische Anmerkungen zur Toolchain

[Lesen](http://www.linuxfromscratch.org/lfs/view/stable/chapter05/toolchaintechnotes.html)!

