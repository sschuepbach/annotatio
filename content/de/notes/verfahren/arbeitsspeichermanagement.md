+++
title = 'Arbeitsspeichermanagement'
+++

@Systemadministration

Quelle: <http://www.upubuntu.com/2013/01/how-to-free-up-unused-memory-in.html>

Momentaner Arbeitsspeicherbedarf in Echtzeit:
``watch -n 1 free -m``

Momentaner Arbeitsspeicherbedarf und Systembedarf in Echtzeit:
``watch -n 1 cat /proc/meminfo``

Nicht mehr benutzter Arbeitsspeicher freigeben:
``sudo sysctl -w vm.drop_caches=3``

Here is another command that can help you free up memory either used or cached (page cache, inodes, and dentries):
``sudo sync && echo 3 | sudo tee /proc/sys/vm/drop_caches``

