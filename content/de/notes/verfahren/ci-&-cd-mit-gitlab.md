+++
title = 'CI/CD mit GitLab'
+++

@DevOps

Für eine generelle Einführung ins Thema s. [offizielle Dokumentation](https://gitlab.com/help/ci/quick_start/README.md)

## .gitlab-ci.yml

vgl. auch [offizielle Dokumentation](https://gitlab.com/help/ci/yaml/README)

## Deployment auf Docker Hub

vgl. auch [Diskussion auf Stackoverflow](https://stackoverflow.com/questions/45517733/how-to-publish-docker-images-to-docker-hub-from-gitlab-ci)

Auf Gitlab:

* Settings -> CI / CD -> Abschnitt [Secret variables](https://gitlab.com/help/ci/variables/README#secret-variables)
* Hier folgende Variablen hinzufügen:
	* ``CI_REGISTRY_USER=<User auf Docker Hub>``
	* ``CI_REGISTRY_PASSWORD=<PW auf Docker Hub>``
	* ``CI_REGISTRY=index.docker.io`` (kann mit ``docker info | grep Registry`` herausgefunden werden)
	* ``CI_REGISTRY_IMAGE=index.docker.io/<docker-hub-user>/<repo-name>``


