+++
title = "OAI-PMH"
+++


*Open Archives Initiative Protocol for Metadata Harvesting*

Ein Protokoll, welches zum Sammeln von Metadaten über archivische Unterlagen dient.

OAI-PMH nutzt XML über HTTP

Verschiedene Versionen, aktuelle 2.0 von 2002

Verschiedene Registrare, um Repositorien zu erfassen, bspw. [The Open Archives list of registered OAI repositories](http://www.openarchives.org/Register/BrowseSites).

Inkrementelles Harvesting ist möglich

Basiert auf Client-Server-Architektur, wo "Service Providers" Informationen über aktualisierte Datensätze bei "Data Providers" abfragen. Eine solche Abfrage kann einen Filter über einen Zeitraum enthalten und/oder auf ein bestimmtes Unterset von Daten beschränkt werden.

Antworten sind immer in XML und müssen mindestens das Dublin-Core-Format anbieten

OAI-PMH wird von diversen Repositorienapplikationen unterstützt

OAI-PMH enthält sechs Verben/Services, die über HTTP aufgerufen werden können.


## Record

Ein Record enhält drei Teile:

* Header: Unique Identifier, Datestamp, 0 oder mehr ``setSpec``-Elemente (Zugehörigkeit(en) zu Set(s))
* Metadata: Eine Manifestation (Format) der Metadaten eines Items
* About (optional): 0 oder mehr Containers, welche Daetn über den Metadaten-Teil enthalten. Muss einem XML-Schema genügen. Zwei verbreitete Anwendungsfälle sind Urheberrechte und Herkunftsinformationen

Repositorien müssen einen von drei Support-Kategorien für gelöschte Records unterstützen, welcher im ``deletedRecord``-Element der ``Identity``-Response sichtbar wird:

* ``no``: Das Repository verfügt über keine Information zu Löschungen.
* ``persistent``: Das Repository unterhält zeitlich unbegrenzt Informationen zu Löschungen
* ``transient``: Es wird nicht garantiert, dass die Liste der Löschungen persistent bzw. konsistent geführt wird.


## Response

* ``ListIdentifiers``
* ``ListRecords``


## Requests / Responses

* ``GetRecord``
* ``ListIdentifiers``
* ``ListRecords``
* ``ListSets``


## Selektives Harvesting

Selektives Harvesting mit den Verben ``ListRecords`` und ``ListIdentifiers`` kann auf Grundlage von

* Zeitstempeln und
* Sets

geschehen.


### Zeitstempel

Gesucht werden kann mit den beiden Parametern ``from`` oder ``until`` oder beiden (jeweils inklusiv). Eine Response muss Records beinhalten, welche entweder im gesuchten Zeitraum entstanden oder modifiziert worden sind. Sie kann - bei entsprechen Einstellungen - auch im gesuchten Zeitraum gelöschte Records enthalten.


### Sets

Gesucht wird mit dem Parameter ``setSpec``. Die Response enthält Records, welche Teil des Sets sind, oder Teil eines Unter-Sets des gesuchten Sets.


## Glossar

* **Data Provider**: Partei, welche Systeme administriert, welche OAI-PMH zum Anbieten von Metadaten nutzen
* **Harvester**: Eine Clientapplikation, welche OAI-PMH-Requests absetzen kann
* **Item**: Eine Art "Container" über eine einzige Ressource, von welchem Records in verschiedenen Formaten erstellt werden können. Jedes Item hat eine eindeutige ID
* **Record**: Metadaten über eine Ressource in einem spezifischen Metadatenformat, die an den Harvester ausgeliefert wird. Records können im Repository gespeichert oder on-the-fly erstellt werden
* **Repository**: Ein über ein Netzwerk zugänglicher Server, welcher die sechs Arten von OAI-PMH-Requests verarbeiten kann. Wird vom data provider verwaltet.
* **Resource**: Objekt, welches Metadaten beschreiben
* **Service Provider**: Die Partei, welche über OAI-PMH erhaltene Metadaten für einen Service nutzt
* **Set**: Ein Set ist ein optionales Kontrukt, um *Items* für selektives Harvesting zu gruppieren


## Links

* <https://www.openarchives.org/Register/BrowseSites>
* <http://re.cs.uct.ac.za/>
* <http://www.swissbib.org/wiki/index.php?title=Swissbib_oai>