+++
title = "IIIF"
+++


Das International Image Interoperability Framework (IIIF) soll den applikationsunabhängigen Zugang zu Bildressourcen und deren Metadaten im Web verbessern. Folgende Ziele werden verfolgt:

* To give scholars an unprecedented level of uniform and rich access to image-based resources hosted around the world.
* To define a set of common application programming interfaces that support interoperability between image repositories.
* To develop, cultivate and document shared technologies, such as image servers and web clients, that provide a world-class user experience in viewing, comparing, manipulating and annotating images.


The IIIF is a set of shared application programming interface (API) specifications for interoperable functionality in digital image repositories. Using [JSON-LD](file:///home/seb/doku/linked/Konzepte/JSON-LD.txt), linked data, and standard W3C web protocols such as [Web Annotation](./Web_Annotation.md), IIIF makes it easy to parse and share digital image data, migrate across technology systems, and provide enhanced image access for scholars and researchers. In short, IIIF enables better, faster and cheaper image delivery. It lets you leverage interoperability and the fabric of the Web to access new possibilities and new users for your image-based resources, while reducing long term maintenance and technological lock in. IIIF gives users a rich set of baseline functionality for viewing, zooming, and assembling the best mix of resources and tools to view, compare, manipulate and work with images on the Web, an experience made portable–shareable, citable, and embeddable.

## APIs

IIIF definiert verschiedene APIs:

* [Image API](http://iiif.io/api/image/): Spezifiziert einen Web service, der ein Bild zurückgibt. Durch die URL können die Region, die Grösse, die Drehung, die Qualität und das Format des darzustellenden Bildes definiert werden
* [Presentation API](http://iiif.io/api/presentation): Spezifiziert einen Web service der Dokumente zurückgibt, welche die Struktur und das Layout eins digitalen Objektes oder verwandte Objekte beschreiben
* [Search API](http://iiif.io/api/search): Zur Suche nach Annotationen in einer einzelnen IIIF-Ressource


### Presentation API

The aim of the Presentation API is to provide the information necessary to allow a rich, online viewing environment for primarily image-based objects to be presented to a human user, likely in conjunction with the IIIF Image API. This is the sole purpose of the API and therefore the descriptive information is given in a way that is intended for humans to read, but not semantically available to machines. In particular, it explicitly does not aim to provide metadata that would drive discovery of the digitized objects.

#### Types

Basic:

* **Manifest**: Overall description of the structure and properties of the digital representation of an object. Carries descriptive information needed by the viewer for presentation to the user. Each manifest describes how to present a single object (e.g. a book, photograph, statue).
* **Sequence**: Order of views of the object. Multiple sequences are allowed (e.g. to define different order of pages in a manuscript)
* **Canvas**: Virtual container that represents a page or view and has content resources associated with it or with parts of it. Provides a frame of reference for the layout of the content.
* **Content**: Resource such as an image associated with a canvas


Advanced:

* **Collection**: Ordered list of manifests and/or further collections used for the organisation of manifests. Can carry its own descriptive information. It can also provide clients with a means to locate all of the manifests known to the publishing institution.
* **Annotation**: Associates content resources and commentary with a canvas. Provides a single, coherent method for aligning information, and provides a standards based framework for distinguishing parts of resources and parts of canvases. As annotations can be added later, it promotes a distributed system in which publishers can align their content with the descrptions created by others.
* **AnnotationList**: Ordered list of annotations, typically associated with a single canvas
* **Layer**: Ordered list of annotation lists. Allows higher level groupings of annotations to be recorded. For example, all of the English translation annotations of a medieval French document could be kept separate from the transcription or an edition in modern French.
* **Range**: Ordered list of canvases, and/or further ranges. Allows canvases, or parts thereof, to be grouped together in some way. This could be for textual reasons, such as to distinguish books, chapters, verses, sections etc. Equally, physical features might be important such as quires or gatherings, sections that have been added later and so forth.