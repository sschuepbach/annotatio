+++
title = "Web Annotation"
+++


* [Web Annotation Data Model](https://www.w3.org/TR/2017/REC-annotation-model-20170223/) - specification describes a structured model and format, in JSON, to enable annotations to be shared and reused across different hardware and software platforms. Common use cases can be modeled in a manner that is simple and convenient, while at the same time enabling more complex requirements, including linking arbitrary content to a particular data point or to segments of timed multimedia resources.
* [Web Annotation Vocabulary](https://www.w3.org/TR/2017/REC-annotation-vocab-20170223/) - specifies the set of RDF classes, predicates and named entities that are used by the Web Annotation Data Model. It also lists recommended terms from other ontologies that are used in the model, and provides the JSON-LD Context and profile definitions needed to use the Web Annotation JSON serialization in a Linked Data context.
* [Web Annotation Protocol](https://www.w3.org/TR/2017/REC-annotation-protocol-20170223/) - describes the transport mechanisms for creating and managing annotations in a method that is consistent with the Web Architecture and REST best practices.


