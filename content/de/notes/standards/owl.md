+++
title = "OWL"
+++


OWL = *Web Ontology Language*

## Bnodes

From a technical perspective they give the capability to:

1. describe **multi-component structures**, like the RDF containers,
2. describe **reification** (i.e. provenance information),
3. represent **complex attributes** without having to name explicitly the auxiliary node (e.g. the address of a person consisting of the street, the number, the postal code and the city) and
4. offer **protection** of the inner information (e.g. protecting the sensitive information of the customers
