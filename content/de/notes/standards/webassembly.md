+++
title = "WebAssembly"
+++


## Grundlagen

* Code, welcher in modernen Browsers laufen kann
* Low-level, Assembler-ähnlich; kompaktes binäres Format
* Kompilierziel für Sprachen wie [Rust](https://developer.mozilla.org/en-US/docs/WebAssembly/rust_to_wasm), [C](https://developer.mozilla.org/en-US/docs/WebAssembly/existing_C_to_wasm), [C++](https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm)
* Kann aus JavaScript aufgerufen werden ([WebAssembly JavaScript API](https://developer.mozilla.org/en-US/docs/WebAssembly/Using_the_JavaScript_API))
* [Webstandard](https://www.w3.org/wasm/)

## Konzepte

* **Module**: Represents a WebAssembly binary that has been compiled by the browser into executable machine code.  A Module is stateless and thus, like a [Blob](https://developer.mozilla.org/en-US/docs/Web/API/Blob), can be explicitly shared between windows and workers (via [postMessage()](https://developer.mozilla.org/en-US/docs/Web/API/MessagePort/postMessage)).  A Module declares imports and exports just like an ES2015 module.
* **Memory**: A resizable ArrayBuffer that contains the linear array of bytes read and written by WebAssembly’s low-level memory access instructions.
* **Table**: A resizable typed array of references (e.g. to functions) that could not otherwise be stored as raw bytes in Memory (for safety and portability reasons).
* **Instance**: A Module paired with all the state it uses at runtime including a Memory, Table, and set of imported values.  An Instance is like an ES2015 module that has been loaded into a particular global with a particular set of imports.

The JavaScript API provides developers with the ability to create modules, memories, tables, and instances.  Given a WebAssembly instance, JavaScript code can synchronously call its exports, which are exposed as normal JavaScript functions.  Arbitrary JavaScript functions can also be synchronously called by WebAssembly code by passing in those JavaScript functions as the imports to a WebAssembly instance.

Since JavaScript has complete control over how WebAssembly code is downloaded, compiled and run, JavaScript developers could even think of WebAssembly as just a JavaScript feature for efficiently generating high-performance functions.

## Tools

* [Emscripten](https://kripken.github.io/emscripten-site)
* [Compiling Rust to WebAssembly Guide](https://hackernoon.com/compiling-rust-to-webassembly-guide-411066a69fde)


