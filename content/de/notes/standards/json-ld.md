+++
title = "JSON-LD"
+++


## Dokumentformen

### Expanded

<http://www.w3.org/TR/json-ld/#expanded-document-form>
Profil-URI: <http://www.w3.org/ns/json-ld#expanded>

Alle IRIs, Typen und Werte sind in ausgeschriebenen Format, so dass kein eigener Kontext mehr nötig ist.

### Compacted

<http://www.w3.org/TR/json-ld/#compacted-document-form>
Profile-URI: <http://www.w3.org/ns/json-ld#compacted>

Gegenstück zur ausgeweiteten Dokumentenform: IRIs werden zu Termen bzw. abgekürzten IRIs, Werte zu einfachen Werttypen wie Strings oder Zahlen reduziert.

### Flattened

<http://www.w3.org/TR/json-ld/#flattened-document-form>
Profile-URI: <http://www.w3.org/ns/json-ld#flattened>

## Einige Links

* [Projektwebsite](http://json-ld.org)
* [W3C Recommendation zur Syntax](http://www.w3.org/TR/json-ld/)
* [W3C Recommendation zu den Processing Algorithms und zur API](http://www.w3.org/TR/json-ld-api/)
* [W3C Draft zum JSON-LD-Framing](http://json-ld.org/spec/latest/json-ld-framing/)
* [W3C Draft zum JSON-LD-Normalization](http://json-ld.org/spec/latest/rdf-dataset-normalization/)