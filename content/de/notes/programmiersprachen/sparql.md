+++
title = "SPARQL"
+++


## Übersicht


### Query Forms

*Query forms* dienen der Darstellung der gefundenen Triples. Es gibt vier Typen:

* [SELECT](https://www.w3.org/TR/2013/REC-sparql11-query-20130321/#select): Gibt alle, oder eine Untermenge, der Variablen zurück, nach welchen in einem Suchmuster gesucht wurde
* [CONSTRUCT](https://www.w3.org/TR/2013/REC-sparql11-query-20130321/#construct): Gibt einen RDF-Graph basierend auf einem Template zurück 
* [ASK](https://www.w3.org/TR/2013/REC-sparql11-query-20130321/#ask): Gibt ``true`` zurück, wenn eine Suche Treffer liefert, ansonsten ``false``
* [DESCRIBE](https://www.w3.org/TR/2013/REC-sparql11-query-20130321/#describe): Gibt einen RDF-Graphen zurück, welcher eine Ressource bzw. einen Graphen beschreibt


## Beispiele


### Basale Abfrage

```sparql
SELECT ?s ?p ?o
WHERE {?s ?p ?o}
```


### Statements zählen

```sparql
SELECT (COUNT (?s) AS ?count)
WHERE {?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/ontology/bibo/Document> }
```

### Distinkte Statements zählen

```sparql
SELECT (COUNT(DISTINCT (?o)) as ?count)
WHERE {?s <http://purl.org/dc/terms/contributor> ?o }
```


## Ressourcen

* [SPARQL 1.1 Query Langauge (W3C Recommendation)](https://www.w3.org/TR/2013/REC-sparql11-query-20130321/)
* [Cheat Sheet](https://moodle.polymtl.ca/pluginfile.php/518775/mod_resource/content/2/sparql-cheat-sheet.pdf)
