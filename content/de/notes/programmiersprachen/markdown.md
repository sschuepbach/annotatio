+++
title = "Markdown"
+++


## Syntax


### Absätze / Zeilenumbruch

* Absätze werden durch eine oder mehrere Leerzeilen (= Zeilen, die keine Zeichen, nur Leerzeichen und/oder Tabs enthalten) voneinander getrennt
* Um einen Zeilumbruch innerhalb eines Absatzes zu erzwingen, muss die Zeile mit zwei oder mehr Leerzeichen beendet werden


### Überschriften

```md
...
###### H6
```

### Blockzitate

* Blockzitate beginnen mit einem > .
* Verschachtelte Blöcke werden mit der Verwendung von mehreren ``>`` ausgezeichnet.


### Code

#### Codeblöcke

* Codeblöcke werden mit **vier Leerzeichen** oder **einem Tab** eingerückt.
* Eine Zeile ohne Einrückung beendet einen Codeblock.
* Markdown- und HTML-Syntax wird innerhalb von Codeblöcken nicht geparst.


#### Codeelemente

* Codeelemente werden mit Backticks (`````) umgrenzt
* Werden innerhalb des Codes Backticks verwendet, werden zur Umgrenzung doppelte Backticks verwendet
* Markdown- und HTML-Syntax wird innerhalb von Codeelementen nicht geparst.


### Listen / Aufzählungen

* Listen werden entweder mit ``-``, ``*`` oder ``+`` am jeweiligen Zeilenanfang ausgezeichnet
* Aufzählungen werden mit ``<Nummer>.`` am jeweiligen Zeilenanfang ausgezeichnet. Es spielt im Moment keine Rolle, welche Nummern eingesetzt oder ob sie konsekutiv verwendet werden. Allerdings wird empfohlen, mit 1 zu beginnen, um zukünftige Inkompatibilitäten zu vermeiden.
* Bei **mehreren Absätzen** unter einem Listen-/Aufzählungspunkt müssen folgende Absätze entweder mit **vier Leerzeichen** oder **einem Tab** eingerückt werden.
* Dasselbe gilt für **Blockzitate**
* Bei **Codeblöcken** müssen **zwei Tabs** oder **acht Leerzeichen** verwendet werden.


### Links

* Syntax: ``[<Verlinkter Text>](<URL/Pfad> "<Optionaler Linktext>")``
* Referenz:
	* Definition: ``[<Verlinkter Text>][<id>]``
	* Link: ``[<id>]: <URL/Pfad> "<Optionaler Linktext>"``
	* Wird in den zweiten eckigen Klammern in der Definition keine ID angegeben, wird der verlinkte Text als ID verwendet
	* Der optionale Linktext kann alternativ auch in eckigen oder runden Klammern stehen
* Links können auch direkt eingefügt werden: ``<http://example.com>``
* Oder (wobei Adresse obfusziert wird): ``<mail@example.com>``


### Bilder

* Syntax: ``![<Alternativer Text>](<URL/Pfad> "<Optionaler Titel>")``
* Referenz:
	* Definition: !``[<Alternativer Text>][<id>]``
	* Link: ``[<id>]: <URL/Pfad> "<Optionaler Titel>"``
* Um Dimensionen für Bilder zu definieren, muss das HTML-Tag ``<img>`` direkt verwendet werden


### Auszeichnungen

* ``*<Text>*`` und ``_<Text>_`` entspricht dem HTML-Tag ``<em>``
* ``**<Text>**`` bzw. ``__<Text>__`` entspricht dem HTML-Tag ``<strong>``
* Auszeichnungen können auch innerhalb eines Wortes verwendet werden
* Literale ``*`` und ``_`` müssen entweder mit Leerzeichen umgeben sein oder mit ``\`` maskiert werden


### Maskierung
Ein Backslash maskiert folgende Zeichen: ``\ ` * _ { } [ ] ( ) # + - . !``

### HTML
Anstelle oder in Ergänzung der Markdown-Syntax kann auch HTML direkt verwendet werden.

#### Blockelemente
In Markdown können HTML-[Blockelemente](https://en.wikipedia.org/wiki/HTML_element#Inline_elements) und -[Tabellen](https://en.wikipedia.org/wiki/HTML_element#Tables) verwendet werden. Zu beachten ist:

* Blöcke müssen durch je eine Leerzeile am Anfang und am Ende vom Rest des Textes getrennt werden
* Innerhalb von Blöcken wird keine Markdown-Syntax geparst


#### Inline
Im Gegenteil zu HTML-Blöcken können [Inlineelemente](https://en.wikipedia.org/wiki/HTML_element#Inline_elements) überall im Text benutzt werden, und Markdown-Auszeichnungen innerhalb eines Inlineelements werden auch geparst.
