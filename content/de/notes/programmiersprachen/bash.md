+++
title = "Bash"
+++


## Positionale Parameter

* ``$0``: Gibt normalerweise den Namen des ausgeführten Programms zurück, kann aber auch auf einen anderen Wert gesetzt werden. Daher kein eigentlicher positionaler Parameter.
* ``$FUNCNAME``: Name der Funktion
* ``$1`` - ``$9``: Argumentlisten-Elemente von 1 bis 9
* ``${10}`` - ``${N}``: Argumentlisten-Elemente über 9
* ``$*``: Alle positionalen Parameter mit Ausnahme von ``$0``
* ``$@``: Alle positionalen Parameter mit Ausnahme von ``$0``
* ``$#``: Die Anzahl Argumente, ``$0`` nicht mitgezählt


## Datenströme

Standard-Datenströme:
| Datenstrom   | Abkürzung   | Beschreibung          | Standardmässig verbunden mit |
|:-------------|:------------|:----------------------|------------------------------|
| `0`          | `STDIN`     | Standardeingabe       | Tastatur                     |
| `1`          | `STDOUT`    | Standardausgabe       | Monitor                      |
| `2`          | `STDERR`    | Standardfehlerausgabe | Monitor                      |


Daneben stehen noch weitere Datenströme, 3 bis 8, zur Verfügung.

### Umleitung von Datenströmen

| Kommando    | Beispiel                              | Bedeutung                                                                                                                   |
|:------------|:--------------------------------------|:----------------------------------------------------------------------------------------------------------------------------|
| `<`         | `cat < testfile`                      | Standardeingabe wird umgestellt (im Beispiel auf die Datei `testfile`)                                                      |
| `> oder 1>` | `echo "Hello world!" > testfile`      | Standardausgabe wird umgestellt (im Beispiel auf die Datei `testfile`) [gleichbedeutend mit `1>`]                           |
| `>>`        | `echo "Hello world!" >> testfile`     | Standardausgabe wird an Datei angehängt (im Beispiel auf `testfile`)                                                        |
| `2>`        | `ls -l /dev/null/void 2> testfile`    | Standardfehlerausgabe wird umgestellt (im Beispiel auf die Datei `testfile`)                                                |
| `&>`        | `ls $HOME /dev/null/void &> testfile` | Standardausgabe und Standardfehlerausgabe werden in gleiche Ausgabe umgeleitet (im Beispiel auf die Datei `testfile`)       |
| `>&1`       | `ls -l /dev/null/void >&1`            | Strom wird auf Standardausgabe umgeleitet                                                                                   |
| `>&2`       | `echo "Hello world!" >&2`             | Strom wird auf Standardfehlerausgabe umgeleitet                                                                             |
| `exec 1>`   | `exec 1> testfilev                    | Standardausgabe wird, bspw. in Skript oder in geöffneter, permanent umgestellt (im Beispiel auf die Datei `testfile`)       |
| `exec 0<`   | `exec 0< testfile`                    | Standardeingabe wird umgestellt (im Beispiel auf die Datei `testfile`)                                                      |
| `exec 3<>`  | `exec 3<> testfile`                   | Für Datenstrom Ziel sowohl als Ein- wie Ausgabe definieren (im Beispiel Ziel `testfile`)                                    |
| `>&-`       | `exec 3>&-`                           | Datenstrom schliessen (im Beispiel für Datenstrom `3`)                                                                      |
| `tee`       | `date \| tee testfile`                | Standardeingabe wird gleichzeitig nach Standardausgabe und einer weiteren Ausgabe weitergeleitet (im Beispiel `testfile`)   |


## Signale

| Signal   | Wert                 | Tastaturkürzel | Beschreibung                                                               |
|:---------|:---------------------|:---------------|:---------------------------------------------------------------------------|
| `1`      | `SIGHUP              |                | Blockierung des Kontrollterminals oder Ende des Kontrollprozess            |
| `2`      | `SIGINT              |                | Interrupt durch die Tastatur; interaktives Warnsignal                      |
| `3`      | `SIGQUIT             |                | Beenden durch die Tastatur                                                 |
| `4`      | `SIGILL              |                | Ungültige Anweisung                                                        |
| `5`      | `SIGTRAP             |                | Haltemarke erreicht                                                        |
| `6`      | `SIGABRT` / `SIGIOT` |                | Abnormale Beendigung                                                       |
| `7`      | `SIGBUS`             |                | BUS Fehler (Speicherzugriffsfehler)                                        |
| `8`      | `SIGFPE`             |                | „Gleitkommaoperation Ausnahmefehler“: fehlerhafte arithmetische Operation. |
| `9`      | `SIGKILL`            |                | Unblockierbares Beenden                                                    |


## Stringmanipulation

Für den Moment s. <http://www.tldp.org/LDP/abs/html/string-manipulation.html>

## Bash-Einzeiler

### Verschlüsseltes tar-Archiv extrahieren

```bash
gpg --decrypt einedatei.tar.gpg | tar xf -
```

### Symmetrische Verschlüsselung eines archivierten Verzeichnisses

```bash
tar cz mein_verzeichnis | gpg -co mein_archiv.tar.gpg
```

### Pfad zur ausführbaren Datei eines Prozesses

```bash
sudo readlink -f /proc/<pid>/exe
```

### Rekursives Suchen und Ersetzen

```bash
find -type f -exec sed -i 's/<alt>/<neu>/g' {} \;
```

### Pipe zu mehreren Commands

```bash
cat eine_datei | tee >(grep "ein_muster") >(grep "ein_weiteres_muster") >/dev/null | less
```

Mithilfe von ``tee`` und [Prozessersetzung](http://tldp.org/LDP/abs/html/process-sub.html) kann das Ergebnis eines Befehls an verschiedene weitere Befehle gepipt werden. Da ``tee`` standardmässig in Dateien (in diesem Beispiel sind das durch die Substitution die ``grep``-Befehle) und nach ``stdout`` schreibt, muss hier Letzteres nach ``/dev/null`` umgeleitet werden, da ansonsten sowohl das gesamte Ergebnis von ``cat`` als auch die Ergebnisse der ``grep``-Befehle an den letzten Befehl weitergeleitet werden.

### Zählen von abweichenden Zeilen in zwei Dateien

```bash
diff -y --suppress-common-lines erste_datei zweite_datei | grep '^' | wc -l
```