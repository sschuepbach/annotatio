+++
title = 'Hamcrest-Matchers'
+++

Machen Code für Assertions lesbarer. Dazu stellt Hamcrest in den Klassen [CoreMatchers](http://hamcrest.org/JavaHamcrest/javadoc/2.0.0.0/org/hamcrest/CoreMatchers.html) und [Matchers](http://hamcrest.org/JavaHamcrest/javadoc/2.0.0.0/org/hamcrest/Matchers.html) eine Reihe von Matchers für diverse Typen zur Verfügung.
Bspw.:
``assertThat(<methode>, hasSize(...));``

## Setup

	import static org.hamcrest.CoreMatchers.*
	import static org.hamcrest.Matchers.*

