+++
title = 'Java - Begriffe'
date = '2015-03-09'
+++


## Begriffe

**associativity**				Konzept, welches die Reihenfolge der Abarbeitung insbesondere von Operatoren der gleichen Präzendenz-Kategorie vorgibt (links-nach-rechts oder rechts-nach-links) (JiN 31f.)
**casting**					Erzwingen der Konvertierung eines Wertes eines Datentyps in einen Datentyp mit kleinerem Wertebereich (s. auch *narrowing conversion*) (JiN 29)
**compound statement**		Mehrere Anweisungen, welcher mit geschweiften Klammern gruppiert werden (JiN 48)
**expression statement**		Ein Ausdruck mit "Nebenwirkung" (s. *side effect*), abgeschlossen mit einem Semikolon (JiN 47f.)
**identifier**				Name (einer Methode, Klasse, Variable etc.) (JiN 21)
**initializer**				Ausdruck, der einen Initialwert für eine Variable spezifiert (JiN 49)
**lexical token**				Syntaktische Basiseinheit (JiN 18)
**literal**					Wert, der direkt im Quelltext vorkommt (JiN 21)
**method overloading**		Definition einer Reihe gleichnamiger Methoden, um sie mit verschiedenen Datentypen einsetzen zu können (JiN 67f.)
**narrowing conversion**		Konvertierung eines Wertes eines Datentyps in einen Datentyp mit kleinerem Wertebereich (JiN 28)
**precedence**				Konzept, welches die Reihenfolge der Abarbeitung von verschiedenen Typen von Operatoren definiert (JiN 31)
**primary expression**		Einfachste Form eines Ausdrucks, d.h. ein einzelner Wert (Zeichen, Zahl etc.) (JiN 30)
**primitive types**			Einfache Datentypen (Boolesche Werte, numerische und Gleitkommazahlen sowie Unicode-Zeichen) (JiN 22ff.)
**side effect**				Attribut eines Ausdrucks, das bewirkt, das die Re-Evaluierung des Ausdrucks zu einem anderen Resultat führen kann (bspw. Inkrementierung eines Wertes) (JiN 35)
**sign extension**			Technik, bei welcher bei einem bitweisen Shifting nach rechts das Vorzeichen beibehalten wird (JiN 42)
**signature**				Spezifische Elemente einer Methode: Name, Anzahl, Reihenfolge, Datentyp und Name der Parameter, Datentyp des Rückgabewerts, definierte Ausnahmen sowie die Modifikatoren (JiN 66)
**statement**				Basale Ausführungseinheit im Java-Quelltext (eine Anweisung oder solitäre "Absichtserklärung" des Programmierers), die im Gegensatz zu einem Ausdruck aber keinen Wert repräsentiert (JiN 46ff.)
**widening conversion**		Konvertierung eines Wertes eines Datentyps in einen Datentyp mit grösserem Wertebereich (JiN 28)
**zero extension**			Technik, bei welcher bei einem bitweisen Shifting nach rechts das Vorzeichen nicht beibehalten wird und die nachrückenden Bits auf 0 gesetzt werden (JiN 42f.)


## Websites


## Literatur

JiN	Evans, Benjamin J. & Flanagan, David. Java in a Nutshell. Sixth Edition. Sebastopol 2014.

@Java @Programmiersprache @IT @Glossar

