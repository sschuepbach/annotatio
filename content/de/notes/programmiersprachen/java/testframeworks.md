+++
title = 'Testframeworks'
+++


## Testframeworks

### JUnit
Das neue JUnit5 unterscheidet sich stark von seinem Vorgänger JUnit4.

### TestNG
TestNG zielt anders als JUnit nicht nur auf Unit Tests ab, sondern bietet auch Unterstützung bei funktionalen, E2E-, Integrations- und weiteren Testarten.

### Spock Framework
<http://spockframework.org>

## Spezialisierte Tools

### AssertJ
AssertJ erlaubt das Schreiben von Assertions im "fluent style":
<https://joel-costigliola.github.io/assertj/index.html>

### Hamcrest

### QuickTheories
<https://github.com/ncredinburgh/QuickTheories>

### JUnit-quickcheck
<http://pholser.github.io/junit-quickcheck>

### Mockito
Mocking Framework für Unit Tests
<http://site.mockito.org>

### H2

## Integration in Build Tools

* <https://github.com/unbroken-dome/gradle-testsets-plugin>


## Ressourcen

* <https://www.petrikainulainen.net/programming/gradle/getting-started-with-gradle-integration-testing-with-the-testsets-plugin/>
* <https://www.petrikainulainen.net/programming/testing/12-tools-that-i-use-for-writing-unit-and-integration-tests/>
* <https://orrsella.com/2014/10/28/embedded-elasticsearch-server-for-scala-integration-tests/>


