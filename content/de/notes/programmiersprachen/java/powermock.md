+++
title = 'PowerMock'
+++

[PowerMock](https://github.com/powermock/powermock) bietet eine Reihe von Möglichkeiten an, die [Mockito](./Mockito.md) nicht anbietet (testen von ``private``- und ``final``-Methoden sowie von *constructors*).

Benötigte Abhängigkeiten zur Verwendung mit Mockito und JUnit:

* ``org.powermock:powermock-module-junit4``
* ``org.powermock:powermock-api-mockito``


## Statische Methoden

Hinweis: Es wird normalerweise nicht empfohlen, statische Methoden zu testen

* Spezifischer *Runner*: ``@RunWith(PowerMockRunner.class)``
* Zu mockende Klasse initialisieren. Bspw.: ``PowerMockito.mockStatic(UtilityClass.class);`` sowie auf Klassenebene: ``@PrepareForTest(UtilityClass.class)``
* ``mock`` ausführen


Um das Ausführen einer Methode zu verifizieren:
	PowerMockito.verifyStatic(); // verify the next call to a static method
	UtilityClass.staticMethod(6);


## Private Methoden

Hinweis: Es wird normalerweise nicht empfohlen, private Methoden zu testen
Um private Methoden zu testen, muss die Utility-Klasse ``Whitebox`` mit der Methode ``invokeMethod`` verwendet werden: ``Whitebox.invokeMethod(<zu_mockende_klasse>, <beschreibung>);`` Die zu mockende Klasse wird mit ``@InjectMock`` injiziert (s. [Mockito](./Mockito.md)).

## Constructors

Hinweis: Es wird normalerweise nicht empfohlen, Konstruktoren zu testen

* PrepareForTest-Annotation: Klasse, welche den Konstruktor einer anderen Klasse anwendet. Bspw. wenn der Konstruktor von ``ArrayList`` in Klasse ``SystemUnderTest`` gemockt werden soll: ``@PrepareForTest(SystemUnderTest.class)``
* Konstruktor überschreiben. Bspw.: ``PowerMockito.whenNew(ArrayList.class).withAnyArguments().thenReturn(mockList);`` (wobei ``mockList`` vom Typ ``ArrayList`` ist)


