+++
bookCollapseSection = true
title = 'Java'
date = '2015-10-15'
+++


## Generics

19. auch <https://docs.oracle.com/javase/tutorial/java/generics/index.html>


### Konventionen für Typparameter

* ``E``: Element (oft vom Java Collections Framework gebraucht)
* ``K``: Schlüssel
* ``N``: Zahl
* ``T``: Typ
* ``V``: Wert
* ``S``,``U``,``V`` etc. - zweiter, dritter, vierter etc. Typ


### Upper Bound
``? extends T``
Der Typ muss eine Unterklasse von T sein.
Es ist möglich, dass ein Typ mehrere Oberklassen bindet. Dies wird folgendermassen ausgedrückt: ``? extends T & U``

### Lower Bound
``? super T``
Der Typ muss eine Oberklasse von T sein.

### PECS-Regel
**Producer extends, Consumer super**: Eine Faustregel, wann für eine Collection ein Typparameter mit *Upper Bound* bzw. *Lower Bound* verwendet werden soll:

* Wenn die typisierte Collection ein Producer ist, ihr also Objekte entnommen und diese verarbeitet werden, dann wird sie mit einem Upper Bound deklariert. So ist es immer möglich, ein Element der Collection einer Variablen vom Typ der Schranke zuzuweisen und damit zu arbeiten.
* Ist die Collection dagegen ein Consumer, werden ihr also Objekte hinzugefügt, dann wird sie mit einem Lower Bound deklariert, denn so können neue Objekte vom Typ der Schranke immer hinzugefügt werden.


## Ändern der Java-Version in Archlinux

	archlinux-java status			# Listet verfügbare Versionen auf
	archlinux-java set <Version>	# Setzt angegebene Version als Default
	archlinux-java unset <Version>  # Löscht angegebene Version als Default


## Heap-Grösse ändern

	-Xms<size>        	set initial Java heap size
	-Xmx<size>        	set maximum Java heap size
	-Xss<size>        	set java thread stack size


Bsp.:
``java -Xms64m -Xmx256m HelloWorld``

Für eine dauerhafte Änderung können die Optionen auch in der Umgebungsvariable ``$_JAVA_OPTIONS`` exportiert werden. Bsp.:
``export _JAVA_OPTIONS="$_JAVA_OPTIONS -Xmx2048m"``

