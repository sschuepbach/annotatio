+++
title = 'Liste von Scala-Applikationen'
+++


## HTTP Servers / Web Frameworks

* [Colossus](https://tumblr.github.io/colossus/): Colossus is a lightweight framework for building high-performance network I/O applications in Scala. In particular Colossus is focused on low-latency microservices, single-feature stateless applications with a RESTful API. Colossus aims to maximize performance while keeping the interface clean and concise.
* [Finatra](https://twitter.github.io/finatra/): Fast, testable, Scala services built on TwitterServer and Finagle.
* [http4s](http://http4s.org/): A typeful, purely functional, streaming library for HTTP clients and servers in Scala.
* [Play Framework](https://www.playframework.com/): Play is based on a lightweight, stateless, web-friendly architecture. Built on Akka, Play provides predictable and minimal resource consumption (CPU, memory, threads) for highly-scalable applications.
* [Scalatra](http://scalatra.org/): Scalatra is a simple, accessible and free web micro-framework. It combines the power of the JVM with the beauty and brevity of Scala, helping you quickly build high-performance web sites and APIs.
* [skinny](http://skinny-framework.org/): Skinny is a full-stack web app framework built on Skinny Micro.. To put it simply, Skinny framework’s concept is Scala on Rails. Skinny is highly inspired by Ruby on Rails and it is optimized for sustainable productivity for Servlet-based web app development.
* [spray](http://spray.io/): spray is an open-source toolkit for building REST/HTTP-based integration layers on top of Scala and Akka. Being asynchronous, actor-based, fast, lightweight, modular and testable it's a great way to connect your Scala applications to the world.


