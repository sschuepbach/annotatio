+++
title = 'Scala-Glossar'
+++


* Ad hoc polymorphism
* Antipattern
* Cake pattern
* Closure
* Combinator
* Companion object
* Compound type: An instance that combines several types (e.g. val c = new C with T1 with T2)
* Context bound
* Contravariant
* Covariant
* Currying: Transforms a function that takes multiple arguments into a chain of functions, each taking a single argument
* Expression problem
* Factory
* Free variable
* Function literal
* Guard
* Implicit conversion
* Implicit evidence
* Least upper bound (LUB): The closest supertype of the original type and the type of the new element
* Lifting
* Linearization: Refers to the algorithm used to „flatten“ this graph for the purposes of resolving method lookup priorities, constructor invocation order, binding of super, etc.
* Memoization
* Miniboxing
* Mixin
* Nominal type: Named type
* Observer Design Pattern
* Package object
* Parametrized type
* Partial application
* Partial function
* Partially applied function
* Pattern matching
* Persistent data structure
* Phantom type
* Pre-initialized fields
* Primary constructor
* Property readers
* Referential transparency
* Scoped private / protected
* Self-type annotation
* Singleton
* Singleton Design Pattern
* Singleton type
* Structural type
* Structure sharing
* Subtype polymorphism
* Template Method Pattern
* Trampoline: A loop that works through a list of functions, calling each one in turn
* Type annotations
* Type Class Pattern
* Type constructor
* Type erasure
* Type refinement
* Uniform Access Principle
* Variable-length argument


