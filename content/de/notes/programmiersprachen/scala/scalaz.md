+++
title = 'Scalaz'
+++

Scalaz besteht aus drei Teilen:

* Neue Datentypen
* Erweiterungen von Standardklassen von Scala
* Implementierung von jeder einzigen generellen Funktion, die benötigt wird


Verschiedene Arten von Polymorphismus in Scala:

* **Parametrischer Polymorphismus** ist gegeben, wenn der Typ eines Wertes einen oder mehrere ungebundene **Typvariablen** enthält, so dass der Wert jeden Typ annehmen kann, der durch die Ersetzung der Typvariablen durch konkrete Typen hervorgeht.
* Subtyp-Polymorphismus:

	trait Plus[A] {
	def plus(a2: A): A
	}
	def plus[A <: Plus[A]](a1: A, a2: A): A = a1.plus(a2)
Dieser Polymorphismus ist insofern nicht flexibel genug, da er bei der Definition des konkreten Datentyps eingemischt werden muss. D.h., es kann nicht für vordefinierte Datentypen wie ``Int`` oder ``String`` benutzt werden

* Ad-hoc-Polymorphismus:

	trait Plus[A] {
	def plus(a1: A, a2: A): A
	}
	def plus[A: Plus](a1: A, a2: A): A = implicitly[Plus[A]].plus(a1, a2)
Der Ad-hoc-Polymorphismus beseitigt das erwähnte Problem des Subtyp-Polymorphismus, indem:

* separate Funktionen für verschiedene konkrete Typen von A definiert werden können
* Funktionen auch für Typen definiert werden können, auf deren Quellcode nicht zugegriffen werden kann
* Funktionen in verschiedenen *scopes *benutzt oder weggelassen werden können.


## Ressourcen

* [Github-Repository](https://github.com/scalaz/scalaz)
* [Learning Scalaz](http://eed3si9n.com/learning-scalaz/)
* [Scalaz cheat sheet](http://eed3si9n.com/scalaz-cheat-sheet)
* [Scalaz presentation by Nick Partridge](https://vimeo.com/10482466) at Melbourne Scala Users Group on March 22, 2010



* [Learn You a Haskell for Great Good!](http://learnyouahaskell.com/)


