+++
bookCollapseSection = true
title = 'TypeScript'
+++


## Typen

### Variablendeklarierung

* ``var``: Im Gegensatz zu vielen anderen Sprachen ist in Typescript (und Javascript) der Gültigkeitsbereich von Variablen nicht auf den Block beschränkt, in dem sie definiert ist, sondern auf die Funktion. So hat bspw. eine in einer for-Schleife deklarierte Variable auch Gültigkeit ausserhalb dieser Schlaufe, sofern der Funktionsbereich nicht verlassen wird.
* ``let``: Deklariert eine Variable mit einem block-orientierten Gültigkeitsbereich. Wenn möglich immer vorzuziehen.
* ``const``: Deklariert eine konstante (nicht-redeklarierbare) Variable (analog zum val-Schlüsselwort in Scala)


### Typsystem

<https://www.typescriptlang.org/docs/handbook/2/everyday-types.html>

#### boolean

{{< hint info >}}
Die Typen `Boolean`, `String` und `Number` (Grossschreibung beachten) werden von TypeScript intern verwendet und sollten nie für eigene Werte verwendet werden.
{{< /hint >}}

```ts
const istWahr: boolean = true;
```

#### string

```ts
const einString: string= "Das ist ein String";
```

#### number

```ts
const eineZahl: number = 12.34;
```

#### any

`any` ist ein nicht näher spezifizierter Typ.

```ts
let irgendetwas: any = 0;
irgendetwas = "abc";
irgendetwas = true;
```

#### null und undefined

`null` und `undefined` sind Subtypen aller anderen Typen und bezeichnen nicht vorhandene bzw. uninitialisierte Werte. 

```ts
const u: undefined = undefined;
const n: null = null;
```

Das Verhalten von `null` bzw. `undefined` hängt davon ab, ob die Einstellung `strictNullChecks` gesetzt ist.

Wenn `strictNullChecks` gesetzt ist, müssen Variablen, die potentiell `null` oder `undefined` sein können, auf `null`/`undefined` getestet werden, bevor auf sie Methoden angewendet werden, welche einen vorhandenen und initialisierten Wert voraussetzen:

```ts
function doSomething(x: string | null) {
    if (x !== null) {
	    console.log("Hello, " + x.toUpperCase());
	}
}
```

Zusätzlich bietet TypeScript den `!`-Operator an, um `null`/`undefined`-Werte zu "entpacken".

```ts
function liveDangerously(x: number | null) {
  console.log(x!.toFixed());
}
```

Falls `x === null` führt das zu einem Laufzeitfehler.

#### void

`void` ist der Rückgabetyp einer Funktion, die keinen Wert zurückgibt.

```ts
function log(msg: string): void {
	console.log(msg);
}
```

#### never

`never` ist der Rückgabetyp einer Funktion, die nie einen Wert zurückgeben kann.

```ts
function fail() {
	return error("Something failed");
}
```

#### Array

```ts
const cards: string[] = ['Visa', 'Maste­rCa­rd'];

// Alternative Syntax:
const cards: Array<string> = ['Visa', 'Maste­rCa­rd'];
```

#### Tuple

Ein Tuple ist in TypeScript ein Array-Typ mit einer fixen Anzahl an Elementen mit potentiell unterschiedlichem Typ.

```ts
const someTuple: [string, number] = ["abc", 2];
const firstElement = someTuple[0];
```

## Zugriffsmodifikatoren

* ``public``: Genereller Zugriff (Standard)
* ``protected``: Zugriff nur von Members der Klasse oder der Members ihrer Erweiterungen
* ``private``: Zugriff nur von Members der Klasse
* ``readonly``: Für Parameter eines komplexen Typs oder Felder einer Klasse: Parameter kann in Instanz nicht redeklariert werden


