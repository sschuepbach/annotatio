+++
title = 'Beautiful Soup'
+++


## Download einer entfernten Website

	from bs4 import BeautifulSoup
	from urllib import request
	 
	url = request.urlopen('https://example.com')
	soup = (url, 'html.parser') # Es stehen auch andere Parser zur Verfügung, s.u.


Weitere Ressourcen:

* [Verfügbare Parser](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#installing-a-parser)


## Weitere Ressourcen

* [Offizielle Dokumentation](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)


