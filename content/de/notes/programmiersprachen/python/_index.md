+++
bookCollapseSection = true
title = 'Python'
+++


## Strings mit str.format() formatieren

19. <https://docs.python.org/3/library/string.html#formatstrings>


## Dokumentationen häufig gebrauchter Module und Klassen

* [argparse](https://docs.python.org/3/library/argparse.html)
* [Beautiful Soup](./Python/Beautiful_Soup.md)
* [datetime](https://docs.python.org/3/library/datetime.html)
* [os](https://docs.python.org/3/library/os.html)
* [re](https://docs.python.org/3/library/re.html)
* [subprocess](https://docs.python.org/3/library/subprocess.html)
* [urllib.request](https://docs.python.org/3/library/urllib.request.html#module-urllib.request)


