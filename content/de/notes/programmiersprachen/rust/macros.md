+++
title = 'Macros'
+++

In Rust existieren zwei Typen von Macros:

* [Declarative Macros](https://doc.rust-lang.org/reference/macros-by-example.html) (``macro_rules! ...``)
* **Procedural Macros**
	* [Derive macros](https://doc.rust-lang.org/reference/procedural-macros.html#derive-macros) (``#[derive(CustomDerive)]``)
	* [Function-like macros](https://doc.rust-lang.org/reference/procedural-macros.html#function-like-procedural-macros) (``custom!(...)``)
	* [Attribute macros](https://doc.rust-lang.org/reference/procedural-macros.html#attribute-macros)** (**``#CustomAttribute]``)


## Declarative Macros

Werden mit dem ``macro_rules!``-Makro definiert:
	macro_rules! test {
	  () => {}
	}

Dieses Macro macht zwar noch gar nichts, doch zeigt es schon alle wichtigen Bestandteile eines deklarativen Makros:

* ``macro_rules!``
* Name des Makros (hier ``test``)
* Pattern matching in geschweiften Klammern, wobei
	* ``()`` der *Matcher* und
	* ``{}`` der *Transcriber* ist

Der Matcher besteht üblicherweise aus zwei Teilen,

* einem Namen, der im Transcriber gebunden wird, und der immer mit ``$`` anfangen muss, und, durch ein ``:`` getrennt,
* ein *Designator*, welcher den Typ definiert, den es zu matchen gilt.


### Designators
| Type  | Matches                                                        | Allowed suffixes             |
|:------|:---------------------------------------------------------------|:-----------------------------|
| expr  | Expression                                                     | => , ;                       |
| stmt  | Expression / declaration, not including any trailing semicolon | => , ;                       |
| ty    | Type                                                           | => , ; = \| { [ : > as where |
| path  | Path                                                           | => , ; = \| { [ : > as where |
| pat   | Pattern                                                        | => , = \| if in              |
| item  | Item                                                           | Anything                     |
| block | Block                                                          | Anything                     |
| meta  | Body of an attribute                                           | Anything                     |
| ident | Identifier                                                     | Anything                     |
| tt    | Token tree                                                     | Anything                     |


## Procedural Macros

## Ressourcen

"Offizielle" Ressourcen:

* [Macros in "The Book"](https://doc.rust-lang.org/book/ch19-06-macros.html)
* [macro_rules! in Rust by Examples](https://doc.rust-lang.org/stable/rust-by-example/macros.html)
* [Macros in der Rust Reference](https://doc.rust-lang.org/reference/macros.html)
* [Procedural Macros in der Rust Reference](https://doc.rust-lang.org/reference/procedural-macros.html)
* [Macros im 2018 Edition Book](https://doc.rust-lang.org/edition-guide/rust-2018/macros/index.html)


Sonstige:

* [A Beginner's Guide to Rust Macros](https://medium.com/@phoomparin/a-beginners-guide-to-rust-macros-5c75594498f1)
* [Procedural Macros in Rust 2018](https://blog.rust-lang.org/2018/12/21/Procedural-Macros-in-Rust-2018.html)
* [A Practical Intro to Macros in Rust 1.0](https://danielkeep.github.io/practical-intro-to-macros.html)


