+++
title = 'sccache'
date = '2021-09-28'
+++


Problem mit nicht mehr vorhandenen tmp-Verzeichnissen in nvim (s. <https://github.com/mozilla/sccache/issues/837>)
	sccache --stop-server
	sccache --start-server

