+++
title = 'Actix'
+++

<https://actix.rs>

## Subprojects

<https://crates.io/teams/github:actix:core>

### Actix
Actor framework for Rust

### Actix web
Actix web is a small, pragmatic, and extremely fast rust web framework

Crates:

* awc
* actix-http
* actix-cors
* actix-files
* actix-framed
* actix-session
* actix-identity
* actix-multipar
* actix-web-actors
* actix-web-codegen


### Actix net Framework
Framework for composable networking services

Crates:

* actix-codec
* actix-connect
* actix-ioframe
* actix-rt
* actix-macros
* actix-service
* actix-server
* actix-testing
* actix-threadpool
* actix-tls
* actix-tracing
* actix-utils


### Actix extras
A collection of additional crates supporting the actix and actix-web frameworks

* actix-rt



### Actix derive
Derive macro for actix framework 

