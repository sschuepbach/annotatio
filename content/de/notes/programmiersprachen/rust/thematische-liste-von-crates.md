+++
title = 'Thematische Liste von Crates'
+++


## CLI

* [getopts](https://crates.io/crates/getopts) ([Code](https://github.com/rust-lang-nursery/getopts), [Dokumentation](https://docs.rs/getopts)): Simpler CLI-Argumente-Parser
* [argparse](https://crates.io/crates/argparse) ([Code & Dokumentation](https://github.com/tailhook/rust-argparse)): Erweiterter CLI-Argumente-Parser
* [clap](https://crates.io/crates/clap) ([Code](https://github.com/kbknapp/clap-rs), [Dokumentation](https://docs.rs/clap/), [Website](https://clap.rs)): Umfangreicher CLI-Argumente-Parser (u.a. Farben, Autovervollständigung für Shell)
* [structopt](https://crates.io/crates/structopt) ([Code](https://github.com/TeXitoi/structopt), [Dokumentation](https://docs.rs/structopt)): Bietet ein ``derive``-Makro, um ``clap``-Definitionen aus einem ``struct`` zu generieren


## Error Handling

* error_chain
* [failure](https://crates.io/crates/failure)


## IO

* [flate2](https://crates.io/crates/flate2) ([Code](https://github.com/alexcrichton/flate2-rs), [Dokumentation](https://docs.rs/flate2)): A streaming compression/decompression library DEFLATE-based streams


## Logging

* log: Logging-Fassade
* slog: Erweiterte Logging-Suite


## Parallelisierung

* [rayon](https://crates.io/crates/rayon) ([Code](https://github.com/rayon-rs/rayon), [Dokumentation](https://docs.rs/rayon)):


## Parser

* [marc](https://crates.io/crates/marc) ([Code](https://github.com/blackbeam/rust-marc), [Dokumentation](https://blackbeam.github.io/rust-marc)): 
* nom: Parser 
* pest: Parser
* [quick_xml](https://crates.io/crates/quick-xml) ([Code](https://github.com/tafia/quick-xml), [Dokumentation](https://docs.rs/quick-xml)):
* [serde](https://serde.rs/): Serialisierungs-/Deserialisierungs-Framework. Spezifische Formate / Utilities in spezialisierten crates, u.a.
	* [serde_json](https://crates.io/crates/serde_json) ([Code](https://github.com/serde-rs/json), [Dokumentation](http://docs.serde.rs/serde_json/)): Für JSON
	* [serde_yaml](https://crates.io/crates/serde_yaml): Für YAML
	* [serde_derive](https://crates.io/crates/serde_derive): Derive macro zur Serialisierung von bzw. Deserialisierung in structs
	* [toml](https://crates.io/crates/toml): Für TOML


## Testing

* [hamcrest](https://crates.io/crates/hamcrest) ([Code](https://github.com/ujh/hamcrest-rust), [Dokumentation](https://docs.rs/hamcrest)): Hamcrest-Port
* [mockito](https://crates.io/crates/mockito) ([Code](https://github.com/lipanski/mockito), [Dokumentation](https://docs.rs/mockito)): HTTP mocking
* [quickcheck](https://crates.io/crates/quickcheck) ([Code](https://github.com/BurntSushi/quickcheck), [Dokumentation](http://burntsushi.net/rustdoc/quickcheck/)): Framework für Property based testing
* [proptest](https://crates.io/crates/proptest) ([Code](https://github.com/altsysrq/proptest), [Dokumentation](https://altsysrq.github.io/rustdoc/proptest/0.8.7/proptest/)): Weiteres Framework für property based testing
* [assert_cmd](https://crates.io/crates/assert_cmd) ([Code](https://github.com/assert-rs/assert_cmd), [Dokumentation](http://docs.rs/assert_cmd/)): Testet ``process::Command``, kann also für das Integration Testing von Kommandozeilen-Applikationen gebraucht werden
* [predicates](https://crates.io/crates/predicates) ([Code](https://github.com/assert-rs/predicates-rs), [Dokumentation](https://docs.rs/predicates)): An implementation of boolean-valued predicate functions in Rust
* [tempfile](https://crates.io/crates/tempfile) ([Code](https://github.com/Stebalien/tempfile), [Dokumentation,](https://docs.rs/tempfile) [Website](https://stebalien.com/projects/tempfile-rs/)): Hilft beim Erstellen von temporären Dateien


## Textprozessierung

* [difference](https://crates.io/crates/difference) ([Code](https://github.com/johannhof/difference.rs), [Dokumentation](https://johannhof.github.io/difference.rs/difference/index.html)): Text diff Bibliothek mit Assertion-Funktionen


## Web

* [reqwest](https://crates.io/crates/reqwest): HTTP client
* [actix-web](https://crates.io/crates/actix-web): Sehr schnelles web framework
* rocket: web framework (flachere Lernkurve als actix-web, "batteries included")
* hyper: Low-level HTTP client/server framework
* Für weitere web frameworks s. <https://github.com/flosse/rust-web-framework-comparison>


## Ressourcen

* [The Evolution of a Rust Programmer](https://blog.antoyo.xyz/evolution-rust-programmer)


