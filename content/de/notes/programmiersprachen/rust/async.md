+++
title = 'async'
+++

Asynchronous code allows us to run multiple tasks concurrently on the same OS thread.

Nachteile der Verwendung von verschiedenen OS-Threads:

* A lot of overhead involved in the proces of switching between different threads and sharing data between threads
* Even a thread which just sits and does nothing uses up valuable system resources


However, asynchronous functions require special support from the language or libraries. In Rust, ``async fn`` creates an asynchronous function which returns a ``Future``. To execute the body of the function, the returned ``Future`` must be run to completion.


* ``async`` / ``await`` language feature
* ``Feature`` / ``Pin`` in std (required for ``async`` / ``await``)
* ``futures`` crate
* Tokio etc.


