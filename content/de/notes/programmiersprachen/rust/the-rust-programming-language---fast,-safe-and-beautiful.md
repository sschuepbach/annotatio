+++
title = 'The Rust Programming Language - Fast, Safe and Beautiful'
+++

Memory safety:

* Threads never share mutable values directly
* Communication occurs through primitives designed for the purpose
* Non-deterministic behavior is localized


``unsafe``-Blocks erlauben u.a. folgende Dinge:

* raw pointers
* C code
* Inline assembly


Basic syntax

* ``if E { S2 } else { S2 } // Auch für ternary expressions``
* ``while E { S }``
* ``for V in E { S }``
* ``loop { S }``
* ``let i : [i32; 2] = [7, 11] // Array``
* ``fn parse(text : &str) -> i32 { E }``


Semikolon am Ende einer Zeile heisst: "Verwerfe den Wert dieses Ausdrucks"

Rust garantiert nicht, dass Tail Recursion in konstanter Zeit geschieht. Daher prinzipiell auf Rekursion verzichten.

Literals können mit Postfix versehen werden, um den Typ zu explizieren. Bspw. ``3.``, ``42i8``, ``-64is`` (für ``isize``), ``b'H``' (im Gegensatz zu '``H``', das 32 bit belegt.

Owning types:

* ``[T;M]``: Array mit ``T``, fixe Grösse
* ``Vec<T>``: Vektor (variable Grösse)
* String
* ``std::colletions::HashMap<K,V>``
* ``Box<T>``: Owning pointer to ``T``


Borrowed pointer types (never null):

* ``&T``: immutable reference to ``T``
* ``&mut T``: mutable reference to ``T``
* ``&[T]``: slice (pointer with length of ``T``)
* ``&mut [T]``: mutable slice of ``T``
* ``&str``: slice of UTF-8 string (always immutable)


Three rules of borrowing:

* Either **one mutable borrow**, or **any number of immutable borrows** at a time
* No changing values immutably borrowed
* No using values at all while mutably borrowed


Weitere Typen:

* *Tuples*: ``(T1, T2, ...)`` (literale Form analog)
* *Structs*: ``struct Name { V: T, ... }`` (literale Form: ``Name { V: T, ... }``). *Structs* können auch generifiziert werden
* *Enums*: Da Enums Typen haben können, können sie auch in der Form von *case classes* verwendet werden 


Traits
"Traits are the way you get dynamic dispatch in Rust"
	trait ToString {
		fn to_string(&self) -> String;
	}
	
	impl Trait for Type {
		fn ...
	}


Deklaration von Modulen:

* In-place: ``mod N { items }``
* Extern: ``mod N;`` Dabei kann das Modul ``N`` an zwei Orten abgelegt werden:
	* In einer Datei namens ``N.rs`` im gleichen Verzeichnis, oder
	* in einem Unterverzeichnis ``N`` in der Datei ``mod.rs``


Vorsicht: Kind-Module sind standardmässig *private*

