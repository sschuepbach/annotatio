+++
title = 'Important Traits in the Standard Library'
+++


## std::borrow: Traits for working with borrowed data

### Borrow / BorrowMut
Conversion traits, like ``AsRef``/``AsMut``, but additionally guaranteeing consistent hashing, ordering, and equality.

### ToOwned
Conversion trait for converting a reference to an owned value.

## std::clone

Contains only one trait, ``Clone``. Types which implement this trait can't be copied "implicitly", but have to be cloned by using the ``clone`` method. Mutually exclusive with ``Copy``.

## std::convert: Conversion traits

### AsMut / AsRef
Conversion traits for borrwing one type of reference from another.

### From / Into
Conversion traits for transforming one type of value into another.

## std::default

Contains only one trait, ``Default``. Used by types which may have meaningful default values.

## std::marker

Primitive traits representing basic properties of types

### Copy
Trait for types that can be cloned simply by making a byte-for-byte copy of the memory containing the value. Mutually exclusive with ``Clone``.

### Send
Types that can be transferred across thread boundaries. Automatically implmented when the compiler determines it's appropriate.

### Sized
Trait for types with a fixed size known at compile time, as opposed to types (such as slices) that are dynamically sized.

### Sync
Types for which it is safe to share references between threads. Automatically implmented when the compiler determines it's appropriate.

## std::ops: Overloadable operators

Many operators are overloadable by implementing its base trait. These traits are provided by this module.

### Deref / DerefMut
Traits for smart pointer types.

### Drop
Destructors. Cleanup code that Rust runs automatically whenever a value is dropped.

