+++
title = "Cypher"
+++


Cypher ist eine Abfragesprache für [Neo4j](../Programme/Neo4j.md).

## Beispiele

### Alle Knoten mit Label X zurückgeben

	MATCH (x:X)
	RETURN x


### Eigenschaft y und z von allen Knoten mit Label X zurückgeben

	MATCH (x:X)
	RETURN x.y, x.z


### Knoten mit Label X finden, welche eine gerichtete Beziehung zu Knoten mit Label Y haben

	MATCH (x:X) --> (y:Y)
	RETURN x


### Knoten mit Label X finden, welche von Knoten mit Label Y referiert werden
	
	MATCH (x:X) <-- (y:Y)
	RETURN x


### Knoten mit Label X finden, welche Knoten mit Label Y referieren oder von einem solchen referiert werden
	
	MATCH (x:X) -- (y:Y)
	RETURN x


### Knoten finden, welche keine Beziehungen zu einem anderen Knoten haben
	
	MATCH (n) 
	WHERE NOT (n)--() 
	RETURN n;


### Art der Beziehung zwischen Label X und Label Y herausfinden
	
	MATCH (:X) -[r]- (:Y)
	RETURN r


### Knoten mit Label X finden, welche über zwei Schritte mit Knoten mit Label Y verbunden sind

	MATCH (x:X) -[*2]- (:Y)
	RETURN x


### Knoten mit Label X inklusive Relationen finden, welche über zwei bis vier Schritte mit Knoten mit Label Y verbunden sind

	MATCH (x:X) -[r*2..4]- (:Y)
	RETURN x, r


### Knoten mit Label X und der Eigenschaft name=abc zurückgeben

	MATCH (x:X { name: 'abc' })
	RETURN x


### Knoten mit Label X zurückgeben, und, falls vorhanden, die sie referierenden Knoten mit Label Y
	
	MATCH (x:X)
	OPTIONAL MATCH (x) <-- (y:Y)
	RETURN x, y


### Einfache Zuweisung

	MATCH path = (x{ name: 'abc' } -[*] - (y{ name:'abc'})
	RETURN path


### Kürzester Pfad zwischen zwei Knoten

	MATCH (x{name:'abc'}), (y{name:'abc'})
	RETURN allShortestPaths((x)-[*]-(y)) as path


### Knoten mit komplexen Beziehungen zurückgeben

	MATCH (n) -[:BELONGS_TO]-> (cc:CostCenter) <-[:MANAGER_OF]- (m) <-[:REPORTS_TO]- (k)
	RETURN n.surname,m.surname,cc.code, k.surname



	MATCH (a:Person {name:'Jim'})-[:KNOWS]->(b)-[:KNOWS]->(c),
	      (a)-[:KNOWS]->(c)
	RETURN b, c

Dies kann auch geschrieben werden als:
	MATCH (a:Person)-[:KNOWS]->(b)-[:KNOWS]->(c), (a)-[:KNOWS]->(c)
	WHERE a.name = 'Jim'
	RETURN b, c


### Alle Knoten löschen

	MATCH (n)
	DELETE n


### Alle Knoten und ihre Beziehungen löschen

	MATCH (n)
	DETACH DELETE n


## Klauseln

* ``WHERE``: Provides criteria for filtering pattern matching results.
* ``CREATE`` and ``CREATE UNIQUE``: Create nodes and relationships.
* ``MERGE``: Ensures that the supplied pattern exists in the graph, either by reusing existing nodes and relationships that match the supplied predicates, or by creating new nodes and relationships.
* ``DELETE``: Removes nodes, relationships, and properties.
* ``SET``: Sets property values.
* ``FOREACH``: Performs an updating action for each element in a list.
* ``UNION``: Merges results from two or more queries.
* ``WITH``: Chains subsequent query parts and forwards results from one to the next. Similar to piping commands in Unix.
* ~~START: Specifies one or more explicit starting points—nodes or relationships—in the graph.~~ (``START`` is deprecated in favor of specifying anchor points in a ``MATCH`` clause.)
