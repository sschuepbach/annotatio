+++
title = 'C von A bis Z'
date = '2015-02-27'
+++


## Kapitel 2: Das erste Programm

* Präprozessor-Befehle werden mit einem # am Beginn der Zeile aufgerufen. Der Präprozessor ist ein teil des Compilers, der nicht das Programm übersetzt, sondern kontrollierend temporäre Änderungen im Programmtext vornimmt. (39)
* Headerdateien sind standardmässig in [/usr/include](file:///usr/include) gespeichert
* Eine main-Funktion wird in einem C-Programm immer benötigt (40)
* Wird ein String mit einem \ über mehrere Zeilen geschrieben, werden alle Leerzeichen zu Beginn der neuen Zeile in den String miteinbezogen! (41)


## Kapitel 3: Grundlagen

Verschiedene Zeichensätze (43ff.)

* Basis-Zeichensatz: 
* Ausführungszeichensatz: Basis-Zeichensatz + Steuerzeichen
* Trigraph-Zeichensatz: ??<Zeichen>; ersetzt in der C-Syntax verwendete Sonderzeichen


Regeln für Bezeichner (48):

* Erlaubt: Buchstaben, Ziffern, Unterstriche
* Erstes Zeichen muss ein ein Buchstabe sein
* Unterscheidung zwischen Gross- und Kleinschreibung
* Alle Schlüsselwörter sind reserviert


Literale: Von einer Programmiersprache definierte Zeichenfolgen zur Darstellung der Werte von Basistypen (48)

* Ganzzahlen:
	* Dezimalzahlen (erste Ziffer darf keine 0 sein)
	* Oktalzahl (beginnt immer mit Ziffer 0)
	* Hexadezimalzahl (beginnt immer mit der Sequenz 0x bzw. 0X; Gross- oder Kleinbuchstaben für Zahlen 10-15 möglich)
	* Suffixe:
		* L bzw. l: long-Zahl (32 bzw. 64 bit)
		* U bzw. u: unsigned
* Suffixe Fliesskommazahl:
	* F bzw. f: Einfache Genauigkeit
	* L bzw. l: Erhöhte Genauigkeit
* Zeichenliteral: In einfachen Hochkommata
* Zeichenkette: In doppelten Hochkommata; Zeichenketten sind immer um ein Zeichen länger als dargestellt (letztes Zeichen ist ASCII-Wert 0, d.h. Literal \0 (Stringbegrenzer))


Kommentare (51ff.)
Vorsicht: Bei einzeiligen Kommentaren (//) darf der Kommentar nicht mit einem \ abgeschlossen werden, da sonst der Kommentar auf der nächsten Zeile weitergeführt wird!

## Kapitel 4: Formatierte Ein-/Ausgabe mit "scanf()" und "printf()"

Eine Variable kann in vier Einzelteile zerlegt werden (57):

* Datentyp
* Name der Variable
* Speicheradresse der Variable
* Wert der Variable
* (Zugriffsrecht)
* (Gültigkeitsbereich)
* (Lebensdauer)


Möglichkeiten zur Umgehung des Puffer-Problems bei scanf():

* Benutzen von fflush() (funktioniert nicht unter Linux)
* Benutzen einer do-while-Schleife
* Alternative zu scanf() wählen (bspw. Kombination von fgets() mit sscanf())


