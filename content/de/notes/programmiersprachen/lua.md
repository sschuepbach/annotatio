+++
title = "Lua"
date = "2022-08-22"
description = "Leichtgewichtige Skriptsprache geeignet für Einbindung in grösseren Programmen"
+++

## Kommentare

```lua
-- Das ist ein Kommentar 
```

## Nativ unterstützte Typen

* **nil**: Unbekannter (absenter) oder ungültiger Wert
* **Boolean**: `true`, `false`
* **number**: Ganz- und Dezimalzahlen
* **string**
* **function**
* **table**
* **userdata**: Komplexe, in C implementierte Datenstruktur
* **thread**

### Einige Stringfunktionen

```lua
str = "hello, world"
laenge = #str
laenge = string.len(str)

concat = str .. "!"
int_to_str = "1" .. 2
str_to_int = "1" + 2
```

### Identifikation eines Typs

```lua
type(variable)
```

## Tables

- Einzige in Lua verfügbare Datenstruktur.
- Kann zur Implementierung von anderen Datenstrukturen (inklusive Klassen und
  Mixins) verwendet werden.
- Entsprechen einem Dictionary (wenn die Schlüssel nicht-numerisch sind) bzw.
  mehrdimensionalen Array (bei numerischen Schlüsseln)

## Standard-API

https://lua-stdlib.github.io/lua-stdlib/

### Module

| Name
| ----           | ------------------------------------------                    |
| std            | Lua-Standardbibliothek                                        |
| std.debug      | debug-Erweiterungen                  |
| std.functional | Funktionale Programmierung                                    |
| std.io         | IO-Erweiterungen                                              |
| std.math     | Mathematische Erweiterungen                                 |
| std.operator | Funktionale Formen der Lua-Operatoren                       |
| std.package  | Erweiterungen des core package-Moduls                       |
| std.strict   | Überprüft Gebrauch von nichtdeklarierten globalen Variablen |
| std.string   | String-Erweiterungen                                        |
| std.table    | Table-Erweiterungen     |

### Klassen

| Name          | Beschreibung                                                                             |
| ------------- | ----------------------------------------                                                 |
| std.tree      | Tree-Container Prototyp                                                        |
| std.container | Container Prototyp                                                                       |
| std.object    | Prototyp-basierte Objekte                                                                |
| std.list      | Tabellen als Listen                                                                      |
| std.optparse  | Parser und Prozessoren für Kommandozeileneingaben                                        |
| std.set       | Set-Container Prototyp                                                        
| std.strbuf | Stringbuffer |

## Ressourcen

* [Kurzreferenz](http://thomaslauer.com/download/luarefv51.pdf)
* [Learn Lua in Y Minutes](https://learnxinyminutes.com/docs/lua/)
