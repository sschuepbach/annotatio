+++
title = "LaTeX"
+++


## Beamer

* Generische Vorlagen für Beamer: [/usr/share/texmf-dist/tex/latex/beamer/solutions](file:///usr/share/texmf-dist/tex/latex/beamer/solutions)
* Themenmatrix: <https://hartwork.org/beamer-theme-matrix/>
