+++
title = "Haskell"
+++


## Installation in Arch Linux

Gemäss [Arch-Wiki](https://wiki.archlinux.org/index.php/Haskell) werden folgende Pakete empfohlen:

* ghc (ghc) — The compiler
* cabal-install (cabal-install) — A command line interface for Cabal and Hackage
* haddock (haskell-haddock-api and haskell-haddock-library) — Tools for generating documentation
* happy (happy) — Parser generator
* alex (alex) — Lexical analyzer generator
