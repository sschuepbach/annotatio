+++
bookCollapseSection = true
title = 'R'
+++

Dieser Artikel soll einige Grundlagen der [Programmiersprache R](http://www.r-project.org) vermitteln.

## Was ist R?

R ist ein Softwarepaket für statistisches Rechnen und statistische Grafiken. Es besteht aus der Programmiersprache R, dem gleichnamigen Interpreter, einem System für die grafische Repräsentation von Berechnungen und einer einfachen Entwicklungsumgebung. R ist Teil des [GNU-Projekts](http://www.gnu.org) und somit Freie Software. Das Softwarepaket läuft auf Linux/Unix, Mac OS und Microsoft Windows.

## Installation & Administration

Ausführliche Installationsanleitungen sowie Hinweise zu grundlegenden Einstellungen für [Windows](http://cran.r-project.org/doc/manuals/r-release/R-admin.html#Installing-R-under-Windows), [OS X](http://cran.r-project.org/doc/manuals/r-release/R-admin.html#Installing-R-under-OS-X) und [Unix/Linux-Systeme](http://cran.r-project.org/doc/manuals/r-release/R-admin.html#Installing-R-under-Unix_002dalikes) sind auf der Website des R-Projekts zu finden.

Die Installationsdateien sowie die meisten Zusatzpakete zu R sind über das sogenannte Comprehensive R Archive Network (CRAN) zu finden. Das statistische Seminar der ETH Zürich bietet einen [Spiegelserver des CRAN](http://stat.ethz.ch/CRAN) an.

## Bedienung

Wie viele interpretierte Programmiersprachen kann R in zwei verschiedenen Modi benutzt werden. Einerseits in einer interaktiven Konsole, anderseits als Skript im sogenannten "Batch Mode".

Für einfachere Berechnung kann R direkt in einer Konsole gestartet werden, die u.a. durch die mitgelieferte Entwicklungsumgebung zur Verfügung gestellt wird. Eine solche interaktive Sitzung hat den Vorteil, dass eine Eingabe direkt interpretiert wird und das Ergebnis anschliessend ausgegeben wird.

Anderseits eignet sich die Konsole weniger für komplexere Berechnungen. Ein R-Skript kann in jedem beliebigen Editor erstellt werden und danach mit dem R-Interpreter gestartet werden.

Die mitgelieferte Entwicklungsumgebung, aber auch viele andere Entwicklungsumgebungen für R stellen sowohl eine interaktive Konsole wie auch ein Editor zur Verfügung. Zudem können zumeist auch extern erstellte Skripts in den Entwicklungsumgebungen gestartet und interpretiert werden. 

### Interaktive Konsole
Die interaktive Konsole von R wird in Windows automatisch mit der mitgelieferten Entwicklungsumgebung gestartet und wird von ihr in einem eigenen Fenster zugänglich gemacht. Bei Mac OS und Linux-/Unix-Systemen kann die interaktive Konsole zusätzlich auch im Terminal durch den Befehl `R` aufgerufen werden.

In der aufgestarteten R-Konsole erscheint nach verschiedenen allgemeinen Hinweisen zur benutzten Version von R ein `>`. Dieses Zeichen am Anfang einer Zeile bedeutet, dass es sich um eine Befehlszeile handelt. D.h., Befehle, sog. "Expressions", können nun eingegeben werden. Wird ein Befehl mit der Enter-Taste bestätigt, wird er durch den Interpreter in Maschinencode übersetzt und ausgeführt. Das Ergebnis wird auf der folgenden Zeile direkt ausgegeben. Ausgabenzeilen sind in der R-Konsole durch eine vorangehende Zahl in eckigen Klammern erkennbar. Beispiel für eine Session in der R-Konsole:

`> 17 + 3`
`[1] 20`
`> x = 23	# Alle Zeichen einer Zeile nach dem # sind Kommentar`
`> print(x)      # Gebe den Wert der Variablen x aus`
`[1] 23`

Die Konsole kann mit der Eingabe von `q()` beendet werden.

Im folgenden werden Befehle und Ausgabenzeilen durch `code-Blöcke` oder einzelne Ausdrücke in `code-Schrift` ausgezeichnet. In Code-Blöcken werden einzugebende Befehle und Ausgaben durch die erwähnten Zeichen kenntlich gemacht. Einzelne Ausdrücke in code-Schrift im Fliesstext sind immer Befehle, sofern nicht anders angegeben. 

### Entwicklungsumgebungen
Neben der mitgelieferten Entwicklungsumgebung existiert eine Vielzahl von weiteren sogenannten IDEs (Integrated Development Environments), die entweder eigenständig oder als Plugin verfügbar sind. Eine Übersicht über verfügbare IDEs sowie ein rudimentärer Vergleich ihrer Features ist [hier](http://rwiki.sciviews.org/doku.php=id=guis:projects) zu finden.

### Zusätzliche Pakete
R kann durch die Installation von sogenannten Paketen um Funktionalitäten und Datensätzen erweitert werden. Beispielsweise bietet das Paket [tm](http://cran-r.project.org/web/packages/tm/index.html) ein Framework für Text Mining-Applikationen in R. Das wichtigste Repositorium für Zusatzpakete zu R ist das [Comprehensive R Archive Network (CRAN)](http://cran.r-project.org), auf dem momentan über 5500 Pakete vorhanden sind.

Die Installation von Paketen über die R-Konsole ist mit einem Befehl möglich:
`> install.packages("Paketname")`


## Ein kurzes R-Tutorial

### Funktionen
Als Funktionen werden in R alle Operationen zur Bearbeitung von Daten bezeichnet. Sie sind normalerweise in der Form `funktion('argument1, argument2, ...)`. Beispielsweise dient die Funktion `log(64,4)` dazu, den Logarithmus von 64 zur Basis 4 zu berechnen. Doch auch mathematische Operatoren wie Addition oder Multiplikation, in R normalerweise als `a + b` und `a * b` geschrieben, werden intern in Funktionen übersetzt.

Neben den eingebauten Funktionen können mit R auch selber Funktionen implementiert werden, die in der Folge im Programmcode zur Verfügung stehen. 

### Variablen
Bei Variablen handelt es sich um namentliche Repräsentationen von Daten. Die Zuweisung eines Wertes zu einer Variablen geschieht mit Hilfe des Zuweisungsoperators (der intern ebenso in eine Funktion übersetzt wird). Neben dem auch in anderen Programmiersprachen bekannten Zuweisungsoperator `=` kommt in R auch `<-` zur Anwendung.

Beispiel:
`> x = 12	# Der Variablen mit Namen x wird der Wert 12 zugewiesen`
`> y = "Hallo!"  # Der Variablen mit Namen y wird der Wert "Hallo!" zugewiesen`
`> print(x) 	 # Gib den Wert von x aus`
`[1] 12`
`> print(y)	 # Gib den Wert von y aus`
`[1] "Hallo!"`

### Einfache Datentypen
R umfasst eine sehr grosse Zahl an verschiedenen Datentypen. Folgend sollen einige der Wichtigsten aufgeführt werden.

| Name      | Umfasst         | Hinweise                                                                                                                                   |
|-----------|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Numeric   | Dezimalzahlen   | Ist der numerische Standard-Datentyp, der auch Ganzzahlen umfasst.                                                                         |
| Integer   | Ganzzahlen      | Zuweisung muss explizit mit x = as.integer(1) erfolgen. Dezimalzahlen werden automatisch in eine Ganzzahl überführt (coerce).              |
| Complex   | Komplexe Zahlen | Eine Komplexe Zahl wird durch den imaginierten Wert i definiert (bspw. z = 1 + 2i). Explizit erfolgt die Zuweisung durch z = as.complex(1) |
| Logical   | TRUE, FALSE     | Logische Operatoren: AND, OR, ! (sowie entsprechende Zeichen)                                                                              |
| Character | Zeichenketten   | Zuweisung erfolgt entweder implizit (bspw. durch fname = "Hans") oder explizit durch as.character                                          |


### Komplexere Datentypen

#### Vektoren
Ein Vektor ist eine Sequenz von Elementen (= Komponenten) mit dem gleichen einfachen Dateityp. Er wird durch eine Zuweisung der Form `x = c(2, 3, 5)` erstellt.

Auf eine einzelne Komponente kann mit einem Index der Form `x[y]` zugegriffen werden, wobei y auf die Komponente an der Stelle y verweist. Die erste Komponente hat den Index 1. Negative Indexwerte werden vom Ende her berechnet. `x[-1]` verweist also beispielsweise auf die letzte Komponente. Es besteht auch die Möglichkeit, auf mehrere Elemente gleichzeitig zuzugreifen. Soll ein ganzer Bereich von Elementen extrahiert werden, kann dies durch die Eingabe von `x[1-3]` geschehen. Disparate Elemente können mit der Angabe eines Indexvektors erfasst werden: `x[c(1, 3)]`. Wird ein Indexwert ausserhalb des Bereichs des Vektors x angegeben, wird die Fehlermeldung `NA` ausgegeben.

Es ist möglich, den Komponenten in einem Vektor Namen zuzuweisen und danach über diesen Namen auf die Komponente zuzugreifen. Bsp.:
`> v = c("Hans", "Maria")`
`> names(v) = c("Erste", "Letzte")`
`> v["Erste"]`
`[1] "Hans"`

Weitere Informationen zu Vektoren:

* [Auf r-tutor](http://www.r-tutor.com/r-introduction/vector)
* [Im R-Manual](http://cran.r-project.org/doc/manuals/r-release/R-intro.html#Simple-manipulations-numbers-and-vectors)



#### Matrizen
Eine Matrix ist eine Sammlung von Elementen, die in einem zweidimensionalen rechteckigen Raum angebracht sind. Bsp.:
`A = matrix(`
`+  c(2, 4, 3, 1, 5, 7), # Elemente`
`+  nrow=2,              # Anzahl Zeilen`
`+  ncol=3,		 # Anzahl Spalten`
`+  byrow = TRUE)	 # Matrix den Zeilen entlang füllen`
/
`> A`
`      [,1]  [,2]  [,3]`
`[1,]     2     4     3`
`[2,]     1     5     7`

Auf ein einzelnes Element kann folgendermassen zugegriffen werden:
`> A[1,2]`
`[1] 4`

Eine ganze Zeile kann beispielsweise durch `A[1,]`, eine Spalte `A[,2]` ausgegeben werden.

Weitere Informationen zu Matrizen:

* [Auf r-tutor](http://www.r-tutor.com/r-introduction/matrix)
* [Im R-Manual](http://cran.r-project.org/doc/manuals/r-release/R-intro.html#Arrays-and-matrices)


#### Listen
Eine Liste ist ein generischer Vektor, der andere Vektoren und Elemente enthalten kann. Bsp.:
	n = c(2, 3, 5)
	s = c("aa", "bb")
	b = c(TRUE, FALSE, TRUE, TRUE)
	x = list(n, s, b, 3)


Der Zugriff auf ein einzelnes Element erfolgt in diesem Fall durch eine "mehrdimensionale" Indexierung, wobei die Listen jeweils mit doppelten eckigen Klammern indexiert sind. Bsp.:
`> x2[1]`
`[1] "aa"`

Zu beachten ist, das Vektoren bzw. andere Datenelemente in eine Liste _kopiert_ und nicht von dieser referenziert werden. Dies ist insofern wichtig, als dass die erneute Wertezuweisung zu einem bestimmten Element in der Liste keinen Einfluss auf den Wert hat, der im betroffenen Vektor o.ä. enthalten ist. Bsp.:
`> x2[1]`
`[1] "aa"`

`> x2[1] = "bb" 	# Der Liste wird neuer Wert zugewiesen`
`> x2[1]`
`[1] "bb" 		# Die Liste enthält den neuen Wert`
`> c[2]`
`[1] "aa"		# Der in x enthaltene Vektor c enthält immer noch den alten Wert`

Weitere Informationen zu Listen:

* [Auf r-tutor](http://www.r-tutor.com/r-introduction/list)
* [Im R-Manual](http://cran.r-project.org/doc/manuals/r-release/R-intro.html#Lists-and-data-frames)


#### Data Frames
Ein Data Frame wird benutzt, um Datentabellen zu speichern. Sie ist eine zweidimensionale Liste von Vektoren mit gleicher Länge. Im Unterschied zu Matrizen können sie auch Elemente mit verschiedenen Datentypen enthalten und sind besonders dazu geeignet, beispielsweise Daten von einer Excel-Tabelle aufzunehmen.

Weitere Informationen zu Data Frames:

* [Auf r-tutor](http://www.r-tutor.com/r-introduction/data-frame)
* [Im R-Manual](http://cran.r-project.org/doc/manuals/r-release/R-intro.html#Lists-and-data-frames)


#### Objekte und Klassen
%%TODO

#### Modelle und Formeln
%%TODO

### Hilfe
Die R-Konsole verfügt über eine interaktive Hilfe:

* Die Hilfe für eine bestimmte Funktion, beispielsweise `solve`, kann mit `help(solve)` oder alternativ mit `?solve` aufgerufen werden
* Um Hilfe für ein spezielles Zeichen, beispielsweise `[`, oder für einige wenige Schlüsselwörter wie `if`, `for` und `function` zu erhalten, muss der Suchbegriff mit Apostrophen versehen werden: `help("[")`
* Die Hilfe ist auch über HTML-Seiten verfügbar. Sie können mit `help.start()` aufgerufen werden
* `help.search(solve)` oder alternativ `??solve` erlaubt die Suche nach der Funktion `solve` in verschiedenen Kontexten
* Anwendungsbeispiele für eine bestimmte Funktion können mit `example(solve)` aufgerufen werden
* Weitere Möglichkeiten der Hilfe können mit `?help` abgefragt werden


Auf der Website des R-Projekts sind zudem weitere/ergänzende Ressourcen verfügbar:

* [The R Manuals](http://cran.r-project.org/manuals.html) enthalten die Dokumentationen, welche mit `help.start()` lokal abgerufen werden können, zusätzlich in verschiedenen Dateiformaten
* Die [FAQs](http://cran.r-project.org/faqs.html) bieten zusätzlich einen Überblick über häufig gestellte Fragen zu R und Antworten darauf (z.T. nach Betriebssystemen geordnet)
* Schlussendlich bieten die [Contributed Documentations](http://cran.r-project.org/other-docs.html) Einführungen, Anleitungen, Referenzkarten etc., die von diversen Personen beigesteuert wurden



## Literatur

Eine Auswahl an Literatur zu R. Eine [erschöpfende Liste](http://www.r-project.org/doc/bib/R-books.html) ist auf der Website des R-Projekts zu finden.

### Einführungen / Referenzen

* Adler, Joseph. R in a Nutshell: A Desktop Quick Reference. 2. Auflage. Sebastopol 2012.
* Cotton, Richard. Learning R: A Step-by-Step Function Guide to Data Analysis. Sebastopol 2013.
* Gardener, Mark. Beginning R: The Statistical Programming Language. Indianapolis 2012.
* Gardener, Mark. The Essential R Reference. Indianapolis 2013.
* Rahlf, Thomas. Datendesign mit R: 100 Visualisierungsbeispiele. München 2014.
* Teetor, Paul. 25 Recipes for Getting Started with R: Excerpts from the R Cookbook.
* Teetor, Paul. R Cookbook. Sebastopol 2011.
* Teutonico, Donato. Instant R Starter. Birmingham 2013.


### Spezifische Themen

* Alexandrowicz, Rainer W. R in 10 Schritten: Einführung in die statistische Programmierumgebung. Stuttgart 2012.
* Chang, Winston. R Graphics Cookbook: Practical Recipes for Visualizing Data. Sebastopol 2012.
* Danneman, Nathan & Heimann, Richard. Social Media Mining with R. Birmingham 2014.
* Lantz, Brett. Machine Learning with R. Birmingham 2013.
* Manderscheid, Katharina. Sozialwissenschaftliche Datenanalyse mit R. Wiesbaden 2012.


## Weblinks

* [Website des R-Projekts](http://www.r-project.org)
* [The R Journal: Artikel für Benutzer und Entwickler von R](http://journal.r-project.org)
* [R-Bloggers: Neuigkeiten und Tutorials zu R](http://www.r-bloggers.com)
* [RSeek: Suchmaschine für Fragen rund um R](http://www.rseek.org)
* [RStudio: Eine beliebte R-IDE](http://www.rstudio.com)
* [Der CRAN-Mirror der ETH](http://stat.ethz.ch/CRAN)
* Deutschsprachige Video-Tutorials:

- [R für Anfänger (1): Warum R lernen?](http://www.youtube.com/watch?v=ldVL0kJYigs)
- [R für Anfänger (2): Wie kommt man an R ran?](http://www.youtube.com/watch?v=WKE5uio0Chc)
- [R für Anfänger (3): Grundlagen zu Daten & Funktionen](http://www.youtube.com/watch?v=v6fhItmajto)
- [R für Anfänger (4): Daten einlesen, analysieren, plotten für Eilige](http://www.youtube.com/watch?v=OADUACud99k)
- [R für Anfänger (5): Das R-Hilfe-System](http://www.youtube.com/watch?v=RFnSDhYR3TE)
- [R für Anfänger (6): Zusätzliche R-Pakete installieren](http://www.youtube.com/watch?v=9uSLtNLrcoE)

