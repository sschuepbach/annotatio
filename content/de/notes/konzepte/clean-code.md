+++
title = 'Clean Code'
+++


## Names++

Naming conventions:

1. Choose your names thoughtfully
2. Communicate your intent
3. Avoid disinformation
4. Pronounceable names
5. Avoid encodings (eg. Hungarian Notation)
6. Clean code should always read like well-written prose => Choose parts of speech well:
	1. Classes -> Nouns
	2. Variables -> Nouns
	3. Boolean variables -> Predicates (e.g. ``isEmpty``)
	4. Methods -> Verbs
	5. Methods with boolean as return values -> Predicates
	6. Enum -> Often adjectives
7. Scope rules:
	1. Variables in a small scope: Short names
	2. Variables in a large scope: Long names
	3. Functions / classes in a small scope (e.g. private scope): Long names (names as documentation)
	4. Functions / classes in a large scope (e.g. public scope): Short names (willl probably be used by other people)


## TDD

### Three Laws of TDD

1. Write **no** production code except to pass a failing test.
2. Write only **enough** of a test to demonstrate a failure.
3. Write only **enough** production code to pass the test.


