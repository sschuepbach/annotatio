+++
title = 'Entwurfsmuster'
+++

### Erzeugungsmuster (Creational patterns)
Erzeugungsmuster beinhalten zwei grundsätzliche Ideen:

* Abkapselung der kokreten Klassen, welche das System benötigt
* Verstecken, wie Instanzen dieser konkreten Klassen erzeugt und kombiniert werden


### Strukturmuster (Structural patterns)

### Verhaltensmuster (Behavorial patterns)

### Entwurfsmuster zu Nebenläufigkeit (Concurrency patterns)

