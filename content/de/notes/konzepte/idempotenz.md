+++
title = 'Idempotenz'
+++


Eine Eigenschaft von Operationen, welche immer das gleiche Resultat zeitigen, egal ob sie ein- oder mehrmals ausgeführt werden (Bsp.: On-Knopf)
