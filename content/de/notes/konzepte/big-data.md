+++
title = 'Big Data'
date = '2015-05-31'
+++


## Applikationen

* [Apache Bigtop](https://bigtop.apache.org/)
* [Apache Crunch](https://crunch.apache.org/)
* [Apache Falcon](http://hortonworks.com/hadoop/falcon/)
* [Apache Flink](https://flink.apache.org/)
* [Apache Hadoop](https://hadoop.apache.org/)
* [Apache Kafka](https://kafka.apache.org/)
* [Apache Parquet](https://parquet.apache.org/)
* [Apache Phoenix](https://phoenix.apache.org/)
* [Apache Samza](https://samza.apache.org/)
* [Apache Spark](https://spark.apache.org/)
* [Apache Storm](https://storm.apache.org/)
* [Apache Tajo](https://tajo.apache.org/)
* [Druid](http://druid.io/)


