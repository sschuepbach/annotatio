+++
title = 'Arität'
+++


(engl. *Arity*)

Anzahl der Parameter einer Funktion, Methode oder Prozedur. Im Englischen wird bei

* keinem Parameter von einer *nullary function*,
* bei einem Parameter von einer *unary function*,
* bei zwei von einer *binary function*,
* bei drei von einer *ternary function*,
* bei n von einer *n-ary function* und
* bei einer variablen Anzahl von Parametern von einer *variadic function* gesprochen.
