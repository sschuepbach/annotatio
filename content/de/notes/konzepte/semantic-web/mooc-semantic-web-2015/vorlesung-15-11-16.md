+++
title = "Vorlesung 16.11.15"
weight = 3
+++


## 01 - Ontology in Philosophy and Computer Science

"An ontology is an **explict, format specification of a shared conceptualization**. The term is borrowed from philosophy, where an Ontology is a systematic account of existence. For AI systems, what 'esists' is that which can be represented." (Thomas R. Gruber)

* Conceptualization: Abstract model (domain, identified relevant concepts, relations)
* explicit: meaning of all concepts must be defined
* formal: machine understandable
* shared: consensus about ontology


Ontologien können durch **Klassen**, **Relationen** und **Instanzen** repräsentiert werden. Bei Klassen handelt es sich um abstrakte Gruppen, Sets oder Collections von Objekten (bspw. "Fische") und sind so Ontologiekonzepte. Zudem verfügen sie über Attribute in Form von Namen-Wert-Paare (hier bspw. Lebensraum = Wasser).
Relationen sind spezielle Attribute, deren Wert ein anderes Objekt ist.
Für Relationen und Attribute können **Regeln** (Einschränkungen) definiert werden
Mithilfe von Klassen, Relationen und Regeln können (komplexe) **Aussagen** getroffen werden. Ein spezieller Fall von Aussagen sind **formale Axiome** (Wissen, das nicht durch bereits vorhandenes Wissen ausgedrückt werden kann).
Instanzen: Beschreiben Individuen einer Ontology
Terminological Knowledge: Drückt Relationen / Attribute zwischen Klassen aus
Assertional Knowledge: Drückt Relationen / Attribute zwischen Instanzen von Klassen aus


## 02 - Ontology Types

Verschiedene Typen von Ontologien:

* Top-level Ontology: Fundamentale Ontologien
* Domain Ontology: Beziehen sich auf ein bestimmtes Gebiet
* Task Ontology: Fundamentale Konzepte einer bestimmten allgemeinen Aktivität oder Aufgabe; spezialisiert Terme, welche in einer Top-level Ontology eingeführt wurden
* Application Ontology: Oftmals eine Spezialsierung sowohl einer Domain Ontology als auch einer Task Ontology, sie sind also fokussiert auf eine bestimmte Aktivität und eine bestimmtes Gebiet (bspw. ein Kochbuch über französische Küche)


Andere Kategorisierungen:

* Über Expressivität von Ontologien (von informal bis formal (=hohe Expressivität)):
	* Kontrolliertes Vokabular: Vokabular ohne Beschreibung
	* Glossare: Definition durch natürliche Sprache
	* Thesauri: Kontrolliertes Vokabular, wobei die Konzepte in Relation zueinander gesetzt werden (Synonmye, Hierarchien, Homonyme, Assoziationen)
	* Taxonomien: Definition eines hierarischen Systems von Gruppen; auch Klassifikationsschema oder Nomenklatur genannt. Taxonomien können weiter unterschieden werden in:
		* informale IST-EIN-Hierarchie: explizite Hierarchie von Klassen, allerdings sind die Relationen von Unterklassen nicht strikt (bspw. Index einer Bibliothek -> ein Buch kann mehreren Kategorien zugeordnet werden)
		* formale IST-EIN-Hierarchie: wie informale IST-EIN-Hierarchie, aber mit strikten Unterklassen-Relationen
		* formale Instanz: explizite Hierarchie von Klassen, neben strikten Unterklassen-Relationen sind auch Instanz-von-Beziehungen erlaubt
	* Frames
	* Restrictions
	* General logical Constrants
	* Disjunctiveness, inversiveness, Part-of


## 03 - The Foundations of Logic

Arbeitsdefinition: **Logic is the study of how to make formal correct deductions and inferences**


* **Itentional semantics**: The meaning intended by the user; beschränkt die Anzahl möglichen Modelle (Bedeutungen) auf die Bedeutung, welche der Anwender intendiert hat
* **Formal semantics**: drückt die Bedeutung einer Symbolsequenz (bspw. Programme) in einer formalen Sprache so aus, dass die Gültigkeit der Sequenz durch die Anwendung von Ableitungsregeln bewiesen werden kann
* **Procedural semantics**: Die Bedeutung eines sprachlichen Ausdrucks (bspw. Programme) ist die Prozedur, welche intern jedesmal abläuft, wenn der Ausdruck vorkommt.


Wie kann die Semantik einer mathematischen Logik ausgedrückt werden? Durch Modelle (eine Modelltheorie), welche eine Analogie zu der Welt sein soll, die modelliert wird.
=> Modelltheoretische Semantik: Führt die semantische Interpretation von natürlicher oder künstlicher Sprache durch die Identifizierung von Bedeutung mit einer exakten und formal definierten Interpretation gemäss einem Model durch. (= Formale Interpretation mit einem Modell).  

## 04 - Short Recapitulation of Propositional Logic

A formula F is

* **tautological** if all interpretations I are true
* **satisfiable** if some interpretations I are true
* **refutable** if not all interpretations I are true
* **not satisfiable** if no interpretation I is true


## 05 - Short Recapitulation of First Order Logic

Theory = Set of Formulas
Theory = Knowledge Base
Da First-Order-Logic nur halb-entscheidbar ist (wir können in finiter Zeit beweisen, dass eine Theorie T eine Formel F impliziert, aber nicht, dass T F nicht impliziert), ist sie für ein automatische Beweisführung nicht gänzlich brauchbar.

## 06 - How to mechanize Reasoning - Tableaux Algorithm

For the logical calculus, there must be proven:

* Correctness: Every syntactic entailment is also a semantic entailment
* Completeness: All semantic entailments are also syntactic entailments


## 07 - Description Logics

## 08 - Inference and Reasoning

## 09 - DLs and the Open World Assumption

## 10 - Tableaux Algorithm for ALC
