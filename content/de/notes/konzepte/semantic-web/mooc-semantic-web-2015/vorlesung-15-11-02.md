+++
title = "Vorlesung 02.11.15"
weight = 1
+++


## 01 - Limits of the Web

Verschiedene Epochen des "Internets":

* Computer centered processing (- ~1990): Keine GUI, nur Terminal
* Document centered processing (~1990-): Neuerungen: Browser, Links, Suchmaschinen, HTML
	* HTML:
		* Wie wird Information präsentiert
		* Wie ist Information verlinkt
		* Nicht, was Information bedeutet
		* => Für Menschen, nicht für Maschinen => Keine *explizite* Semantik (Interpretation ist notwendig)
* Data centered processing: The Web of Data
	* Upgrade des Web of Documents
	* Web gedacht als riesige dezentralisierte Datenbank (knowledge base) mit maschinenlesbaren Informationen


## 02 - The Importance of Meaning

"Building blocks" von Bedeutung:

* Syntax: ("**The definition of normative structure of data.**")
* Semantik (= Verstehen) ("**The study of relationships between signs and symbols and what they represent.**")
	* "Semantics asks, how sense and meaning of complex concepts can be *derived* from simple concepts based on the *rules of syntax*."
	* Die Semantik einer Nachricht hängt vom Kontext und der Pragmatik ab.
* Kontext: Umfasst alle Elemente jeder Art von Kommunikation welche die Interpretation eines kommunizierten Inhalts definieren. ("**The denotion of all elements defining the interpretation of communicated content.**")
* Pragmatik: Intention des Nachrichtensenders ("**The study of applying language in different situation.**")
* Erfahrung


## 03 - Understanding Content on the Web

Zwei Wege, um Webinhalte maschinenlesbar zu machen:

* Implizit: NLP
* Explizit: Semantic Web-Technologien: Annotation von traditionellen Webinhalten mit semantischen Metadaten

=>Die Bedeutung von Informationen (Semantik) im Web wird expliziert duch formale (strukturierte) und standardisierte Wissensrepräsentationen (Ontologien)

Im Web of Data ist es möglich,

* Bedeutung von Informationen automatisch zu verarbeiten,
* heterogene Daten zu einander in Beziehung zu setzen und zu integrieren sowie
* implizite Informationen von expliziten Bedeutungszusammenhängen automatisch abzuleiten


*Properties* (bspw. "name") haben eine oder mehrere *domains* (Klassen, bspw. "planet") und eine *range* (Klasse, in diesem Fall "string").

**"The Semantic Web is an extension of the current web in which information is given well-defined meaning, better enabling computers and people to work in cooperation."**
(Tim Berners-Lee, James Hendler, Ora Lassila: The Semantic Web, Scientific American, 284(5), S. 24-43 (2001))

## 04 - Semantic Web Technology and the Web of Data

## 05 - How to Use the Web of Data

Definition Linked Open Data: "LOD denote publicly available (RDF) Data in the Web, identified via URI and accessable via HTTP. Linked data link to other data via URI."
Definition Semantische Entität: Ein Objekt oder DIng mit einer gegebenen expliziten Bedeutung.

## 06 - How to Name Things - URIs

Definition Uniform Resource Identifiers: "URI defines a simple and extensible schema for worldwide unique identification of abstract physical resources (RFC 3986)."

Repräsentation (Webpage) definiert die Präsentation der Informationen. In diesem Fall ist die Webpage der Designator, das Ding, welche sie beschreibt, das Designierte.
Die URI kann je nach Kontext das Designierte oder der Designator repräsentieren: Im Rahmen einer Content Negotation wird bspw. eine Anfrage an DBPedia über den Eiffelturm gemacht, indem DBPedia der Identifier (der Name) des Eiffelturms geschickt wird. Hier repräsentiert die URI das Designierte, also der Eiffelturm. Zurückgegeben wird unter anderem die URI der DBPedia-Seite über den Eiffelturm. Diese URI wiederum repräsentiert hier den Designator, die HTML-Repräsentation der Informationen über den Eiffelturm.
Unter derselben URI können verschiedene Repräsentationen von Informationen zu finden sein, deren Retrieval wiederum abhängig ist von der Content Negotiation: Bspw. Informationen als html oder rdf-xml repräsentiert.

## 07 - How to Represent Simple Facts with RDF

**RDF:**

* **Resource**:
	* Kann alles sein
	* Muss eindeutig durch eine URI identifizierbar und referenzierbar sein
* **Description**:
	* Beschreibung einer Resource,
	* indem Eigenschaften und Beziehungen zwischen Resourcen als gerichtete Graphen ausgedrückt werden
* **Framework**:
	* Kombination verschiedener webbasierter Protokolle (URI; HTTP; XML, Turtle, JSON, ...)
	* Auf einem formalen Model basierend (Semantik)


RDF Statements (RDF-Triple):
Subject + Property + Object / Value

RDF Building Blocks:

* URI
* Literals
	* typed
	* untyped
* Blank nodes


Literals können durch

* ein *XML Schema Datentyp* typisiert (Namespace: <http://www.w3.org/2001/XMLSchema>#) und
* deren Sprache durch ein *Language tag* (@<lang>) bezeichnet werden.


## 08 - RDF and Turtle Serialization

Präfix-Definition:
``@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .``

Base directive in Turtle:
	@base <http://dbpedia.org/resource/> .
	<Pluto> <http://dbpedia.org/ontology/> "1930" .



* Predicate list: Statements, welche das gleiche Subjekt teilen. Getrennt durch **;**
* Object list: Statements, welche das gleiche Subjekt und das gleiche Prädikat teilen. Getrennt durch **,**


Blank nodes als Subjekt in Turtle:
	@prefix dbo: <http://dbpedia.org/ontology/> .
	@prefix xsd: <http://www.w3c.org/2001/XMLSchema#> .
	@base <http://dbpedia.org/resource/>.
	[] dbo:spaceShip <New_Horizons> .


*(Anonymous) blank nodes* als Objekt in Turtle:
	@prefix dbo: <http://dbpedia.org/ontology/> .
	@prefix xsd: <http://www.w3c.org/2001/XMLSchema#> .
	@base <http://dbpedia.org/resource/>.
	
	<Pluto> dbo:spaceMission [ dbo:spaceShip <New_Horizons> ;
	dbo:visited "2015-07-14"^^xsd:date .
	],
	[ dbo:spaceShip <USS_Enterprise_(NCC-1701)> ;
	dbo:visited "2245-07-14"^^xsd:date .
	] .

*Dereferencable blank nodes* (kann nur von innerhalb eines Dokuments / Nodes referenziert werden):
	@prefix dbo: <http://dbpedia.org/ontology/> .
	@prefix xsd: <http://www.w3c.org/2001/XMLSchema#> .
	@base <http://dbpedia.org/resource/>.
	
	<Pluto> dbo:spaceMission _:ID1, _:ID2 .
	
	_:ID1 dbo:spaceShip <New_Horizons> ;
	dbo:visited "2015-07-14"^^xsd:date .
	
	_:ID2 dbo:spaceShip <USS_Enterprise_(NCC-1701)> ;
	dbo:visited "2245-07-14"^^xsd:date .

Problem von Multi Valued Relations (=> Gruppierung von Statements zu einem bestimmten Subjekt, welche zusammengehören, bspw. Statements über zwei verschiedene Raumfahrt-Missionen zum Planet Pluto, ausgedrückt über das Subjekt Pluto):
=> Lösung: Blank nodes als "auxiliary nodes"

*RDF Lists*:

* Generelle Datenstrukturen, um Resourcen oder Literale aufzuzählen
* Es handelt sich lediglich um Abkürzungen - die Semantik wird nicht erweitert ("Syntactic sugar")
* Es gibt zwei Arten von Listen:
	* *Container*: Offene Listen, neue extensions (Einträge) sind möglich
	* *Collection*: Geschlossene Listen, keine extensions möglich


Container können Elemente als

* unordered set (Bag - ausgedrückt mit rdf:Bag),
* ordered set (Seq - ausgedrückt mit rdf:Seq) oder
* alternatives container (Alt - ausgedrückt mit rdf:Alt) - verschiedene Optionen sind möglich

beinhalten
Membership property von Elementen in einem Container: rdf:_1, rdf:_2, rdf:_3 etc.
Ausgedrückt in Turtle:
	@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
	@prefix ex: <http://example.org/test#> .
	
	ex:SolarSystem ex:hasPlanets [
	a rdf:Seq ;
	rdf:_1 ex:Mercury ;
	rdf:_2 ex:Venus;
	rdf:_3 ex:Earth ;
	rdf:_4 ex:Mars ;
	rdf:_5 ex:Jupiter ;
	rdf:_6 ex:Saturn 
	] .

Collection (vergleichbar mit Linked Lists, Listen mit einem Head- und Tail-Elementen, rekursiv). Ausgedrückt in Turtle:
	@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
	@prefix ex: <http://example.org/test#> .
	
	ex:SolarSystem ex:hasPlanets [
	rdf:first ex:Mercury ; rdf:rest [
	rdf:first ex:Venus; rdf:rest [
	rdf:first ex:Earth ; rdf:rest [
	rdf:first ex:Mars ; rdf:rest [
	rdf:first ex:Jupiter ; rdf:rest [
	rdf:first ex:Saturn ;
	rdf:rest rdf:nil ] ] ] ] ] ] .

In Turtle können aber Collections inzwischen auch einfacher ausgedrückt werden:
	@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
	@prefix ex: <http://example.org/test#> .
	
	ex:SolarSystem ex:hasPlanets (
	ex:Mercury ex:Venus ex:Earth ex:Mars ex:Jupiter ex:Saturn ) .

