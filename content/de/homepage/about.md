---
title: annotat.io
draft: false
# role: Example Role
avatar: images/logo.png
bio: Dies und das
social:
  - icon: envelope
    iconPack: fas
    url: mailto:mail@annotat.io
  - icon: mastodon
    iconPack: fab
    url: https://openbiblio.social/@seb
  - icon: gitlab
    iconPack: fab
    url: https://gitlab.switch.ch/sschuepbach

widget:
  handler: about

  # Options: sm, md, lg and xl. Default is md.
  width:

  sidebar:
    # Options: left and right. Leave blank to hide.
    position:
    # Options: sm, md, lg and xl. Default is md.
    scale:
  
  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color: secondary
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment: 
---

## Hallo...

...liebe Reisende, lieber Reisender der unermesslichen Weiten, die man World Wide
Web nennt. Ich freue mich, dass du genau diese Seite unter den Milliarden
gefunden hast, die da draussen zu finden sind!

Gestatten, mein Name ist Sebastian, ich habe vor längerer Zeit einmal
Geschichte und Philosophie studiert und arbeite als als Full-Stack Entwickler
und Softwarearchitekt an der Universitätsbibliothek Basel. Ab und zu schreibe
und spreche ich über die Dinge, die ich mache. Als Klammer für diese Ergüsse
dient diese Website.
